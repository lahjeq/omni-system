/**
 * 
 */
package queryhouse;

import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 * @author Billy Bissic
 *
 */
public class SystemMethods {
		Short	afternoon;
		String	afternoonStart;
		Integer	computer;
		String	computerName;
		Short	dawn;
		String	dawnEnd;
		Short	dusk;
		String	duskEnd;
		String	duskStart;
		private static String  errorMessage;
		private static String  message;
		private static String currentMediaPath;
		Short	evening;
		String	eveningEnd;
		String	eveningStart;
		//DirectoryInfo	directoryInfo;
		//DirectoryInfo()	directoryInfo[]
		String	directoryPath;
		private static ArrayList<String> systemDrives = new ArrayList<String>();
		private static ArrayList<String> mediaFolders = new ArrayList<String>();
		private static ArrayList<String> currentMediaFolder = new ArrayList<String>();
		private static ArrayList<String> mediaFiles = new ArrayList<String>();
		private static ArrayList<String> currentMediaFiles = new ArrayList<String>();
		
		String	file;
		//FileInfo()	fileInfo[]
		String	midnight;
		String	morning;
		String	night;
		String	nightEnd;
		String	noon;
		//TimeSpan	time;
		Byte TimeElapsed;
		Integer	timeLapse;
		Byte timeRemaining;
		String	timeStampCommand;
		Boolean	exists;
		String	extension;
		Integer	fieldCount;
		Integer	fields;
		Integer	characterCount;
		private static Integer length;
		String	library;
		//MD5	md5Hasher;
		Integer	median;
		Integer	nowPlaying;
		Integer	OIndex;
		String	oldPosition;
		ArrayList<String> oldUser = new ArrayList<String>();
		Integer	OP;
		Integer	orginalIndex;
		Integer	decision;
		ArrayList<String> paths = new ArrayList<String>();
		Integer	playing;
		Integer	muteState;
		//	currentProcess
		ArrayList<String> dataRow = new ArrayList<String>();
		//DataRow	dataRow
		//DataTable	dataTable
		ArrayList<String> temp = new ArrayList<String>();
		ArrayList<String> tempArray = new ArrayList<String>();
		String	tempitem;
		ArrayList<String> tempres = new ArrayList<String>();
		Integer	totalSelectedItems;
		char trim;
		Integer	upperBound;
		String	fileStream;
		private static int counter;
		private static String mrSwappy;
		private static String newCharString;
		private static Integer subStringStart;
		private static String replace;
		private static String propertiesString;
		private static String userName;
		private static String password;
		private static String userInput;
		private static String dbClassName = "com.mysql.jdbc.Driver";
		private static String url;
		private static Connection connection;
		private static PreparedStatement preparedStatement = null;
		private static String preparedQuery;
		private static String preparedSetString = null;
		    
		
		public static void resetCounter()
		{
			counter = 0;
		}
		
		public static int getCurrentCount()
		{
			return counter;
		}
		
		public static void setCounter(int value)
		{
			counter = value;
		}
		
		public static void setCounterUp()
		{
			counter = counter + 1;
		}
		
		public static void setCounterDown()
		{
			counter = counter - 1;
		}
		
		public static void setPreparedQuery(String value)
		{
		   	preparedQuery = value;
		}
		
		public static void clearMediaFolders()
		{
			mediaFolders.clear();
		}
		
		public static void addSystemDrive(String value)
		{
			systemDrives.add(value);
		}
		
		public static String getSystemDrive(int value)
		{
			return systemDrives.get(value);
		}
		
		public static int getSystemDriveSize()
		{
			return systemDrives.size();
		}
		
		public static void clearSystemDrives()
		{
			systemDrives.clear();
		}
		
		public static void clearCurrentMediaFiles()
		{
			currentMediaFiles.clear();
		}
		
		public static int getCurrentMediaFilesSize()
		{
			return currentMediaFiles.size();
		}
		
		public static void setCurrentMediaFile(String value)
		{
			currentMediaFiles.add(value);
		}
		
		public static void clearCurrentMediaFolders()
		{
			currentMediaFolder.clear();
		}
		
		public static int getCurrentMediaFolderSize()
		{
			return currentMediaFolder.size();
		}
		
		public static String getCurrentMediaFolder(int value)
		{
			return currentMediaFolder.get(value);
		}
		
		public static void addFolderToMediaFolders(String value)
		{
			mediaFolders.add(value);
		}
		
		public static String getMediaFolder(int value)
		{
			return mediaFolders.get(value);
		}
		
		public static ArrayList<String> getMediaFolders()
		{
			return mediaFolders;
		}
		
		public static int getMediaFolderSize()
		{
			return mediaFolders.size();
		}
		
		public static void clearMediaFiles()
		{
			mediaFiles.clear();
		}
		
		public static void addFileToMediaFiles(String value)
		{
			mediaFiles.add(value);
		}
		
		public static String getMediaFile(int value)
		{
			return mediaFiles.get(value);
		}
		
		public static ArrayList<String> getMediaFiles()
		{
			return mediaFiles;
		}
		
		public static int getMediaFileSize()
		{
			return mediaFiles.size();
		}
		
		public static String getPreparedQuery()
		{
		   	return preparedQuery;
		}
		    
	    public static String getPreparedSetString()
	    {
	    	return preparedSetString;
	    }
		    
	    public static void setPreparedSetString(String value)
	    {
	    	String fullPreparedSetString = value;
	    	String [] preparedSetStringArray = fullPreparedSetString.split("|");
	    	int count = 0;
	    	for(String x : preparedSetStringArray)
	    	{
	    		try 
	    		{
					preparedStatement.setString(count, x);
				} 
	    		catch (SQLException e) 
				{
						e.printStackTrace();
				}
		    		
	    	}
	   }
		    
		public static Connection getConnection() {
			return connection;
		}

		public static void setConnection(Connection value) {
			connection = value;
		}

		public static Properties fluidhifiProperties = new Properties();

		public static void setUrlString(String value)
		{
			url = value;
		}
		
		public static String getUrl()
		{
			return url;
		}
		
		public static void setDbClassName(String value)
		{
			dbClassName = value;
		}
		
		public static String getDbClassName()
		{
			return dbClassName;
		}
		
		 public static String getUserName()
		 {
		    return userName;
		 }

		 public static void setUserName(String value)
		 {
		    userName = value;
		 }

		 public static String getPassword()
		 {
		    return password;
		 }

		 public static void setPassword(String value)
		 {
		    password = value;
		 }
		   
		 public static void setUserInput(String value)
		 {
		    userInput = value;
		 }

		 public static String getUserInput()
		 {
		    return userInput;
		 }

		protected Short getAfternoon() {
			return afternoon;
		}
		protected void setAfternoon(Short afternoon) {
			this.afternoon = afternoon;
		}
		protected String getAfternoonStart() {
			return afternoonStart;
		}
		protected void setAfternoonStart(String afternoonStart) {
			this.afternoonStart = afternoonStart;
		}
		protected Integer getComputer() {
			return computer;
		}
		protected void setComputer(Integer computer) {
			this.computer = computer;
		}
		protected String getComputerName() {
			return computerName;
		}
		protected void setComputerName(String computerName) {
			this.computerName = computerName;
		}
		protected Short getDawn() {
			return dawn;
		}
		protected void setDawn(Short dawn) {
			this.dawn = dawn;
		}
		protected String getDawnEnd() {
			return dawnEnd;
		}
		protected void setDawnEnd(String dawnEnd) {
			this.dawnEnd = dawnEnd;
		}
		protected Short getDusk() {
			return dusk;
		}
		protected void setDusk(Short dusk) {
			this.dusk = dusk;
		}
		protected String getDuskEnd() {
			return duskEnd;
		}
		protected void setDuskEnd(String duskEnd) {
			this.duskEnd = duskEnd;
		}
		
		protected String getDuskStart() {
			return duskStart;
		}
		protected void setDuskStart(String duskStart) {
			this.duskStart = duskStart;
		}
		public static String getErrorMessage() {
			return errorMessage;
		}
		public static void setErrorMessage(String value) {
			errorMessage = value;
		}
		public static String getMessage() {
			return message;
		}
		public static void setMessage(String value)
		{
			message = value;
		}
		protected Short getEvening() {
			return evening;
		}
		protected void setEvening(Short evening) {
			this.evening = evening;
		}
		protected String getEveningEnd() {
			return eveningEnd;
		}
		protected void setEveningEnd(String eveningEnd) {
			this.eveningEnd = eveningEnd;
		}
		protected String getEveningStart() {
			return eveningStart;
		}
		protected void setEveningStart(String eveningStart) {
			this.eveningStart = eveningStart;
		}
		protected String getDirectoryPath() {
			return directoryPath;
		}
		protected void setDirectoryPath(String directoryPath) {
			this.directoryPath = directoryPath;
		}
		protected ArrayList<String> getSystemDrives() {
			return systemDrives;
		}
		protected void setSystemDrives(ArrayList<String> systemDrives) {
			this.systemDrives = systemDrives;
		}
		protected String getFile() {
			return file;
		}
		protected void setFile(String file) {
			this.file = file;
		}
		protected String getMidnight() {
			return midnight;
		}
		protected void setMidnight(String midnight) {
			this.midnight = midnight;
		}
		protected String getMorning() {
			return morning;
		}
		protected void setMorning(String morning) {
			this.morning = morning;
		}
		protected String getNight() {
			return night;
		}
		protected void setNight(String night) {
			this.night = night;
		}
		protected String getNightEnd() {
			return nightEnd;
		}
		protected void setNightEnd(String nightEnd) {
			this.nightEnd = nightEnd;
		}
		protected String getNoon() {
			return noon;
		}
		protected void setNoon(String noon) {
			this.noon = noon;
		}
		protected Byte getTimeElapsed() {
			return TimeElapsed;
		}
		protected void setTimeElapsed(Byte timeElapsed) {
			TimeElapsed = timeElapsed;
		}
		protected Integer getTimeLapse() {
			return timeLapse;
		}
		protected void setTimeLapse(Integer timeLapse) {
			this.timeLapse = timeLapse;
		}
		protected Byte getTimeRemaining() {
			return timeRemaining;
		}
		protected void setTimeRemaining(Byte timeRemaining) {
			this.timeRemaining = timeRemaining;
		}
		protected String getTimeStampCommand() {
			return timeStampCommand;
		}
		protected void setTimeStampCommand(String timeStampCommand) {
			this.timeStampCommand = timeStampCommand;
		}
		protected Boolean getExists() {
			return exists;
		}
		protected void setExists(Boolean exists) {
			this.exists = exists;
		}
		protected String getExtension() {
			return extension;
		}
		protected void setExtension(String extension) {
			this.extension = extension;
		}
		protected Integer getFieldCount() {
			return fieldCount;
		}
		protected void setFieldCount(Integer fieldCount) {
			this.fieldCount = fieldCount;
		}
		protected Integer getFields() {
			return fields;
		}
		protected void setFields(Integer fields) {
			this.fields = fields;
		}
		protected Integer getCharacterCount() {
			return characterCount;
		}
		protected void setCharacterCount(Integer characterCount) {
			this.characterCount = characterCount;
		}
		public static Integer getLength() {
			return length;
		}
		public static void setLength(Integer value) {
			length = value;
		}
		protected String getLibrary() {
			return library;
		}
		protected void setLibrary(String library) {
			this.library = library;
		}
		protected Integer getMedian() {
			return median;
		}
		protected void setMedian(Integer median) {
			this.median = median;
		}
		protected Integer getNowPlaying() {
			return nowPlaying;
		}
		protected void setNowPlaying(Integer nowPlaying) {
			this.nowPlaying = nowPlaying;
		}
		protected Integer getOIndex() {
			return OIndex;
		}
		protected void setOIndex(Integer oIndex) {
			OIndex = oIndex;
		}
		protected String getOldPosition() {
			return oldPosition;
		}
		protected void setOldPosition(String oldPosition) {
			this.oldPosition = oldPosition;
		}
		protected ArrayList<String> getOldUser() {
			return oldUser;
		}
		protected void setOldUser(ArrayList<String> oldUser) {
			this.oldUser = oldUser;
		}
		protected Integer getOP() {
			return OP;
		}
		protected void setOP(Integer oP) {
			OP = oP;
		}
		protected Integer getOrginalIndex() {
			return orginalIndex;
		}
		protected void setOrginalIndex(Integer orginalIndex) {
			this.orginalIndex = orginalIndex;
		}
		protected Integer getDecision() {
			return decision;
		}
		protected void setDecision(Integer decision) {
			this.decision = decision;
		}
		protected ArrayList<String> getPaths() {
			return paths;
		}
		protected void setPaths(ArrayList<String> paths) {
			this.paths = paths;
		}
		protected Integer getPlaying() {
			return playing;
		}
		protected void setPlaying(Integer playing) {
			this.playing = playing;
		}
		protected Integer getMuteState() {
			return muteState;
		}
		protected void setMuteState(Integer muteState) {
			this.muteState = muteState;
		}
		protected ArrayList<String> getDataRow() {
			return dataRow;
		}
		protected void setDataRow(ArrayList<String> dataRow) {
			this.dataRow = dataRow;
		}
		protected ArrayList<String> getTemp() {
			return temp;
		}
		protected void setTemp(ArrayList<String> temp) {
			this.temp = temp;
		}
		protected ArrayList<String> getTempArray() {
			return tempArray;
		}
		protected void setTempArray(ArrayList<String> tempArray) {
			this.tempArray = tempArray;
		}
		protected String getTempitem() {
			return tempitem;
		}
		protected void setTempitem(String tempitem) {
			this.tempitem = tempitem;
		}
		protected ArrayList<String> getTempres() {
			return tempres;
		}
		protected void setTempres(ArrayList<String> tempres) {
			this.tempres = tempres;
		}
		protected Integer getTotalSelectedItems() {
			return totalSelectedItems;
		}
		protected void setTotalSelectedItems(Integer totalSelectedItems) {
			this.totalSelectedItems = totalSelectedItems;
		}
		protected char getTrim() {
			return trim;
		}
		protected void setTrim(char trim) {
			this.trim = trim;
		}
		protected Integer getUpperBound() {
			return upperBound;
		}
		protected void setUpperBound(Integer upperBound) {
			this.upperBound = upperBound;
		}
		protected String getFileStream() {
			return this.fileStream;
		}
		protected void setFileStream(String fileStream) {
			this.fileStream = fileStream;
		}
		public static String getReplace() {
			return replace;
		}
		public static void setReplace(String value) {
			replace = value;
		}
		
		public static String getMrSwappy()
		{
			return mrSwappy;
		}
		
		public static void setMrSwappy(String value)
		{
			mrSwappy = value;
		}
		
		public static String getCharNewString()
		{
			return newCharString;
		}
		
		public static void addToNewCharString(char value)
		{
			newCharString = newCharString + value;
		}
		
		public static Integer getSubStringStart()
		{
			return subStringStart;
		}
		
		public static void setSubStringStart(Integer value)
		{
			subStringStart = value;
		}
		
		public static void incrementSubStringStartLength()
		{
			subStringStart = subStringStart + 1;
		}
		
		public static void decrementSubStringStartLength()
		{
			subStringStart = subStringStart - 1;
		}
		
		public static String getPropertiesString()
		{
			return propertiesString;
		}
		
		public static void setPropertiesString(String value)
		{
			propertiesString = value;
		}

		public static void clearCurrentMediaPath() 
		{
			currentMediaPath = "";
		}
		
		public static String getCurrentMediaPath()
		{
			return currentMediaPath;
		}
		
		public static void setCurrentMediaPath(String value)
		{
			currentMediaPath = value;
		}
}