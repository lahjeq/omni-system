/**
 * 
 */
package queryhouse;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import queryhouse.SystemMethods;
import core.Core;
import mysqlDataConnection.jdbcConnection;


/**
 * @author Billy Bissic
 *
 */
public class OmniCRUDService {
 
    public static String runQuerySelectArtistKeyWithNoDatabase(String value) throws ClassNotFoundException, SQLException
    {
    	ResultSet rs = null;
    	String artistKey = null;
    	try 
    	{
    		rs = jdbcConnection.runSelectQueryUsingJDBCConnection(value, true);
    		
    		while(rs.next())
    		{
    			artistKey = rs.getString("artist_key");
    		}
    		System.out.println("Artist Key: " + artistKey);
       		
    	}
    	    	
    	catch (ClassNotFoundException | SQLException e)
    	{
    		SystemMethods.setErrorMessage(e.getMessage() 
    				+ "Prepared setString: " + SystemMethods.getPreparedSetString());
    		// TODO Add logging functionality
    		//return SystemMethods.getErrorMessage();*/
    		//return artistKey;
    	} 
    	finally
    	{
    		jdbcConnection.closeJdbcConnection();
    	}
    	
    	return artistKey;
    }
    
    public static String runQuerySelectPathKeyWithNoDatabase(String value) throws ClassNotFoundException, SQLException
    {
    	ResultSet rs = null;
    	String pathKey = null;
    	try 
    	{
    		rs = jdbcConnection.runSelectQueryUsingJDBCConnection(value, true);
     		
    		while(rs.next())
    		{
    			pathKey = rs.getString("path_key");
    		}
    		System.out.println("Path Key: " + pathKey);
    		
    	}
	
    	catch (ClassNotFoundException | SQLException e)
    	{
    		SystemMethods.setErrorMessage(e.getMessage() 
    				+ "Prepared setString: " + SystemMethods.getPreparedSetString());
    		//return SystemMethods.getErrorMessage();
    		
    	}
    	finally
    	{
    		jdbcConnection.closeJdbcConnection();
    	}
    	
    	return pathKey;
    }
    
    public static String runQuerySelectAlbumKeyWithNoDatabase(String value) throws ClassNotFoundException, SQLException
    {
    	String albumKey = null;
    	ResultSet rs = null;
    	try 
    	{
    		rs = jdbcConnection.runSelectQueryUsingJDBCConnection(value, true);
    		
    		while(rs.next())
    		{
    			albumKey = rs.getString("album_key");
    		}
    		
    		System.out.println("Album Key: " + albumKey);
       	}
	   	catch (SQLException e)
    	{
    		//stmt.close();
    		SystemMethods.setErrorMessage(e.getMessage() 
    				+ "Prepared setString: " + SystemMethods.getPreparedSetString());
    		//return SystemMethods.getErrorMessage();
       	}
    	finally
    	{
    		jdbcConnection.closeJdbcConnection();
    	}
    	return albumKey;
    }
}

