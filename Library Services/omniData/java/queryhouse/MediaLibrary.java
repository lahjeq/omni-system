package queryhouse;

import java.util.*;

public class MediaLibrary {
	private static LinkedList currentPlayList = new LinkedList();
	private static String mp3 = "mp3";
	private static String ogg = "ogg";
	private static String wma = "wma";
	private static String wav = "wav";
	private static String flac = "flac";
	private static String mp4 = "mp4";
	private static String m4a = "m4a";
	String	album;
	ArrayList<String> albums = new ArrayList<String>();
	Integer	albumCount;
	String	albumDir;
	String	albumKey;
	String	albumName;
	ArrayList<Boolean> albumPlayedBoolean = new ArrayList<Boolean>();
	Integer	albumPlayedBooleanValue;
	Integer	albumPosition;
	String	artist;
	Short	artistCount;
	String	artistKey;
	private static String artistName;
	String	databaseAlbumName;
	Integer	databaseConnection;
	String	databaseFileName;
	String	databasePath;
	Integer	fullAlbum;
	String	fullPath;
	String	fileName;
	String	genre;
	String  item;
	ArrayList<String> genres = new ArrayList<String>();
	private static String original;
	String	originalAlbum;
	private static String	originalArtist;
	String	originalFile;
	String  path;
	String  pathKey;
	String  pathLink;
	private static String replace;
	String	track;
	Integer	trackCount;
	Integer	trackEntries;
	Float	trackLength;
	//StreamReader	trackReader
	ArrayList<String> tracks = new ArrayList<String>();
	String	trackTime;
	ArrayList<String> trackTimes = new ArrayList<String>();
	String	trackTitle;
	ArrayList<String> trackTitles = new ArrayList<String>();
	private static String search;
	ArrayList<String> searches = new ArrayList<String>();
	ArrayList<String> searchResults = new ArrayList<String>();
	String	searchType;
	private static String currentMediaPath;
	
	public static String getCurrentMediaPath() {
		return currentMediaPath;
	}
	public static void setCurrentMediaPath(String currentMediaPath) {
		MediaLibrary.currentMediaPath = currentMediaPath;
	}
	protected String getAlbum() {
		return album;
	}
	protected void setAlbum(String album) {
		this.album = album;
	}
	protected ArrayList<String> getAlbums() {
		return albums;
	}
	protected void setAlbums(ArrayList<String> albums) {
		this.albums = albums;
	}
	protected Integer getAlbumCount() {
		return albumCount;
	}
	protected void setAlbumCount(Integer albumCount) {
		this.albumCount = albumCount;
	}
	protected String getAlbumDir() {
		return albumDir;
	}
	protected void setAlbumDir(String albumDir) {
		this.albumDir = albumDir;
	}
	protected String getAlbumKey() {
		return albumKey;
	}
	protected void setAlbumKey(String albumKey) {
		this.albumKey = albumKey;
	}
	protected String getAlbumName() {
		return albumName;
	}
	protected void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
	protected ArrayList<Boolean> getAlbumPlayedBoolean() {
		return albumPlayedBoolean;
	}
	protected void setAlbumPlayedBoolean(ArrayList<Boolean> albumPlayedBoolean) {
		this.albumPlayedBoolean = albumPlayedBoolean;
	}
	protected Integer getAlbumPlayedBooleanValue() {
		return albumPlayedBooleanValue;
	}
	protected void setAlbumPlayedBooleanValue(Integer albumPlayedBooleanValue) {
		this.albumPlayedBooleanValue = albumPlayedBooleanValue;
	}
	protected Integer getAlbumPosition() {
		return albumPosition;
	}
	protected void setAlbumPosition(Integer albumPosition) {
		this.albumPosition = albumPosition;
	}
	protected String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	protected Short getArtistCount() {
		return artistCount;
	}
	protected void setArtistCount(Short artistCount) {
		this.artistCount = artistCount;
	}
	protected String getArtistKey() {
		return artistKey;
	}
	protected void setArtistKey(String artistKey) {
		this.artistKey = artistKey;
	}
	public static String getArtistName() {
		return artistName;
	}
	public static void setArtistName(String value) {
		artistName = value;
	}
	protected String getDatabaseAlbumName() {
		return databaseAlbumName;
	}
	protected void setDatabaseAlbumName(String databaseAlbumName) {
		this.databaseAlbumName = databaseAlbumName;
	}
	protected Integer getDatabaseConnection() {
		return databaseConnection;
	}
	protected void setDatabaseConnection(Integer databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	protected String getDatabaseFileName() {
		return databaseFileName;
	}
	protected void setDatabaseFileName(String databaseFileName) {
		this.databaseFileName = databaseFileName;
	}
	protected String getDatabasePath() {
		return databasePath;
	}
	protected void setDatabasePath(String databasePath) {
		this.databasePath = databasePath;
	}
	protected Integer getFullAlbum() {
		return fullAlbum;
	}
	protected void setFullAlbum(Integer fullAlbum) {
		this.fullAlbum = fullAlbum;
	}
	protected ArrayList<String> getGenres() {
		return genres;
	}
	protected void setGenres(ArrayList<String> genres) {
		this.genres = genres;
	}
	protected Float getTrackLength() {
		return trackLength;
	}
	protected void setTrackLength(Float trackLength) {
		this.trackLength = trackLength;
	}
	protected String getFullPath() {
		return fullPath;
	}
	protected void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	protected String getFileName() {
		return fileName;
	}
	protected void setFileName(String fileName) {
		this.fileName = fileName;
	}
	protected String getGenre() {
		return genre;
	}
	protected void setGenre(String genre) {
		this.genre = genre;
	}
	protected String getItem() {
		return item;
	}
	protected void setItem(String item) {
		this.item = item;
	}
	public static String getOriginal() {
		return original;
	}
	
	public static void setOriginal(String value) {
		original = value;
	}
	
	protected String getOriginalAlbum() {
		return originalAlbum;
	}
	protected void setOriginalAlbum(String originalAlbum) {
		this.originalAlbum = originalAlbum;
	}
	public static String getOriginalArtist() {
		return originalArtist;
	}
	public static void setOriginalArtist(String value) {
		originalArtist = value;
	}
	protected String getOriginalFile() {
		return originalFile;
	}
	protected void setOriginalFile(String originalFile) {
		this.originalFile = originalFile;
	}
	protected String getPath() {
		return path;
	}
	protected void setPath(String path) {
		this.path = path;
	}
	protected String getPathKey() {
		return pathKey;
	}
	protected void setPathKey(String pathKey) {
		this.pathKey = pathKey;
	}
	protected String getPathLink() {
		return pathLink;
	}
	protected void setPathLink(String pathLink) {
		this.pathLink = pathLink;
	}
	public static void setReplace(String value)
	{
		replace = value;
	}
	
	public static String getReplace()
	{
		return replace;
	}
	
	protected String getTrack() {
		return track;
	}
	protected void setTrack(String track) {
		this.track = track;
	}
	protected Integer getTrackCount() {
		return trackCount;
	}
	protected void setTrackCount(Integer trackCount) {
		this.trackCount = trackCount;
	}
	protected Integer getTrackEntries() {
		return trackEntries;
	}
	protected void setTrackEntries(Integer trackEntries) {
		this.trackEntries = trackEntries;
	}
	protected ArrayList<String> getTracks() {
		return tracks;
	}
	protected void setTracks(ArrayList<String> tracks) {
		this.tracks = tracks;
	}
	protected String getTrackTime() {
		return trackTime;
	}
	protected void setTrackTime(String trackTime) {
		this.trackTime = trackTime;
	}
	protected ArrayList<String> getTrackTimes() {
		return trackTimes;
	}
	protected void setTrackTimes(ArrayList<String> trackTimes) {
		this.trackTimes = trackTimes;
	}
	protected String getTrackTitle() {
		return trackTitle;
	}
	protected void setTrackTitle(String trackTitle) {
		this.trackTitle = trackTitle;
	}
	protected ArrayList<String> getTrackTitles() {
		return trackTitles;
	}
	protected void setTrackTitles(ArrayList<String> trackTitles) {
		this.trackTitles = trackTitles;
	}
	public static String getSearch() {
		return search;
	}
	public static void setSearch(String value) {
		search = value;
	}
	protected ArrayList<String> getSearches() {
		return searches;
	}
	protected void setSearches(ArrayList<String> searches) {
		this.searches = searches;
	}
	protected ArrayList<String> getSearchResults() {
		return searchResults;
	}
	protected void setSearchResults(ArrayList<String> searchResults) {
		this.searchResults = searchResults;
	}
	protected String getSearchType() {
		return searchType;
	}
	protected void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public static String getMp3() {
		return mp3;
	}
	public static String getOgg() {
		return ogg;
	}
	public static String getWma() {
		return wma;
	}
	public static String getWav() {
		return wav;
	}
	public static String getFlac() {
		return flac;
	}
	public static String getMp4() {
		return mp4;
	}
	public static String getM4a() {
		return m4a;
	}
	
	public static String stringReplacement(String name, String searchString, String replacmentString)
	{
		String finalString;
		finalString = name.replace(searchString, replacmentString);
		return finalString;
	}
	
	public static LinkedList  getPlaylistElements() {
		return currentPlayList;
	}
	
	public static void clearPlaylistElements() {
		currentPlayList.clear();
	}

	public static void addItemToCurrentPlaylist(String item) {
		currentPlayList.add(item);
	}
	
	public static void removeItemFromCurrentPlayList(String item) {
	    int index = currentPlayList.indexOf(item);
		if (currentPlayList.indexOf(item) !=-1) {
		   currentPlayList.remove(index);
		}
   }
 
}
