/**
 * 
 */
package queryhouse;
import java.util.Scanner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import queryhouse.OmniQueries;
import queryhouse.SystemMethods;
import queryhouse.MediaLibrary;
import core.Core;
import systemconfigurations.OmniProperties;
import systemconfigurations.SetUserPassword;
import mysqlDataConnection.jdbcConnection;

/**
 * @author Billy Bissic
 *
 */
public class InitalizeDatabase {		 
	/**
	 * @param args
	 */
	public static void initializeDatabaseInstall() throws ClassNotFoundException, SQLException {
		SystemMethods.setUserName(Core.getCurrentUserName());
		System.out.println("Greetings and Welcome to Fluid Hifi!");
		System.out.println("");
		System.out.println("Database initilization is starting...");
	
		System.out.println("This appears to be the first time " + SystemMethods.getUserName() + " has been executed.");
		System.out.println("");
		System.out.println("This installation should be straight forward. ");
		System.out.println("Please follow the instructions as they appear. Enjoy! ;D");
		System.out.println("");
		System.out.println("");
		System.out.println("***Note: The following prompts are for system use only.");
		System.out.println("***You will have an opportunity to set up personal accounts ");
		System.out.println("***later on during set up.");

		System.out.println("");
		System.out.println("");
		System.out.println("The current username for the system is : " + SystemMethods.getUserName());
		System.out.println("This is the name of your AI personality for the media player.");
		System.out.println("You are encouraged to customize your system as much as you would like.");
		
		System.out.println("");
		
		Scanner input = new Scanner(System.in);
		System.out.println("Would you like to keep this name as the system username? Y/N");
		SystemMethods.setUserInput(input.next());
		
		   if (((SystemMethods.getUserInput().equals("Y"))||(SystemMethods.getUserInput().equals("y")))) {
			  System.out.println("You chose to continue with the default system username : " + SystemMethods.getUserName());
		   }
		   else {
		      System.out.println("UserName : ");
		      SystemMethods.setUserName(input.next());
		   }
           
		   System.out.println("***Note: By allowing the system to generate a password the system will generate a password to follow security standards.");
		   System.out.println("***      This will allow Omni to maintain itself and remove any worrying about extra passwords. Passwords will ");
		   System.out.println("***      periodically changed to ensure security remains tight.");
		   System.out.println("Would you like " + SystemMethods.getUserName() + " to generate a system password now? Y/N" );

		   SystemMethods.setUserInput(input.next());
		   
		   if (((SystemMethods.getUserInput().equals("Y"))||(SystemMethods.getUserInput().equals("y")))) {
             //generate system password
		   }
		   else
		   {
			   while (!SetUserPassword.setUserPassword().equals("Success"))
			   {
				   SetUserPassword.setUserPassword();
			   }
		   }

		   System.out.println("Testing Database Connection...");
		   
		   try {
			  /* SystemMethods.setDbClassName("com.mysql.jdbc.Driver");
			   SystemMethods.setUrlString("jdbc:mysql://localhost:3306/?connectTimeout=0&socketTimeout=0&autoReconnect=true");
			   
			 Connection c = DriverManager.getConnection(SystemMethods.getUrl(), Core.getCurrentMysqlUserId(), Core.getCurrentMysqlPassword());
			 Connection con =
			    		DriverManager.getConnection(
			    		Core.getMysqlConnectionUrl(), 
			    		Core.getCurrentMysqlUserId(), 
			    		Core.getCurrentMysqlPassword());
			 
			 OmniProperties.createProperties(
					 Core.getCurrentMysqlUserId(), 
					 Core.getCurrentMysqlPassword(),
					 SystemMethods.getUrl(),
					 SystemMethods.getDbClassName());
			
			 
		     System.out.println("Pass");*/
		     
		     jdbcConnection.readConnectionConfiguration();
		     System.out.println("Beginning to install " + Core.getCurrentMysqlUserId() + "'s database...");
		     System.out.println("Creating databases");
		     
		     OmniQueries fluidHifiQueries = new OmniQueries();
		     jdbcConnection.dropDatabaseUsingJDBCConnection(fluidHifiQueries.getQueryDropMp3Database());
		     //queryhouse.OmniCRUDService.runDropDatabaseQuery(fluidHifiQueries.getQueryDropMp3Database());
		     jdbcConnection.createDatabaseUsingJDBCConnection(fluidHifiQueries.getQueryCreateMp3Database());
		     //queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateMp3Database());
		     //System.out.println(fluidHifiQueries.getQueryCreateMp3Database());
		     jdbcConnection.dropDatabaseUsingJDBCConnection(fluidHifiQueries.getQueryDropTracksDatabase());
		     //queryhouse.OmniCRUDService.runDropDatabaseQuery(fluidHifiQueries.getQueryDropTracksDatabase());
		     jdbcConnection.createDatabaseUsingJDBCConnection(fluidHifiQueries.getQueryCreateTracksDatabase());
		     //queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateTracksDatabase());
		     //System.out.println(fluidHifiQueries.getQueryCreateTracksDatabase());
		     //queryhouse.FluidHifiCRUDService.runDropDatabaseQuery(fluidHifiQueries.getQueryDropHifiLibrary());
		     //queryhouse.FluidHifiCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateHifiLibrary());
		     jdbcConnection.dropDatabaseUsingJDBCConnection(fluidHifiQueries.getQueryDropAccountDatabase());
		     jdbcConnection.createDatabaseUsingJDBCConnection(fluidHifiQueries.getQueryCreateAccountDatabase());
		     //queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateAccountDatabase());
		     //queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateEnvironmentSettingsDatabase());
		     //System.out.println(fluidHifiQueries.getQueryCreateHifiLibrary());
		     
		     
		     System.out.println("Completed creating databses");
		     System.out.println("Adding database tables");
		     
		     jdbcConnection.createTableUsingJDBCConnection(fluidHifiQueries.getQueryCreateAlbumTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateAlbumTable());
		     jdbcConnection.createTableUsingJDBCConnection(fluidHifiQueries.getQueryCreateArtistTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateArtistTable());
		     jdbcConnection.createTableUsingJDBCConnection(fluidHifiQueries.getQueryCreateDrivesTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateDrivesTable());
		     jdbcConnection.createTableUsingJDBCConnection(fluidHifiQueries.getQueryCreateGroupsTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGenresTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGenresTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGroupsTable());
		     jdbcConnection.createTableUsingJDBCConnection(fluidHifiQueries.getQueryCreatePathTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreatePasswordsTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreatePathTable());
		     jdbcConnection.createTableUsingJDBCConnection(fluidHifiQueries.getQueryCreateUsersTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserGroupsTable());
		    // queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUsersTable());
		     
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateAccountsTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateAccountTypesTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateAccountPricingTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserLoginAttemptsTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateSecurityQuestionTable());
		     //queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateAccountDatabase());
		     
		     //System.out.println(fluidHifiQueries.getQueryCreateAlbumTable());
		     //System.out.println(fluidHifiQueries.getQueryCreateArtistTable());
		     //System.out.println(fluidHifiQueries.getQueryCreateDrivesTable());
		     //System.out.println(fluidHifiQueries.getQueryCreateGenresTable());
		     //System.out.println(fluidHifiQueries.getQueryCreateGroupsTable());
		     //System.out.println(fluidHifiQueries.getQueryCreatePasswordsTable());
		     //System.out.println(fluidHifiQueries.getQueryCreatePathTable());
		     //System.out.println(fluidHifiQueries.getQueryCreateUserGroupsTable());
		     //System.out.println(fluidHifiQueries.getQueryCreateUsersTable());
		     
/*		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateCustomerRelationsDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateCustomerRelationsDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateErrorsDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateErrorsDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateEventsDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateEventsDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateGenresDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateGenresDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateLanguagesDatabases());
		     System.out.println(fluidHifiQueries.getQueryCreateLanguagesDatabases());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateMenuRoutingDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateMenuRoutingDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateMessagesDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateMessagesDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateSessionsDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateSessionsDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateSourcesDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateSourcesDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateStatusesDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateStatusesDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateUserMediaDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateUserMediaDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateUserMediaDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateUserMediaDatabase());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateWarningsDatabase());
		     System.out.println(fluidHifiQueries.getQueryCreateWarningsDatabase());
		     
		     System.out.println(fluidHifiQueries.getQueryCreateCustomerBookingInquiresTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateCustomerBookingInquiresTable());
		     System.out.println(fluidHifiQueries.getQueryCreateCustomerMailingListTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateCustomerMailingListTable());
		     System.out.println(fluidHifiQueries.getQueryCreateServiceTypesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateServiceTypesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateSystemErrorCodesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateSystemErrorCodesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUploadErrorCodesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUploadErrorCodesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateSystemEventCodesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateSystemEventCodesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUploadEventCodesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUploadEventCodesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateLanguageCodesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateLanguageCodesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMenuCustomTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMenuCustomTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMenuLinksTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMenuLinksTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMenuRouterTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMenuRouterTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserSessionsTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserSessionsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUploadStashSourcesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUploadStashSourcesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMediaStatusTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMediaStatusTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUploadStatusTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUploadStatusTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserStatusTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserStatusTable());
		     System.out.println(fluidHifiQueries.getQueryCreateCaptionTypesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateCaptionTypesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateCaptionsTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateCaptionsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateContainerTypesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateContainerTypesTable());
		     System.out.println(fluidHifiQueries.getQuearyCreateDisplayObjectTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQuearyCreateDisplayObjectTable());
		     System.out.println(fluidHifiQueries.getQueryCreateDisplayObjectTypesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateDisplayObjectTypesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateFileArchiveTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateFileArchiveTable());
		     System.out.println(fluidHifiQueries.getQueryCreateFilesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateFilesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleriesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleriesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleryChildrenTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleryChildrenTable());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleryContainerImageTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleryContainerImageTable());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleryContainersTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleryContainersTable());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleryGroupTypes());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleryGroupTypes());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleryItems());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleryItems());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleryPermissionsTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleryPermissionsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateGalleryStatsTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateGalleryStatsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateImageEffects());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateImageEffects());
		     System.out.println(fluidHifiQueries.getQueryCreateImageLinksTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateImageLinksTable());
		     System.out.println(fluidHifiQueries.getQueryCreateImageStylesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateImageStylesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateImagesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateImagesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMediaTypesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMediaTypesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMediaUsersTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMediaUsersTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMetaItemTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMetaItemTable());
		     System.out.println(fluidHifiQueries.getQueryCreateMimeTypeCategoryTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateMimeTypeCategoryTable());
		     System.out.println(fluidHifiQueries.getQueryCreateOldImageTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateOldImageTable());
		     System.out.println(fluidHifiQueries.getQueryCreateOldMetaItemsTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateOldMetaItemsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateSpeakersTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateOldMetaItemsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateSpeakersVideosTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateSpeakersVideosTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUploadStashTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUploadStashTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUploadsTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUploadsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserImagesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserImagesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserQuotaTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserQuotaTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserVideosTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserVideosTable());
		     System.out.println(fluidHifiQueries.getQueryCreateVideosTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateVideosTable());
		     System.out.println(fluidHifiQueries.getQueryCreatePrivilegeTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreatePrivilegeTable());
		     System.out.println(fluidHifiQueries.getQueryCreateProfileAttributeTypeTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateProfileAttributeTypeTable());
		     System.out.println(fluidHifiQueries.getQueryCreateProfileAttributesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateProfileAttributesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateProfilesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateProfilesTable());
		     System.out.println(fluidHifiQueries.getQeueryCreateRelationshipTypes());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQeueryCreateRelationshipTypes());
		     System.out.println(fluidHifiQueries.getQueryCreateRelationshipsTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateRelationshipsTable());
		     System.out.println(fluidHifiQueries.getQueryCreateRolePrivilegesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateRolePrivilegesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateRolesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateRolesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserAddressTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserAddressTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserDataTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserDataTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserIdentityTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserIdentityTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUserRolesTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUserRolesTable());
		     System.out.println(fluidHifiQueries.getQueryCreateUsersTable());
		     queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(fluidHifiQueries.getQueryCreateUsersTable());
		     System.out.println(fluidHifiQueries.getQueryCreateWarningCodesTable());
		     queryhouse.OmniCRUDService.runCreateDatabaseQuery(fluidHifiQueries.getQueryCreateWarningCodesTable());*/
		     
		     System.out.println("Completed creating database tables.");
		     
		     //c.close();
		   }
		   catch (SQLException e) {
			   System.out.println(SystemMethods.getUserName());
			   System.out.println(SystemMethods.getPassword());
			   System.out.println("SQLException: " + e.getMessage());
			   System.out.println("SQLState: " + e.getSQLState());
			   System.out.println("VendorError: " + e.getErrorCode());
		   }
	 	   
	}
	
}
