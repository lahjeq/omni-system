/**
 * 
 */
package queryhouse;
/**
 * @author Billy Bissic
 *
 */
public class OmniQueries extends MediaLibrary {

      private String queryCreateMp3Database = "CREATE DATABASE mp3;";
   
      public String getQueryCreateMp3Database()
      {
         return this.queryCreateMp3Database;
      }

      private String queryDropMp3Database = "DROP DATABASE mp3;";
      
      public String getQueryDropMp3Database()
      {
    	  return this.queryDropMp3Database;
      }
      
      private String queryCreateTracksDatabase = "CREATE DATABASE tracks;";

      public String getQueryCreateTracksDatabase()
      {
         return this.queryCreateTracksDatabase;
      }

      private String queryDropTracksDatabase = "DROP DATABASE tracks;";
      
      public String getQueryDropTracksDatabase()
      {
         return this.queryDropTracksDatabase;
      }
      
      /*private String queryCreateHifiLibrary = "CREATE DATABASE hifi_library;";
   
      public String getQueryCreateHifiLibrary()
      {
         return this.queryCreateHifiLibrary;
      }*/
      
      //private String queryDropHifiLibrary = "DROP DATABASE hifi_library;";
      
      /*public String getQueryDropHifiLibrary()
      {
    	  return this.queryDropHifiLibrary;
      }*/

      private String queryCreateArtistTable = "CREATE TABLE mp3.artists ( "
         + "artist_key INT(255) NOT NULL AUTO_INCREMENT, "
         + "artistName VARCHAR(255) DEFAULT NULL, "
         + "artist_name VARCHAR(255) DEFAULT NULL, "
         + "PRIMARY KEY (artist_key), "
         + "UNIQUE KEY artistName (artistName), "
         + "UNIQUE KEY artist_name (artist_name) "
         + ") ENGINE=InnoDB DEFAULT CHARSET=latin1 "
         + "COMMENT='Houses the names of the artists.' "
         + "AUTO_INCREMENT=0;";

      public String getQueryCreateArtistTable()
      {
         return this.queryCreateArtistTable;
      }

      private String queryCreateAlbumTable = "CREATE TABLE mp3.album (" 
              + "album_key INT(255) NOT NULL AUTO_INCREMENT," 
              + "albumName VARCHAR(255) NOT NULL default ''," 
              + "dbalbumName VARCHAR(255) NOT NULL default ''," 
              + "artist_link VARCHAR(255) NOT NULL default ''," 
              + "genre_link VARCHAR(255) NOT NULL default ''," 
              + "art_link VARCHAR(255) NOT NULL default ''," 
              + "PRIMARY KEY (album_key)," 
              + "UNIQUE KEY albumName (albumName)" 
              + ") ENGINE=InnoDB DEFAULT CHARSET=latin1 " 
              + "COMMENT='Houses the album information and linked " 
              + "tables associated wi' " 
              + "AUTO_INCREMENT=0;";
              
      public String getQueryCreateAlbumTable()
      {
    	  return this.queryCreateAlbumTable;
      }
      private String queryCreateDrivesTable = "CREATE TABLE mp3.drives ( "
         + "drive_index INT(10) unsigned NOT NULL AUTO_INCREMENT, "
         + "OWNER VARCHAR(45) NOT NULL, "
         + "group_id VARCHAR(45) NOT NULL, "
         + "public VARCHAR(1) NOT NULL, "
         + "path VARCHAR(45) NOT NULL, "
         + "name VARCHAR(45) NOT NULL, "
         + "PRIMARY KEY (drive_index), "
         + "UNIQUE KEY path (path) "
         + ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1; ";

      public String getQueryCreateDrivesTable()
      {
         return this.queryCreateDrivesTable;
      }

      private String queryCreateGroupsTable = "CREATE TABLE useraccounts.groups ( "
         + "group_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, "
         + "group_name VARCHAR(45) NOT NULL, "
         + "group_desc TEXT, "
         + "PRIMARY KEY(group_id) "
         + ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1; ";

      public String getQueryCreateGroupsTable()
      {
         return this.queryCreateGroupsTable;
      }

      /*private String queryCreateUsersTable = "CREATE TABLE useraccounts.users ( "
          + "userid INT(10) UNSIGNED NOT NULL AUTO_INCREMENT, "
          + "username VARCHAR(45) NOT NULL, "
          + "password VARCHAR(45) NOT NULL, "
          + "PRIMARY KEY (userid) "
          + ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1; ";
*/
      /*private String queryCreatePasswordsTable = "CREATE TABLE hifi_library.passwords ( "
          + "user_links VARCHAR(255) NOT NULL, "
          + "password VARCHAR(255) NOT NULL, "
          + "PRIMARY KEY (user_links) "
          + ") ENGINE=InnoDB DEFAULT CHARSET=latin1; ";

      public String getQueryCreatePasswordsTable()
      {
         return this.queryCreatePasswordsTable;
      }

      private String queryCreateUserGroupsTable = "CREATE TABLE hifi_library.user_groups ( "
          + "group_id INT(10) UNSIGNED NOT NULL, "
          + "userid VARCHAR(45) NOT NULL "
          + ") ENGINE=InnoDB DEFAULT CHARSET=latin1; ";

      public String getQueryCreateUserGroupsTable()
      {
         return this.queryCreateUserGroupsTable;
      }

      public String getQueryCreateGenresTable()
      {
         return this.queryCreateGenresTable;
      }*/

      private String queryCreatePathTable = "CREATE TABLE mp3.path ( "
         + "path_key INT(255) NOT NULL AUTO_INCREMENT, "
         + "album_link VARCHAR(255) NOT NULL DEFAULT '', "
         + "parentDir VARCHAR(255) NOT NULL DEFAULT '', "
         + "albumDir VARCHAR(255) NOT NULL DEFAULT '', "
         + "album VARCHAR(255) NOT NULL DEFAULT '', "
         + "PRIMARY KEY (path_key), "
         + "UNIQUE KEY albumDir (albumDir) "
         + ") ENGINE=InnoDB DEFAULT charset=latin1 "
         + "AUTO_INCREMENT=0; ";

      public String getQueryCreatePathTable()
      {
         return this.queryCreatePathTable;
      }
      
      public static String querySelectAlbumKey(String albumName, String artistKey)
      {
    	  String query = "SELECT album_key FROM mp3.album "
	          + "WHERE albumName = '" + albumName + "'"
              + " AND artist_link = '" + artistKey + "';";
    	  return query;
      }
      
//      public String getQuerySelectAlbumKey()
//      {
//    	  return this.querySelectAlbumKey;
//      }

      public static String querySelectArtistKey(String artistName)
      { 
    	  String query = "SELECT artist_key FROM mp3.artists "
              + "WHERE artist_name = '" + artistName + "';";
    	  return query;
      }
      
//      public String getQuerySelectArtistKey()
//      {
//    	  return this.querySelectArtistKey;
//      }

      public static String querySelectPathKey(String albumName)
      { 
    	  String query = "SELECT path_key FROM mp3.path WHERE album = '" 
              + albumName + "';";
    	  
    	  return query;
      
      }
//      public String getQuerySelectPathKey()
//      {
//    	  return this.querySelectPathKey;
//      }

      public static String queryInsertPath(String albumLink, String parentAlbumFilePath, String albumFilePath, String albumName) 
      {
    	  String query = "INSERT INTO mp3.path (album_link, parentDir, albumDir, album) "
              + "VALUES ( '" + albumLink + "','" + parentAlbumFilePath + "', '" + albumFilePath
              + "', '" + albumName + "');";
    	  return query;
      }
      
//      public String getQueryInsertPath()
//      {
//    	  return this.queryInsertPath;
//      }
                
      public static String queryInsertIntoAlbum(String albumName, String databaseFriendlyAlbumName, String subquery)
      { 
    	  String query= "INSERT INTO mp3.album (" 
    		  +	"albumName, " 
    		  + "dbalbumName, "
              + "artist_link, "
              + "genre_link, "
              + "art_link)"
              + "VALUES ('" 
              + albumName 
              + "', '" + databaseFriendlyAlbumName + "',"
              + "'" + subquery + "'" + ", '', '');";
    	  return query;
      }
      
//      public String getQueryInsertIntoAlbum()
//      {
//    	  return this.queryInsertIntoAlbum;
//      }
  
      public static String queryCreateAlbumTableInTracksDatabase(String albumName, String subquery)
      { 
    	  String query = "CREATE TABLE tracks." + albumName + " ("
              + "tracks_key INT(255) NOT NULL AUTO_INCREMENT,"
              + "artist_link VARCHAR(255) CHARACTER SET ascii NOT NULL default '',"
              + "album_link VARCHAR(255) CHARACTER SET ascii NOT NULL default '',"
              + "pathLink VARCHAR(255) CHARACTER SET ascii NOT NULL default '',"
              + "fileName VARCHAR(255) COLLATE utf8_general_mysql500_ci NOT NULL default '',"
              + "timesAccessed VARCHAR(255) CHARACTER SET ascii NOT NULL default '',"
              + "lastDate VARCHAR(255) CHARACTER SET ascii NOT NULL default '',"
              + "PRIMARY KEY (tracks_key)"
              + ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci "
              + "COMMENT='" + subquery + "_" + albumName
              + "' AUTO_INCREMENT=1;";
    	  return query;
      }

      private String queryCreateUserAccountsDatabase = "CREATE DATABASE useraccounts";

      public String getQueryCreateAccountDatabase()
      {
    	  return this.queryCreateUserAccountsDatabase;
      }
      
   
      
     /* private String queryCreateGenresTable = "CREATE TABLE genres.genres ( "
    		 + "genreID INT NOT NULL AUTO_INCREMENT,"
    		 + "genre VARCHAR(45) NOT NULL,"
    		 + "genreDescription VARCHAR(255) NOT NULL,"
    		 + "genreOrginId INT NULL COMMENT 'link to the genres.orgin table ',"
    		 + "PRIMARY KEY (genreID),"
    		 + "UNIQUE INDEX genre_UNIQUE (genre ASC))";
    		 
      private String queryCreateSecurityQuestionsTable = 
    		  "CREATE TABLE useraccounts.securityquestions ("
    		  + "securityQuestionId INT NOT NULL AUTO_INCREMENT," 
    		  + "secuirtyQestionLanguageCode VARCHAR(3) NOT NULL," 
    		  + "securityQuestionText VARCHAR(45) NOT NULL, PRIMARY KEY (securityQuestionId)," 
    		  + "UNIQUE INDEX secuirtyQestionLanguageCode_UNIQUE (secuirtyQestionLanguageCode ASC))" 
    		  + "COMMENT = 'holds security questions to retrieve password and user logins';";

      public String getQueryCreateSecurityQuestionTable()
      {
    	  return this.queryCreateSecurityQuestionsTable;
      }
      
      private String queryCreateAccountPricingTable = 
    		  "CREATE TABLE useraccounts.accountpricing ("
    		 + "accountpricecode INT NOT NULL, "
    	     + "accountprice DECIMAL NOT NULL, "
    		 + "PRIMARY KEY (accountpricecode))"
    	     + "COMMENT = 'user account membership pricing';";

      public String getQueryCreateAccountPricingTable()
      {
    	  return this.queryCreateAccountPricingTable;
      }
      
      private String queryCreateEnvironmentSettinigsDatabase = "CREATE SCHEMA envsettings;";

      public String getQueryCreateEnvironmentSettingsDatabase()
      {
    	  return this.queryCreateEnvironmentSettinigsDatabase;
      }
      
      private String queryCreateLanguagueCodesTable = 
    		  "CREATE TABLE envsettings.languagecodes ("
    		+ "languagecode INT NOT NULL, "
    		+ "languagecodetext VARCHAR(45) NOT NULL, "
    		+ "languagecodeabbreviation VARCHAR(5) NOT NULL, "
    		+ "PRIMARY KEY (languagecode), "
    		+ "UNIQUE INDEX languagecodeabbreviation_UNIQUE (languagecodeabbreviation ASC))"
    		+ "COMMENT = 'system language code';";
 
      public String getQueryCreateLanguageCodeTable()
      {
    	  return this.queryCreateLanguagueCodesTable;
      }
      
      private String queryCreateUserLoginAttemptsTable = 
    		  "CREATE TABLE useraccounts.userloginattempts ("
    		+ "username VARCHAR(45) NOT NULL, "
    		+ "lastsuccesfullogin DATETIME NOT NULL, "
    		+ "lastsuccessfulip VARCHAR(45) NOT NULL, "
    		+ "successfullogincount INT NOT NULL, "
    		+ "lastfailedlogin DATETIME NOT NULL, "
    		+ "lastfailedloginip VARCHAR(45) NOT NULL, "
    		+ "lastfailedlogincount INT NOT NULL, "
    	    + "PRIMARY KEY (username))"
    	    + "COMMENT = 'stores user account login attempts';";

      public String getQueryCreateUserLoginAttemptsTable()
      {
    	  return this.queryCreateUserLoginAttemptsTable;
      }
      
      private String queryCreateAccountsTable = 
    		  "CREATE TABLE useraccounts.accounts ("
    		+ "acountid INT NOT NULL AUTO_INCREMENT, " 
    		+ "firstname VARCHAR(45) NOT NULL, "
            + "lastname VARCHAR(45) NOT NULL, "
            + "birthday DATE NOT NULL, "
            + "emailaddress VARCHAR(45) NOT NULL, "
            + "phonenumber VARCHAR(45) NOT NULL, "
            + "password VARCHAR(45) NOT NULL, "
            + "zipcode VARCHAR(10) NOT NULL, "
            + "countrycode CHAR(3) NOT NULL, "
            + "userlanguage CHAR(30) NOT NULL, "
            + "firstsecurityquestionid VARCHAR(45) NOT NULL, "
            + "firstsecurityquestionanswer VARCHAR (45) NOT NULL, "
            + "secondsecurityquestionid VARCHAR(45) NOT NULL, "
            + "secondsecurityquestionanswer VARCHAR(45) NOT NULL, "
            + "accountcreatedate DATETIME NOT NULL,"
            + "PRIMARY KEY (acountid),"
            + "UNIQUE INDEX emailaddress_UNIQUE (emailaddress ASC))"
            + "COMMENT = 'main account information';";

      public String getQueryCreateAccountsTable()
      {
    	  return this.queryCreateAccountsTable;
      }

      private String queryCreateAccountTypesTable = 
    		  "CREATE TABLE useraccounts.accounttypes ("
    		+ "accounttypecode INT NOT NULL AUTO_INCREMENT, "
    		+ "accounttypedescription VARCHAR(45) NOT NULL, "
    		+ "accountpricecode INT(11) NOT NULL COMMENT " 
    		+ "'foriegn key to accountpricecode.accountpricing', "
            + "PRIMARY KEY (accounttypecode))"
            + "COMMENT = 'different account types to determine price';";
      
      public String getQueryCreateAccountTypesTable()
      {
    	  return this.queryCreateAccountTypesTable;
      }
      */
      public static String queryInsertAlbum(String albumName, String subqueryForArtistKey, String subqueryForAlbumKey, String subqueryforPathKey, String fileName)
      {
    		  String query = "INSERT INTO tracks." + albumName + " ( "
	          + "artist_link, album_link, pathLink, fileName, "
              + "timesAccessed, lastDate ) VALUES ('" + subqueryForArtistKey 
              + "', '" + subqueryForAlbumKey + "', '" + subqueryforPathKey + "', '" 
              + fileName + "', 0, '');";
    		  System.out.println(query);
    		  return query;
      }
//      public String getQueryInsertAlbum()
//      {
//    	  return this.queryInsertAlbum;
//      }

      public static String queryInsertArtist(String orginalArtist, String artist) 
      {
    	  String query = "INSERT INTO mp3.artists (artistName, artist_name) "
              + "VALUES ('" 
    		  + orginalArtist 
    		  + "' , '" 
    		  + artist 
              + "');";
    	  System.out.println(query);
    	  return query;
      }

      private String queryCountArtists = "SELECT COUNT(*) FROM mp3.artists";
      
      public String getQueryCountArtists()
      {
    	  return this.queryCountArtists;
      }

      private String queryDeleteStats = "DELETE FROM stats";

      public String getQueryDeleteStats()
      {
    	  return this.queryDeleteStats;
      }
      
      private String queryInsertStats = "INSERT INTO stats (artist_count, album_count, " 
              + "track_count, last_update_ts) VALUES (" + getArtistCount() 
              + ", " + getAlbumCount() + ", " + getTrackCount() + ", NOW())";
      
      public String getQueryInsertStats()
      {
    	  return this.queryInsertStats;
      }

      private String queryCountAlbums = "SELECT COUNT(*) FROM album";

      public String getQueryCountAlbums()
      {
    	  return this.queryCountAlbums;
      }
      
      private String querySelectDataNormalizedAlbumName = "SELECT dbalbumname FROM album";

      public String getQuerySelectDataNormalizedAlbumName()
      {
    	  return this.querySelectDataNormalizedAlbumName;
      }
      
      private String queryCountItems = "SELECT COUNT(*) FROM " + getItem();
      
      public String getQueryCountItems()
      {
    	  return this.queryCountItems;
      }

 
private String queryCreateCustomerRelationsDatabase = "CREATE DATABASE customer_relations /*!40100 DEFAULT CHARACTER SET utf8 */;";

public String getQueryCreateCustomerRelationsDatabase()
{
	return this.queryCreateCustomerRelationsDatabase;
}

private String queryCreateErrorsDatabase = "CREATE DATABASE errors /*!40100 DEFAULT CHARACTER SET utf8 */;";

public String getQueryCreateErrorsDatabase()
{
	return this.queryCreateErrorsDatabase;
}

private String queryCreateEventsDatabase = "CREATE DATABASE events /*!40100 DEFAULT CHARACTER SET utf8 */;";

public String getQueryCreateEventsDatabase()
{
	return this.queryCreateEventsDatabase;
}

private String queryCreateGenresDatabase = "CREATE DATABASE genres /*!40100 DEFAULT CHARACTER SET utf8 */;";

public String getQueryCreateGenresDatabase()
{
	  return this.queryCreateGenresDatabase;
}

private String queryCreateLanguagesDatabase = "CREATE DATABASE languages /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateLanguagesDatabases()
{
	return this.queryCreateLanguagesDatabase;
}
private String queryCreateMenuRoutingDatabase = "CREATE DATABASE menu_routing /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateMenuRoutingDatabase()
{
	return this.queryCreateMenuRoutingDatabase;
}
private String queryCreateMessagesDatabase = "CREATE DATABASE messages /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateMessagesDatabase()
{
	return this.queryCreateMessagesDatabase;
}
private String queryCreateSessionsDatabase = "CREATE DATABASE sessions /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateSessionsDatabase()
{
	return this.queryCreateSessionsDatabase;
}
private String queryCreateSourcesDatabase = "CREATE DATABASE sources /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateSourcesDatabase()
{
	return this.queryCreateSourcesDatabase;
}
private String queryCreateStatusesDatabase = "CREATE DATABASE statuses /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateStatusesDatabase()
{
	return this.queryCreateStatusesDatabase;
}
private String queryCreateUserMediaDatabase = "CREATE DATABASE user_media /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateUserMediaDatabase()
{
	return this.queryCreateUserMediaDatabase;
}
private String queryCreateUserProfilesDatabase = "CREATE DATABASE user_profiles /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateUserProfilesDatabase()
{
	return this.queryCreateUserProfilesDatabase;
}
private String queryCreateWarningsDatabase = "CREATE DATABASE warnings /*!40100 DEFAULT CHARACTER SET utf8 */;";
public String getQueryCreateWarningsDatabase()
{
	return this.queryCreateWarningsDatabase;
}
private String queryCreateCustomerBookingInquiresTable = "CREATE TABLE customer_booking_inquiries ("
		+ "booking_inquiry_id int(11) NOT NULL,"
		+ "first_name varchar(45) NOT NULL,"
		+ "last_name varchar(45) NOT NULL,"
		+ "primary_phone text NOT NULL,"
		+ "secondary_phone text,"
		+ "email text NOT NULL,"
		+ "preferred_contact text NOT NULL,"
		+ "service_type_id int(11) NOT NULL,"
		+ "personal_message longtext NOT NULL,"
		+ "PRIMARY KEY (booking_inquiry_id)"
		+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateCustomerBookingInquiresTable()
{
	return this.queryCreateCustomerBookingInquiresTable;
}
private String queryCreateCustomerMailingListTable = "CREATE TABLE customer_mailinglist ("
	+ "mailinglist_id varchar(45) NOT NULL,"
	+ "first_name text NOT NULL,"
	+ "last_name text NOT NULL,"
	+ "  email text NOT NULL,"
	+ "  birthday date NOT NULL,"
	+ "PRIMARY KEY (mailinglist_id)"
	+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateCustomerMailingListTable()
{
	return this.queryCreateCustomerMailingListTable;
}
private String queryCreateServiceTypesTable = "CREATE TABLE service_types ("
  + "service_type_id int(11) NOT NULL,"
  + "service_type_name text,"
  + "service_type_description text,"
  + "PRIMARY KEY (service_type_id)"
  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateServiceTypesTable()
{
	return this.queryCreateServiceTypesTable;
}
private String queryCreateSystemErrorCodesTable = "CREATE TABLE system_error_codes ("
  + "error_code_id int(11) NOT NULL,"
  + "error_code_name text,"
  + "error_code_description text,"
  + "language_code int(11) DEFAULT NULL,"
  + "  PRIMARY KEY (error_code_id)"
  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateSystemErrorCodesTable()
{
	return this.queryCreateSystemErrorCodesTable;
}
private String queryCreateUploadErrorCodesTable = "CREATE TABLE upload_error_codes ("
		+ "  error_code_id int(11) NOT NULL,"
  + "error_code_name text,"
  + "error_code_description text,"
  + "language_code int(11) DEFAULT NULL,"
  + "PRIMARY KEY (error_code_id)"
  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUploadErrorCodesTable()
{
	return this.queryCreateUploadErrorCodesTable;
}
private String queryCreateSystemEventCodesTable = "CREATE TABLE system_event_codes ("
		  + "event_code_id int(11) NOT NULL,"
		  + "event_code_name text,"
		  + "event_code_description text,"
		  + "language_code int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (event_code_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateSystemEventCodesTable()
{
	return this.queryCreateSystemEventCodesTable;
}
private String queryCreateUploadEventCodesTable = "CREATE TABLE upload_event_codes ("
		  + "event_code_id int(11) NOT NULL,"
		  + "event_code_name text,"
		  + "event_code_description text,"
		  + "language_code int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (event_code_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUploadEventCodesTable()
{
	return this.queryCreateUploadEventCodesTable;
}
private String queryCreateLanguageCodesTable = "CREATE TABLE language_codes ("
		  + "language_code int(11) NOT NULL,"
		  + "language text,"
		  + "language_description text,"
		  + "PRIMARY KEY (language_code)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateLanguageCodesTable()
{
	return this.queryCreateLanguageCodesTable;
}
private String queryCreateMenuCustomTable = "CREATE TABLE menu_custom ("
		  + "menu_name int(11) NOT NULL,"
		  + "title varchar(45) DEFAULT NULL,"
		  + "description varchar(45) DEFAULT NULL,"
		  + "menu_links_menu_link_id varchar(45) DEFAULT NULL,"
		  + "PRIMARY KEY (menu_name)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMenuCustomTable()
{
	return this.queryCreateMenuCustomTable;
}
private String queryCreateMenuLinksTable = "CREATE TABLE menu_links ("
		  + "  menu_name int(11) NOT NULL,"
		  + "menu_link_id int(11) DEFAULT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "plid int(11) DEFAULT NULL,"
		  + "link_path varchar(255) DEFAULT NULL,"
		  + "router_path varchar(255) DEFAULT NULL,"
		  + "language_code int(11) DEFAULT NULL,"
		  + "link_title text,"
		  + "options varchar(45) DEFAULT NULL,"
		  + "module varchar(45) DEFAULT NULL,"
		  + "hidden varchar(45) DEFAULT NULL,"
		  + "external varchar(45) DEFAULT NULL,"
		  + "has_children tinyint(4) DEFAULT NULL,"
		  + "expanded varchar(45) DEFAULT NULL,"
		  + "weight int(11) DEFAULT NULL,"
		  + "customized tinyint(4) DEFAULT NULL,"
		  + "p1 varchar(45) DEFAULT NULL,"
		  + "p2 varchar(45) DEFAULT NULL,"
		  + "p3 varchar(45) DEFAULT NULL,"
		  + "p4 varchar(45) DEFAULT NULL,"
		  + "p5 varchar(45) DEFAULT NULL,"
		  + "p6 varchar(45) DEFAULT NULL,"
		  + "p7 varchar(45) DEFAULT NULL,"
		  + "p8 varchar(45) DEFAULT NULL,"
		  + "p9 varchar(45) DEFAULT NULL,"
		  + "last_update_timestamp datetime DEFAULT NULL,"
		  + "route_name text,"
		  + "route_paramaters text,"
		  + "PRIMARY KEY (menu_name)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMenuLinksTable()
{
	return this.queryCreateMenuLinksTable;
}
private String queryCreateMenuRouterTable = "CREATE TABLE menu_router ("
		  + "path int(11) NOT NULL,"
		  + "load_functions varchar(45) DEFAULT NULL,"
		  + "to_arg_functions varchar(45) DEFAULT NULL,"
		  + "access_callback varchar(45) DEFAULT NULL,"
		  + "access_arguments varchar(45) DEFAULT NULL,"
		  + "page_callback varchar(45) DEFAULT NULL,"
		  + "page_arguments varchar(45) DEFAULT NULL,"
		  + "fit varchar(45) DEFAULT NULL,"
		  + "number_parts varchar(45) DEFAULT NULL,"
		  + "context varchar(45) DEFAULT NULL,"
		  + "tab_parent varchar(45) DEFAULT NULL,"
		  + "tab_root varchar(45) DEFAULT NULL,"
		  + "title varchar(45) DEFAULT NULL,"
		  + "title_call_back varchar(45) DEFAULT NULL,"
		  + "title_arguments varchar(45) DEFAULT NULL,"
		  + "theme_arguements varchar(45) DEFAULT NULL,"
		  + "type varchar(45) DEFAULT NULL,"
		  + "description varchar(45) DEFAULT NULL,"
		  + "description_callback varchar(45) DEFAULT NULL,"
		  + "description_arugements varchar(45) DEFAULT NULL,"
		  + "position varchar(45) DEFAULT NULL,"
		  + "weight varchar(45) DEFAULT NULL,"
		  + "include_file varchar(45) DEFAULT NULL,"
		  + "route_name varchar(45) DEFAULT NULL,"
		  + "PRIMARY KEY (path)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMenuRouterTable()
{
	return this.queryCreateMenuRouterTable;
}
private String queryCreateUserSessionsTable = "CREATE TABLE user_sessions ("
		  + "user_id int(11) NOT NULL,"
		  + "session_id int(11) NOT NULL,"
		  + "session_session_id int(11) DEFAULT NULL,"
		  + "hostname text,"
		  + "session_timestamp datetime DEFAULT NULL,"
		  + "session_session int(11) DEFAULT NULL,"
		  + "last_activity_date datetime DEFAULT NULL,"
		  + "last_login_date datetime DEFAULT NULL"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserSessionsTable()
{
	return this.queryCreateUserSessionsTable;
}
private String queryCreateUploadStashSourcesTable = "CREATE TABLE upload_stash_sources ("
		  + "upload_stash_source_id int(11) NOT NULL,"
		  + "upload_stash_source_name text,"
		  + "upload_stash_source_description text,"
		  + "PRIMARY KEY (upload_stash_source_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUploadStashSourcesTable()
{
	return this.queryCreateUploadStashSourcesTable;
}
private String queryCreateMediaStatusTable = "CREATE TABLE media_status ("
		  + "media_status_id int(11) NOT NULL,"
		  + "media_status_name text,"
		  + "media_status_description text,"
		  + "PRIMARY KEY (media_status_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMediaStatusTable()
{
	return this.queryCreateMediaStatusTable;
}
private String queryCreateUploadStatusTable = "CREATE TABLE upload_status ("
		  + "upload_status_id int(11) NOT NULL,"
		  + "upload_status_name text,"
		  + "upload_status_description text,"
		  + "PRIMARY KEY (upload_status_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUploadStatusTable()
{
	return this.queryCreateUploadStatusTable;
}
private String queryCreateUserStatusTable = "CREATE TABLE user_status ("
		  + "status_id int(11) NOT NULL,"
		  + "status_name text,"
		  + "status_description text,"
		  + "PRIMARY KEY (status_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserStatusTable()
{
	return this.queryCreateUserStatusTable;
}
private String queryCreateCaptionTypesTable = "CREATE TABLE caption_types ("
		  + "caption_type_id int(11) NOT NULL,"
		  + "caption_type_name text,"
		  + "caption_type_description varchar(45) DEFAULT NULL,"
		  + "PRIMARY KEY (caption_type_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateCaptionTypesTable()
{
	return this.queryCreateCaptionTypesTable;
}
private String queryCreateCaptionsTable = "CREATE TABLE captions ("
		  + "caption_id int(11) NOT NULL,"
		  + "language_code int(11) DEFAULT NULL,"
		  + "caption_type_id int(11) DEFAULT NULL,"
		  + "caption_sub_type_id int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (caption_id),"
		  + "KEY captions_language_code_idx (language_code),"
		  + "CONSTRAINT captions_caption_type_id FOREIGN KEY (caption_id) REFERENCES caption_types (caption_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT captions_language_code FOREIGN KEY (language_code) REFERENCES languages.language_codes (language_code) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT captions_sub_type_id FOREIGN KEY (caption_id) REFERENCES caption_types (caption_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateCaptionsTable()
{
	return this.queryCreateCaptionsTable;
}
private String queryCreateContainerTypesTable = "CREATE TABLE container_types ("
		  + "container_type_id int(11) NOT NULL,"
		  + "container_type_name text,"
		  + "container_type_description text,"
		  + "PRIMARY KEY (container_type_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateContainerTypesTable()
{
	return this.queryCreateContainerTypesTable;
}
private String quearyCreateDisplayObjectTable = "CREATE TABLE display_object ("
		  + "object_id int(11) NOT NULL,"
		  + "url varchar(255) DEFAULT NULL,"
		  + "view_size int(11) DEFAULT NULL,"
		  + "display_object_type_id int(11) NOT NULL,"
		  + "height int(11) DEFAULT NULL,"
		  + "width int(11) DEFAULT NULL,"
		  + "html_output longtext,"
		  + "script_output varchar(255) DEFAULT NULL,"
		  + "PRIMARY KEY (object_id),"
		  + "KEY display_object_type_id_idx (display_object_type_id),"
		  + "CONSTRAINT display_object_type_id FOREIGN KEY (display_object_type_id) REFERENCES display_object_types (display_object_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQuearyCreateDisplayObjectTable()
{
	return this.quearyCreateDisplayObjectTable;
}
private String queryCreateDisplayObjectTypesTable = "CREATE TABLE display_object_types ("
		  + "display_object_type_id int(11) NOT NULL,"
		  + "display_object_type text NOT NULL,"
		  + "PRIMARY KEY (display_object_type_id),"
		  + "UNIQUE KEY display_object_id_UNIQUE (display_object_type_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateDisplayObjectTypesTable()
{
	return this.queryCreateDisplayObjectTypesTable;
}
private String queryCreateFileArchiveTable = "CREATE TABLE file_archive ("
		  + "file_archive_id int(11) NOT NULL,"
		  + "file_archive_name text,"
		  + "file_archive_storage_group varchar(45) DEFAULT NULL,"
		  + "file_archive_storage_key varchar(45) DEFAULT NULL,"
		  + "file_archive_deleted_user int(11) DEFAULT NULL,"
		  + "file_archive_deleted_timestamp datetime DEFAULT NULL,"
		  + "file_archive_deleted_reason text,"
		  + "file_archive_size int(11) DEFAULT NULL,"
		  + "file_archive_width int(11) DEFAULT NULL,"
		  + "file_archive_height int(11) DEFAULT NULL,"
		  + "file_archive_metadata varchar(45) DEFAULT NULL,"
		  + "file_archive_bits varchar(45) DEFAULT NULL,"
		  + "file_archive_media_type int(11) DEFAULT NULL,"
		  + "file_archive_major_mime int(11) DEFAULT NULL,"
		  + "file_archive_minor_mime int(11) DEFAULT NULL,"
		  + "file_archive_user int(11) DEFAULT NULL COMMENT 'uuid',"
		  + "file_archive_timestamp datetime DEFAULT NULL,"
		  + "file_archive_deleted tinyint(4) DEFAULT NULL,"
		  + "PRIMARY KEY (file_archive_id),"
		  + "KEY file_archive_deleted_uuid_idx (file_archive_deleted_user),"
		  + "KEY file_archive_minor_mime_type_idx (file_archive_minor_mime),"
		  + "KEY file_archive_major_mime_type_idx (file_archive_major_mime),"
		  + "KEY file_archive_uuid_idx (file_archive_user),"
		  + "KEY file_archive_media_type_idx (file_archive_media_type),"
		  + "CONSTRAINT file_archive_deleted_uuid FOREIGN KEY (file_archive_deleted_user) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT file_archive_major_mime_type FOREIGN KEY (file_archive_major_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT file_archive_media_type FOREIGN KEY (file_archive_media_type) REFERENCES media_types (media_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT file_archive_minor_mime_type FOREIGN KEY (file_archive_minor_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT file_archive_uuid FOREIGN KEY (file_archive_user) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateFileArchiveTable()
{
	return this.queryCreateFileArchiveTable;
}
private String queryCreateFilesTable = "CREATE TABLE files ("
		  + "file_id int(11) NOT NULL,"
		  + "file_path varchar(255) DEFAULT NULL,"
		  + "is_url tinyint(1) DEFAULT NULL,"
		  + "file_type int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (file_id),"
		  + "KEY file_type_idx (file_type),"
		  + "CONSTRAINT file_type FOREIGN KEY (file_type) REFERENCES media_types (media_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateFilesTable()
{
	return this.queryCreateFilesTable;
}
private String queryCreateGalleriesTable = "CREATE TABLE galleries ("
		  + "gallery_id int(11) NOT NULL,"
		  + "gallery_name text,"
		  + "gallery_summary text,"
		  + "comments_id int(11) DEFAULT NULL,"
		  + "creation_timestamp datetime DEFAULT NULL,"
		  + "last_modified_timestamp datetime DEFAULT NULL,"
		  + "serial_number int(11) DEFAULT NULL,"
		  + "gallery_group_type_id int(11) DEFAULT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "date_start datetime DEFAULT NULL,"
		  + "date_end datetime DEFAULT NULL,"
		  + "is_private tinyint(4) DEFAULT NULL,"
		  + "theme_id int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (gallery_id),"
		  + "KEY gallery_caption_id_idx (comments_id),"
		  + "KEY gallery_uuid_idx (uuid),"
		  + "KEY gallery_groupe_type_idx (gallery_group_type_id),"
		  + "CONSTRAINT gallery_caption_id FOREIGN KEY (comments_id) REFERENCES captions (caption_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT gallery_groupe_type FOREIGN KEY (gallery_group_type_id) REFERENCES gallery_group_types (group_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT gallery_uuid FOREIGN KEY (uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleriesTable()
{
	return this.queryCreateGalleriesTable;
}
private String queryCreateGalleryChildrenTable = "CREATE TABLE gallery_children ("
		  + "child_id int(11) NOT NULL,"
		  + "parent_id int(11) DEFAULT NULL,"
		  + "KEY gallery_parent_id_idx (parent_id),"
		  + "KEY gallery_child_id_idx (child_id),"
		  + "CONSTRAINT gallery_child_id FOREIGN KEY (child_id) REFERENCES gallery_containers (gallery_container_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT gallery_parent_id FOREIGN KEY (parent_id) REFERENCES gallery_containers (gallery_container_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleryChildrenTable()
{
	return this.queryCreateGalleryChildrenTable;
}
private String queryCreateGalleryContainerImageTable = "CREATE TABLE gallery_container_image ("
		  + "gallery_container_image_id int(11) NOT NULL,"
		  + "gallery_image_id int(11) DEFAULT NULL,"
		  + "width int(11) DEFAULT NULL,"
		  + "height int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (gallery_container_image_id),"
		  + "KEY gallery_container_image_idx (gallery_image_id),"
		  + "CONSTRAINT gallery_container_image FOREIGN KEY (gallery_image_id) REFERENCES images (image_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleryContainerImageTable()
{
	return this.queryCreateGalleryContainerImageTable;
}
private String queryCreateGalleryContainersTable = "CREATE TABLE gallery_containers ("
		  + "gallery_container_id int(11) NOT NULL,"
		  + "container_permissions int(11) DEFAULT NULL,"
		  + "container_order int(11) DEFAULT NULL,"
		  + "container_width int(11) DEFAULT NULL,"
		  + "container_height int(11) DEFAULT NULL,"
		  + "container_type int(11) DEFAULT NULL,"
		  + "container_mime_type int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (gallery_container_id),"
		  + "KEY gallery_container_permissions_idx (container_permissions),"
		  + "KEY gallery_container_mime_idx (container_mime_type),"
		  + "KEY gallery_container_type_idx (container_type),"
		  + "CONSTRAINT gallery_container_mime FOREIGN KEY (container_mime_type) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT gallery_container_permissions FOREIGN KEY (container_permissions) REFERENCES gallery_permissions (gallery_permissions_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT gallery_container_type FOREIGN KEY (container_type) REFERENCES container_types (container_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleryContainersTable()
{
	return this.queryCreateGalleryContainersTable;
}
private String queryCreateGalleryGroupTypes = "CREATE TABLE gallery_group_types ("
		  + "group_type_id int(11) NOT NULL,"
		  + "group_name text,"
		  + "PRIMARY KEY (group_type_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleryGroupTypes()
{
	return this.queryCreateGalleryGroupTypes;
}
private String queryCreateGalleryItems = "CREATE TABLE gallery_items ("
		  + "gallery_item_id int(11) NOT NULL,"
		  + "title text,"
		  + "caption_id int(11) DEFAULT NULL,"
		  + "description text,"
		  + "keywords text,"
		  + "layout_order_number int(11) DEFAULT NULL,"
		  + "order_by varchar(45) DEFAULT NULL,"
		  + "order_direction varchar(45) DEFAULT NULL,"
		  + "order_weight varchar(45) DEFAULT NULL,"
		  + "item_id int(11) DEFAULT NULL,"
		  + "item_type_id int(11) DEFAULT NULL,"
		  + "mime_type_id int(11) DEFAULT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (gallery_item_id),"
		  + "KEY gallery_item_caption_idx (caption_id),"
		  + "KEY gallery_item_user_id_idx (uuid),"
		  + "CONSTRAINT gallery_item_caption FOREIGN KEY (caption_id) REFERENCES captions (caption_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT gallery_item_user_id FOREIGN KEY (uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
		  + "CREATE TABLE gallery_object_types ("
		  + "gallery_object_type_id int(11) NOT NULL AUTO_INCREMENT,"
		  + "gallery_object_type text NOT NULL,"
		  + "PRIMARY KEY (gallery_object_type_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleryItems()
{
	return this.queryCreateGalleryItems;
}
private String queryCreateGalleryPermissionsTable = "CREATE TABLE gallery_permissions ("
		  + "gallery_permissions_id int(11) NOT NULL,"
		  + "administer_gallery tinyint(4) DEFAULT NULL,"
		  + "add_child_album tinyint(4) DEFAULT NULL,"
		  + "add_media_object tinyint(4) DEFAULT NULL,"
		  + "administer_site tinyint(4) DEFAULT NULL,"
		  + "delete_album tinyint(4) DEFAULT NULL,"
		  + "delete_child_album tinyint(4) DEFAULT NULL,"
		  + "deleted_media_object tinyint(4) DEFAULT NULL,"
		  + "edit_album tinyint(4) DEFAULT NULL,"
		  + "edit_media_object tinyint(4) DEFAULT NULL,"
		  + "hide_watermark tinyint(4) DEFAULT NULL,"
		  + "synchronize tinyint(4) DEFAULT NULL,"
		  + "view_album_or_media_object tinyint(4) DEFAULT NULL,"
		  + "view_original_media_object tinyint(4) DEFAULT NULL,"
		  + "can_contain_children tinyint(4) DEFAULT NULL,"
		  + "is_a_parent tinyint(4) DEFAULT NULL,"
		  + "PRIMARY KEY (gallery_permissions_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleryPermissionsTable()
{
	return this.queryCreateGalleryPermissionsTable;
}
private String queryCreateGalleryStatsTable = "CREATE TABLE gallery_stats ("
		  + "gallery_id int(11) NOT NULL,"
		  + "viewed_since_timestamp datetime DEFAULT NULL,"
		  + "number_of_albums int(11) DEFAULT NULL,"
		  + "number_of_media_items int(11) DEFAULT NULL,"
		  + "number_of_views int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (gallery_id),"
		  + "CONSTRAINT gallery_stats_gallery_id FOREIGN KEY (gallery_id) REFERENCES galleries (gallery_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateGalleryStatsTable()
{
	return this.queryCreateGalleryStatsTable;
}
private String queryCreateImageEffects = "CREATE TABLE image_effects ("
		  + "image_effects_id int(11) NOT NULL,"
		  + "image_style_id int(11) DEFAULT NULL,"
		  + "weight int(11) DEFAULT NULL,"
		  + "name text,"
		  + "data longblob,"
		  + "PRIMARY KEY (image_effects_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

public String getQueryCreateImageEffects()
{
	return this.queryCreateImageEffects;
}

private String queryCreateImageLinksTable = "CREATE TABLE image_links ("
		  + "image_links_from varchar(45) DEFAULT NULL,"
		  + "image_links_to varchar(45) DEFAULT NULL"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateImageLinksTable()
{
	return this.queryCreateImageLinksTable;
}

private String queryCreateImageStylesTable = "CREATE TABLE image_styles ("
		  + "image_style_id int(11) NOT NULL,"
		  + "image_style_name text,"
		  + "PRIMARY KEY (image_style_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateImageStylesTable()
{
	return this.queryCreateImageStylesTable;
}
private String queryCreateImagesTable = "CREATE TABLE images ("
		  + "image_id int(11) NOT NULL,"
		  + "image_name text,"
		  + "image_size int(11) DEFAULT NULL,"
		  + "image_width int(11) DEFAULT NULL,"
		  + "image_height int(11) DEFAULT NULL,"
		  + "image_metadata_id int(11) DEFAULT NULL,"
		  + "image_bits int(11) DEFAULT NULL,"
		  + "image_media_type int(11) DEFAULT NULL,"
		  + "image_major_mime int(11) DEFAULT NULL,"
		  + "image_minor_mime int(11) DEFAULT NULL,"
		  + "image_description text,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "image_user_text text,"
		  + "image_timestamp datetime DEFAULT NULL,"
		  + "image_sha1 varbinary(32) DEFAULT NULL,"
		  + "PRIMARY KEY (image_id),"
		  + "KEY image_media_type_idx (image_media_type),"
		  + "KEY image_major_mime_idx (image_major_mime),"
		  + "KEY image_minor_mime_idx (image_minor_mime),"
		  + "KEY image_owner_uuid_idx (uuid),"
		  + "CONSTRAINT image_major_mime FOREIGN KEY (image_major_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT image_minor_mime FOREIGN KEY (image_minor_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateImagesTable()
{
	return this.queryCreateImagesTable;
}
private String queryCreateMediaTypesTable = "CREATE TABLE media_types ("
		  + "media_type_id int(11) NOT NULL,"
		  + "media_type_name text,"
		  + "media_type_description text,"
		  + "media_type_file_extension text,"
		  + "PRIMARY KEY (media_type_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMediaTypesTable()
{
	return this.queryCreateMediaTypesTable;
}
private String queryCreateMediaUsersTable = "CREATE TABLE media_users ("
		  + "uiid int(11) NOT NULL,"
		  + "gallery_id int(11) NOT NULL,"
		  + "comment text,"
		  + "is_approved tinyint(4) NOT NULL,"
		  + "enable_user_album tinyint(4) NOT NULL,"
		  + "can_add_album_to_at_least_one_album tinyint(4) NOT NULL,"
		  + "can_add_media_to_at_least_one_album tinyint(4) NOT NULL,"
		  + "creation_date datetime NOT NULL,"
		  + "PRIMARY KEY (uiid),"
		  + "UNIQUE KEY uiid_UNIQUE (uiid),"
		  + "UNIQUE KEY gallery_id_UNIQUE (gallery_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMediaUsersTable()
{
	return this.queryCreateMediaUsersTable;
}
private String queryCreateMetaItemTable = "CREATE TABLE meta_item ("
		  + "meta_item_id int(11) NOT NULL,"
		  + "title text,"
		  + "value text,"
		  + "keywords longtext,"
		  + "desc text,"
		  + "topic text,"
		  + "gallery_type_id int(11) DEFAULT NULL,"
		  + "media_type_id int(11) DEFAULT NULL,"
		  + "media_id int(11) DEFAULT NULL,"
		  + "is_editable tinyint(4) DEFAULT NULL,"
		  + "date datetime DEFAULT NULL,"
		  + "PRIMARY KEY (meta_item_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMetaItemTable()
{
	return this.queryCreateMetaItemTable;
}
private String queryCreateMimeTypeCategoryTable = "CREATE TABLE mime_type_category ("
		  + "mime_type_id int(11) NOT NULL AUTO_INCREMENT,"
		  + "mime_type text NOT NULL,"
		  + "PRIMARY KEY (mime_type_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateMimeTypeCategoryTable()
{
	return this.queryCreateMimeTypeCategoryTable;
}
private String queryCreateOldImageTable = "CREATE TABLE old_image ("
		  + "  old_image_id int(11) NOT NULL,"
		  + "old_image_name int(11) DEFAULT NULL,"
		  + "old_image_archive_name text,"
		  + "old_image_size int(11) DEFAULT NULL,"
		  + "old_image_width int(11) DEFAULT NULL,"
		  + "old_image_height int(11) DEFAULT NULL,"
		  + "old_image_bits int(11) DEFAULT NULL,"
		  + "old_image_description text,"
		  + "old_image_uuid int(11) DEFAULT NULL,"
		  + "old_image_user_text text,"
		  + "old_image_timestamp datetime DEFAULT NULL,"
		  + "old_image_meta_data int(11) DEFAULT NULL,"
		  + "old_image_media_type int(11) DEFAULT NULL,"
		  + "old_image_major_mime int(11) DEFAULT NULL,"
		  + "old_image_minor_mime int(11) DEFAULT NULL,"
		  + "old_image_deleted tinyint(4) DEFAULT NULL,"
		  + "PRIMARY KEY (old_image_id),"
		  + "KEY old_image_mime_type_idx (old_image_major_mime),"
		  + "KEY old_image_minor_mime_idx (old_image_minor_mime),"
		  + "KEY old_image_uuid_idx (old_image_uuid),"
		  + "KEY old_image_media_type_idx (old_image_media_type),"
		  + "KEY old_image_meta_data_idx (old_image_meta_data),"
		  + "CONSTRAINT old_image_major_mime FOREIGN KEY (old_image_major_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT old_image_media_type FOREIGN KEY (old_image_media_type) REFERENCES media_types (media_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT old_image_meta_data FOREIGN KEY (old_image_meta_data) REFERENCES old_meta_items (old_meta_item_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT old_image_minor_mime FOREIGN KEY (old_image_minor_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT old_image_uuid FOREIGN KEY (old_image_uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateOldImageTable()
{
	return this.queryCreateOldImageTable;
}
private String queryCreateOldMetaItemsTable = "CREATE TABLE old_meta_items ("
		  + "old_meta_item_id int(11) NOT NULL,"
		  + "old_meta_item_title text,"
		  + "old_meta_item_value text,"
		  + "old_meta_item_keywords longtext,"
		  + "old_meta_item_desc text,"
		  + "old_meta_item_topic text,"
		  + "old_meta_item_gallery_type_id int(11) DEFAULT NULL,"
		  + "old_meta_item_media_type_id int(11) DEFAULT NULL,"
		  + "old_meta_item_media_id int(11) DEFAULT NULL,"
		  + "old_meta_item_is_editable tinyint(4) DEFAULT NULL,"
		  + "old_meta_item_date datetime DEFAULT NULL,"
		  + "PRIMARY KEY (old_meta_item_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateOldMetaItemsTable()
{
	return this.queryCreateOldMetaItemsTable;
}
private String queryCreateSpeakersTable = "CREATE TABLE speakers ("
		  + "speaker_id int(11) NOT NULL,"
		  + "is_registered_user tinyint(4) NOT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "video_id int(11) NOT NULL,"
		  + "first_name text NOT NULL,"
		  + "last_name text NOT NULL,"
		  + "gender tinytext NOT NULL,"
		  + "age int(11) NOT NULL,"
		  + "email text NOT NULL,"
		  + "phone text NOT NULL,"
		  + "send_notifications tinyint(4) NOT NULL,"
		  + "PRIMARY KEY (speaker_id,video_id),"
		  + "KEY speaker_user_id_idx (uuid),"
		  + "CONSTRAINT speaker_user_id FOREIGN KEY (uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateSpeakersTable()
{
	return this.queryCreateSpeakersTable;
}
private String queryCreateSpeakersVideosTable = "CREATE TABLE speakers_videos ("
		  + "video_id int(11) NOT NULL,"
		  + "speaker_id int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (video_id),"
		  + "KEY speaker_id_idx (speaker_id),"
		  + "CONSTRAINT speaker_id FOREIGN KEY (speaker_id) REFERENCES speakers (speaker_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT video_id FOREIGN KEY (video_id) REFERENCES videos (video_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateSpeakersVideosTable()
{
	return this.queryCreateSpeakersVideosTable;
}
private String queryCreateUploadStashTable = "CREATE TABLE upload_stash ("
		  + "upload_stash_id int(11) NOT NULL,"
		  + "upload_stash_user int(11) DEFAULT NULL,"
		  + "upload_stash_original_path text,"
		  + "upload_stash_path text,"
		  + "upload_stash_source_type int(11) DEFAULT NULL,"
		  + "upload_stash_timestamp datetime DEFAULT NULL,"
		  + "upload_stash_status int(11) DEFAULT NULL,"
		  + "upload_stash_chunk int(11) DEFAULT NULL,"
		  + "upload_stash_size int(11) DEFAULT NULL,"
		  + "upload_stash_sha1 int(11) DEFAULT NULL,"
		  + "upload_stash_major_mime int(11) DEFAULT NULL,"
		  + "upload_stash_minor_mime int(11) DEFAULT NULL,"
		  + "upload_stash_media_type int(11) DEFAULT NULL,"
		  + "upload_stash_image_width int(11) DEFAULT NULL,"
		  + "upload_stash_image_height int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (upload_stash_id),"
		  + "KEY upload_stash_uuid_idx (upload_stash_user),"
		  + "KEY upload_stash_source_id_idx (upload_stash_source_type),"
		  + "KEY upload_stash_status_id_idx (upload_stash_status),"
		  + "KEY upload_stash_media_type_idx (upload_stash_media_type),"
		  + "KEY upload_stash_major_mime_idx (upload_stash_major_mime),"
		  + "KEY upload_stash_minor_mime_idx (upload_stash_minor_mime),"
		  + "CONSTRAINT upload_stash_major_mime FOREIGN KEY (upload_stash_major_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT upload_stash_media_type FOREIGN KEY (upload_stash_media_type) REFERENCES media_types (media_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT upload_stash_minor_mime FOREIGN KEY (upload_stash_minor_mime) REFERENCES mime_type_category (mime_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT upload_stash_source_id FOREIGN KEY (upload_stash_source_type) REFERENCES sources.upload_stash_sources (upload_stash_source_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT upload_stash_uuid FOREIGN KEY (upload_stash_user) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT upload_status_id FOREIGN KEY (upload_stash_status) REFERENCES statuses.upload_status (upload_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUploadStashTable()
{
	return this.queryCreateUploadStashTable;
}

private String queryCreateUploadsTable = "CREATE TABLE uploads ("
		  + "upload_id int(11) NOT NULL,"
		  + "external_id int(11) DEFAULT NULL,"
		  + "language_code int(11) DEFAULT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "consumed_quota int(11) DEFAULT NULL,"
		  + "timestamp datetime DEFAULT NULL,"
		  + "last_status_update varchar(45) DEFAULT NULL,"
		  + "status_id int(11) DEFAULT NULL,"
		  + "info varchar(45) DEFAULT NULL,"
		  + "error_code int(11) DEFAULT NULL,"
		  + "event_code int(11) DEFAULT NULL,"
		  + "cluster_job_ids int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (upload_id),"
		  + "KEY uploads_language_code_idx (language_code),"
		  + "KEY uploads_uuid_idx (uuid),"
		  + "KEY uploads_status_id_idx (status_id),"
		  + "KEY uploads_error_code_idx (error_code),"
		  + "KEY uploads_event_code_idx (event_code),"
		  + "CONSTRAINT uploads_error_code FOREIGN KEY (error_code) REFERENCES errors.upload_error_codes (error_code_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT uploads_event_code FOREIGN KEY (event_code) REFERENCES events.upload_event_codes (event_code_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT uploads_language_code FOREIGN KEY (language_code) REFERENCES languages.language_codes (language_code) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT uploads_status_id FOREIGN KEY (status_id) REFERENCES statuses.upload_status (upload_status_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT uploads_uuid FOREIGN KEY (uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUploadsTable()
{
	return this.queryCreateUploadsTable;
}
private String queryCreateUserImagesTable = "CREATE TABLE user_images ("
		  + "user_image_id int(11) NOT NULL,"
		  + "image_id int(11) DEFAULT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (user_image_id),"
		  + "KEY user_images_uuid_idx (uuid),"
		  + "KEY user_images_image_id_idx (image_id),"
		  + "CONSTRAINT user_images_image_id FOREIGN KEY (image_id) REFERENCES images (image_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT user_images_uuid FOREIGN KEY (uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserImagesTable()
{
	return this.queryCreateUserImagesTable;
}
private String queryCreateUserQuotaTable = "CREATE TABLE user_quota ("
		  + "uuid int(11) NOT NULL,"
		  + "quota_videos int(11) DEFAULT NULL,"
		  + "accu_time int(11) DEFAULT NULL,"
		  + "accum_videos int(11) DEFAULT NULL,"
		  + "accum_photos int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (uuid),"
		  + "CONSTRAINT user_quota_uuid FOREIGN KEY (uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserQuotaTable()
{
	return this.queryCreateUserQuotaTable;
}
private String queryCreateUserVideosTable = "CREATE TABLE user_videos ("
		  + "  video_id int(11) NOT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (video_id),"
		  + "KEY user_videos_uuid_idx (uuid),"
		  + "CONSTRAINT user_videos_id FOREIGN KEY (video_id) REFERENCES videos (video_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT user_videos_uuid FOREIGN KEY (uuid) REFERENCES user_profiles.users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserVideosTable()
{
	return this.queryCreateUserVideosTable;
}
private String queryCreateVideosTable = "CREATE TABLE videos ("
		  + "video_id int(11) NOT NULL,"
		  + "external_id text,"
		  + "duration int(11) DEFAULT NULL,"
		  + "language_code int(11) DEFAULT NULL,"
		  + "video_name text,"
		  + "video_size int(11) DEFAULT NULL,"
		  + "video_width int(11) DEFAULT NULL,"
		  + "video_height int(11) DEFAULT NULL,"
		  + "video_metadata_id int(11) DEFAULT NULL,"
		  + "video_bits int(11) DEFAULT NULL,"
		  + "video_media_type int(11) DEFAULT NULL,"
		  + "video_major_mime int(11) DEFAULT NULL,"
		  + "video_minor_mime int(11) DEFAULT NULL,"
		  + "video_description text,"
		  + "video_user_text text,"
		  + "video_timestamp datetime DEFAULT NULL,"
		  + "video_sha1 varbinary(32) DEFAULT NULL,"
		  + "video_image_id int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (video_id),"
		  + "KEY video_language_code_idx (language_code),"
		  + "CONSTRAINT video_language_code FOREIGN KEY (language_code) REFERENCES languages.language_codes (language_code) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateVideosTable()
{
	return this.queryCreateVideosTable;
}
private String queryCreatePrivilegeTable = "CREATE TABLE privilege ("
		  + "privilege_id int(11) NOT NULL,"
		  + "privilege text,"
		  + "privilege_description text,"
		  + "PRIMARY KEY (privilege_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreatePrivilegeTable()
{
	return this.queryCreatePrivilegeTable;
}

private String queryCreateProfileAttributeTypeTable = "CREATE TABLE profile_attribute_type ("
		  + "profile_attritbute_type_id int(11) NOT NULL,"
		  + "name text,"
		  + "description text,"
		  + "format varchar(45) DEFAULT NULL,"
		  + "foreign_key varchar(45) DEFAULT NULL,"
		  + "searchable tinyint(4) DEFAULT NULL,"
		  + "creator int(11) DEFAULT NULL,"
		  + "date_created datetime DEFAULT NULL,"
		  + "changed_by int(11) DEFAULT NULL,"
		  + "date_changed datetime DEFAULT NULL,"
		  + "edit_privilege varchar(45) DEFAULT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "sort_weight int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (profile_attritbute_type_id),"
		  + "KEY profile_att_type_uuid_idx (uuid),"
		  + "KEY profile_att_type_creator_uuid_idx (creator),"
		  + "CONSTRAINT profile_att_type_creator_uuid FOREIGN KEY (creator) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT profile_att_type_uuid FOREIGN KEY (uuid) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

public String getQueryCreateProfileAttributeTypeTable()
{
	return this.queryCreateProfileAttributeTypeTable;
}
private String queryCreateProfileAttributesTable = "CREATE TABLE profile_attributes ("
		  + "profile_attribute_id int(11) NOT NULL,"
		  + "profile_attribute_type_id int(11) NOT NULL,"
		  + "value varchar(45) NOT NULL,"
		  + "profile_id int(11) DEFAULT NULL,"
		  + "creator int(11) DEFAULT NULL,"
		  + "date_created datetime DEFAULT NULL,"
		  + "changed_by int(11) DEFAULT NULL,"
		  + "date_changed datetime DEFAULT NULL,"
		  + "voided tinyint(4) DEFAULT NULL,"
		  + "voided_by int(11) DEFAULT NULL,"
		  + "date_voided datetime DEFAULT NULL,"
		  + "void_reason text,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (profile_attribute_id),"
		  + "KEY profile_att_creator_idx (creator),"
		  + "KEY profile_att_voided_by_id_idx (voided_by),"
		  + "KEY profile_att_profile_id_idx (profile_id),"
		  + "KEY profile_att_uuid_idx (uuid),"
		  + "CONSTRAINT profile_att_creator FOREIGN KEY (creator) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT profile_att_profile_id FOREIGN KEY (profile_id) REFERENCES profiles (profile_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT profile_att_uuid FOREIGN KEY (uuid) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT profile_att_voided_by_id FOREIGN KEY (voided_by) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateProfileAttributesTable()
{
	return this.queryCreateProfileAttributesTable;
}
private String queryCreateProfilesTable = "CREATE TABLE profiles ("
		  + "profile_id int(11) NOT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "gender tinytext,"
		  + "birthdate date DEFAULT NULL,"
		  + "creator int(11) DEFAULT NULL,"
		  + "date_created datetime DEFAULT NULL,"
		  + "changed_by int(11) DEFAULT NULL,"
		  + "date_changed datetime DEFAULT NULL,"
		  + "voided tinyint(4) DEFAULT NULL,"
		  + "voided_by int(11) DEFAULT NULL,"
		  + "date_voided datetime DEFAULT NULL,"
		  + "void_reason text,"
		  + "notify_user_on_password_change tinyint(4) DEFAULT NULL,"
		  + "PRIMARY KEY (profile_id),"
		  + "KEY profiles_uuid_idx (uuid),"
		  + "KEY profiles_creator_uuid_idx (creator),"
		  + "KEY profiles_voided_uuid_idx (voided_by),"
		  + "CONSTRAINT profiles_creator_uuid FOREIGN KEY (creator) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT profiles_uuid FOREIGN KEY (uuid) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT profiles_voided_uuid FOREIGN KEY (voided_by) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateProfilesTable()
{
	return this.queryCreateProfilesTable;
}
private String qeueryCreateRelationshipTypes = "CREATE TABLE relationship_type ("
		  + "relation_ship_type_id int(11) NOT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "a_is_to_b text,"
		  + "b_is_to_a text,"
		  + "preferred tinyint(4) DEFAULT NULL,"
		  + "weight int(11) DEFAULT NULL,"
		  + "description text,"
		  + "creator int(11) DEFAULT NULL,"
		  + "date_created datetime DEFAULT NULL,"
		  + "voided tinyint(4) DEFAULT NULL,"
		  + "voided_by int(11) DEFAULT NULL,"
		  + "date_voided datetime DEFAULT NULL,"
		  + "void_reason text,"
		  + "PRIMARY KEY (relation_ship_type_id),"
		  + "KEY rel_type_uuid_idx (uuid),"
		  + "KEY rel_type_creator_uuid_idx (creator),"
		  + "CONSTRAINT rel_type_creator_uuid FOREIGN KEY (creator) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT rel_type_uuid FOREIGN KEY (uuid) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQeueryCreateRelationshipTypes()
{
	return this.qeueryCreateRelationshipTypes;
}
private String queryCreateRelationshipsTable = "CREATE TABLE relationships ("
		  + "relationship_id int(11) NOT NULL,"
		  + "person_a int(11) NOT NULL,"
		  + "relationship_type_id int(11) NOT NULL,"
		  + "person_b int(11) NOT NULL,"
		  + "creator int(11) DEFAULT NULL,"
		  + "date_created datetime DEFAULT NULL,"
		  + "voided tinyint(4) DEFAULT NULL,"
		  + "voided_by int(11) DEFAULT NULL,"
		  + "date_voided datetime DEFAULT NULL,"
		  + "void_reason text,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (relationship_id),"
		  + "KEY reletionships_uuid_fk_idx (uuid),"
		  + "KEY relationships_creator_fk_idx (creator),"
		  + "KEY relationships_voided_uuid_fk_idx (voided_by),"
		  + "CONSTRAINT relationships_creator_fk FOREIGN KEY (creator) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT relationships_relation_type_fk FOREIGN KEY (relationship_id) REFERENCES relationship_type (relation_ship_type_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT relationships_voided_uuid_fk FOREIGN KEY (voided_by) REFERENCES users (uuid),"
		  + "CONSTRAINT reletionships_uuid_fk FOREIGN KEY (uuid) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateRelationshipsTable()
{
	return this.queryCreateRelationshipsTable;
}
private String queryCreateRolePrivilegesTable = "CREATE TABLE role_privileges ("
		  + "role_id int(11) NOT NULL,"
		  + "privilege_id int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (role_id),"
		  + "CONSTRAINT role_privilege_role_fk FOREIGN KEY (role_id) REFERENCES roles (role_id) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateRolePrivilegesTable()
{
	return this.queryCreateRolePrivilegesTable;
}
private String queryCreateRolesTable = "CREATE TABLE roles ("
		  + "role_id int(11) NOT NULL,"
		  + "role_name text,"
		  + "role_description text,"
		  + "PRIMARY KEY (role_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

public String getQueryCreateRolesTable()
{
	return this.queryCreateRolesTable;
}
private String queryCreateUserAddressTable = "CREATE TABLE user_address ("
		  + "profile_address_id int(11) NOT NULL,"
		  + "uuid int(11) DEFAULT NULL,"
		  + "profile_id int(11) DEFAULT NULL,"
		  + "preferred tinyint(4) DEFAULT NULL,"
		  + "address1 text,"
		  + "address2 text,"
		  + "city_village text,"
		  + "state_province text,"
		  + "postal_code int(11) DEFAULT NULL,"
		  + "country_code int(11) DEFAULT NULL,"
		  + "latitude int(11) DEFAULT NULL,"
		  + "longitude int(11) DEFAULT NULL,"
		  + "creator int(11) DEFAULT NULL,"
		  + "date_created datetime DEFAULT NULL,"
		  + "voided tinyint(4) DEFAULT NULL,"
		  + "date_voided datetime DEFAULT NULL,"
		  + "country_district text,"
		  + "region text,"
		  + "township_division text,"
		  + "PRIMARY KEY (profile_address_id),"
		  + "KEY user_add_uuid_idx (uuid),"
		  + "KEY user_add_profile_id_idx (profile_id),"
		  + "CONSTRAINT user_add_profile_id FOREIGN KEY (profile_id) REFERENCES profiles (profile_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT user_add_uuid FOREIGN KEY (uuid) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

public String getQueryCreateUserAddressTable()
{
	return this.queryCreateUserAddressTable;
}
private String queryCreateUserDataTable = "CREATE TABLE user_data ("
		  + "user_data_record_id int(11) NOT NULL,"
		  + "user_id int(11) DEFAULT NULL,"
		  + "module text,"
		  + "name text,"
		  + "value text,"
		  + "serialized int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (user_data_record_id),"
		  + "KEY uuid_ref_idx (user_id),"
		  + "CONSTRAINT uuid_ref FOREIGN KEY (user_id) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserDataTable()
{
	return this.queryCreateUserDataTable;
}
private String queryCreateUserIdentityTable = "CREATE TABLE user_identity ("
		  + "person_id int(11) NOT NULL,"
		  + "user_profile_id int(11) DEFAULT NULL,"
		  + "user_id int(11) DEFAULT NULL,"
		  + "preffered tinyint(4) DEFAULT NULL,"
		  + "prefix text,"
		  + "given_name text,"
		  + "middle_name text,"
		  + "family_name_prefix text,"
		  + "family_name text,"
		  + "family_name2 text,"
		  + "family_name_suffix text,"
		  + "degree text,"
		  + "creator int(11) DEFAULT NULL,"
		  + "date_created datetime DEFAULT NULL,"
		  + "voided tinyint(4) DEFAULT NULL,"
		  + "date_voided datetime DEFAULT NULL,"
		  + "void_reason text,"
		  + "date_changed datetime DEFAULT NULL,"
		  + "PRIMARY KEY (person_id),"
		  + "KEY user_id_idx (user_id),"
		  + "KEY profile_id_idx (user_profile_id),"
		  + "CONSTRAINT profile_id FOREIGN KEY (user_profile_id) REFERENCES profiles (profile_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserIdentityTable()
{
	return this.queryCreateUserIdentityTable;
}
private String queryCreateUserRolesTable = "CREATE TABLE user_roles ("
		  + "user_id int(11) NOT NULL,"
		  + "role_id int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (user_id),"
		  + "KEY registered_roles_idx (role_id),"
		  + "CONSTRAINT registered_roles FOREIGN KEY (role_id) REFERENCES roles (role_id) ON DELETE NO ACTION ON UPDATE NO ACTION,"
		  + "CONSTRAINT uuid FOREIGN KEY (user_id) REFERENCES users (uuid) ON DELETE NO ACTION ON UPDATE NO ACTION"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUserRolesTable()
{
	return this.queryCreateUserRolesTable;
}
private String queryCreateUsersTable = "CREATE TABLE useraccounts.users ("
		  + "uuid int(11) NOT NULL,"
		  + "user_profile_id int(11) NOT NULL,"
		  + "user_name varchar(15) NOT NULL,"
		  + "user_password varchar(45) DEFAULT NULL,"
		  + "last_password_changed_date datetime DEFAULT NULL,"
		  + "password_change_requested tinyint(4) DEFAULT NULL,"
		  + "password_reset_requested tinyint(4) DEFAULT NULL,"
		  + "user_email varchar(45) DEFAULT NULL,"
		  + "user_touched varchar(45) DEFAULT NULL,"
		  + "user_token varchar(45) DEFAULT NULL,"
		  + "user_email_authenticated tinyint(4) DEFAULT NULL,"
		  + "user_email_token varchar(45) DEFAULT NULL,"
		  + "user_email_token_expires datetime DEFAULT NULL,"
		  + "user_edit_count int(11) DEFAULT NULL,"
		  + "user_language_code int(11) DEFAULT NULL,"
		  + "user_signature varchar(45) DEFAULT NULL,"
		  + "user_create_timestamp datetime DEFAULT NULL,"
		  + "user_login_count int(11) DEFAULT NULL,"
		  + "user_timezone_code int(11) DEFAULT NULL,"
		  + "user_preferred_admin_language_code int(11) DEFAULT NULL,"
		  + "user_init varchar(45) DEFAULT NULL,"
		  + "PRIMARY KEY (uuid,user_name,user_profile_id),"
		  + "KEY language_code_idx (user_language_code),"
		  + "CONSTRAINT language_code FOREIGN KEY (user_language_code) REFERENCES languages.language_codes (language_code)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateUsersTable()
{
   return this.queryCreateUsersTable;
}
private String queryCreateWarningCodesTable = "CREATE TABLE warning_codes ("
		  + "warning_code_id int(11) NOT NULL,"
		  + "warning_code_name text,"
		  + "warning_code_description text,"
		  + "language_code int(11) DEFAULT NULL,"
		  + "PRIMARY KEY (warning_code_id)"
		  + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
public String getQueryCreateWarningCodesTable()
{
	return this.queryCreateWarningCodesTable;
}

private String queryDropCustomerRelationsDatabase = "DROP DATABASE customer_relations;";
public String getQueryDropCustomerRelationsDatabase()
{
	return this.queryDropCustomerRelationsDatabase;
}
private String queryDropErrorsDatabase = "DROP DATABASE errors;";
public String getQueryDropErrorsDatabase()
{
	return this.queryDropErrorsDatabase;
}
private String queryDropEventsDatabase = "DROP DATABASE events;";
public String getQueryDropEventsDatabase()
{
	return this.queryDropEventsDatabase;
}
private String queryDropGenresDatabase = "DROP DATABASE genres;";
public String getQueryDropGenresDatabase()
{
	return this.queryDropGenresDatabase;
}
private String queryDropLanguagesDatabase = "DROP DATABASE languages;";
public String getQueryDropLanguagesDatabase()
{
	return this.queryDropLanguagesDatabase;
}
private String queryDropMenuRoutingDatabse = "DROP DATABASE menu_routing;";
public String getQueryDropMenuRoutingDatabse()
{
	return this.queryDropMenuRoutingDatabse;
}

private String queryDropMessagesDatabase = "DROP DATABASE messages;";
public String getQueryDropMessagesDatabase()
{
	return this.queryDropMessagesDatabase;
}
private String queryDropSessionsDatabase = "DROP DATABASE sessions;";
public String getQueryDropSessionsDatabase()
{
	return this.queryDropSessionsDatabase;
}
private String queryDropSourcesDatabase = "DROP DATABASE sources;";
public String getQueryDropSourcesDatabase()
{
	return this.queryDropSourcesDatabase;
}
private String queryDropStatusesDatabase = "DROP DATABASE statuses;";
public String getQueryDropStatusesDatabase()
{
	return this.queryDropStatusesDatabase;
}
private String queryDropUserMediaDatabase = "DROP DATABASE user_media;";
public String getQueryDropUserMediaDatabase()
{
	return this.queryDropUserMediaDatabase;
}
private String queryDropUserProfiles = "DROP DATABASE user_profiles;";
public String getQueryDropUserProfiles()
{
	return this.queryDropUserProfiles;
}
private String queryDropWarningsProfiles = "DROP DATABASE warnings;";
public String getQueryDropWarningsProfiles()
{
	return this.queryDropWarningsProfiles;
}
private String queryDropAccountDatabase = "DROP DATABASE useraccounts;";
public String getQueryDropAccountDatabase()
{
	return this.queryDropAccountDatabase;
}

    
      
      
}
