package systemconfigurations;

import java.util.Scanner;

import queryhouse.SystemMethods;

public class SetUserPassword {
	public static String setUserPassword()
	 {
		 @SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		 System.out.println(" Password : ");
		 String pass = input.next();
	     //setPassword(password);
	     System.out.println(" Confirm Password : ");
	     String passConfirmation = input.next();
	      
	      if (!pass.equals(passConfirmation))
	      {
	    	  SystemMethods.setErrorMessage("Password did not match!. Please retry.");
	    	  return SystemMethods.getErrorMessage().toString();
	      }
	      else
	      {
	    	  SystemMethods.setPassword(pass);
	    	  SystemMethods.setMessage("Success");
	    	  return SystemMethods.getMessage();
	      }
	 }

}
