package systemconfigurations;

import java.util.Properties;
import queryhouse.SystemMethods;

public class OmniProperties {
	 
	public static void createProperties(
			 String username,
			 String password, 
			 String url, 
			 String className)
	 {
		   try {
			Class.forName(SystemMethods.getDbClassName());
			   
			   SystemMethods.fluidhifiProperties.put("user", 
					   username);
			   SystemMethods.fluidhifiProperties.put("password", 
					   password);
			   SystemMethods.fluidhifiProperties.put("url",
					   url);
			   SystemMethods.fluidhifiProperties.put("classname",
					   className);
			   			   
			   SystemMethods.setMessage("Success");
		   } 
		   catch (ClassNotFoundException e) 
		   {
			   SystemMethods.setErrorMessage(e.getMessage());
		   }

	 }
	
	public static Properties getProperties()
	 {
		 return SystemMethods.fluidhifiProperties;
	 }
	
	public static void writePropertiesFile()
	{
		//TODO: get properties file;
		//      get output filename;
		//      open outputfile;
		//      write properties file in user's home directory
		//      set permissions for intended user access only.
	}
	
}
