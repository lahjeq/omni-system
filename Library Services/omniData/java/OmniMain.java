
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mysqlDataConnection.jdbcConnection;
import systemCheckHome.SystemConfigurationsCheck;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import systemconfigurations.OmniProperties;
//import deamon.DataInsertHelpers;
import core.Core;
import data.XmlReader;

/**
 * @author Billy Bissic
 *
 */
public class OmniMain {

	
	/**
	 * @param args
	 */
	 
public static void main(String[] args) throws ClassNotFoundException, SQLException {
		try {
			//XmlReader.dbInitialization();
			//XmlReader.sysInitialization();
			SystemConfigurationsCheck.loadConfigurations();
			queryhouse.InitalizeDatabase.initializeDatabaseInstall();
			queryhouse.SystemMethods.clearSystemDrives();
			queryhouse.SystemMethods.addSystemDrive(Core.getMediaPath());
			ArrayList<String> temp = new ArrayList<String>();
			ArrayList<String> subDirectories = new ArrayList<String>();
			ArrayList<String> subDirectoryFiles = new ArrayList<String>();
			String directoryPath;
			String albumFilePath;
			String tempPathKey;
			String tempArtistKey;
			String tempAlbumKey;
			String tempFile;
			String tempQuery;
			String tempDirectory;
			String tempLegalTableName;
			String tempLegalAlbumName;
			String tempLegalAlbumFilePath;
						
			if (queryhouse.SystemMethods.getSystemDriveSize() > 0)
			{
				//clear system variables to prevent garbage in file list
				queryhouse.SystemMethods.clearMediaFiles();
				queryhouse.SystemMethods.clearMediaFolders();
				queryhouse.SystemMethods.clearCurrentMediaPath();
				
				//get the first set of files & folders
				deamon.LibraryPopulate.listFiles(queryhouse.SystemMethods.getSystemDrive(0));
				//add the remaining files into the system unsorted album
				if (queryhouse.SystemMethods.getMediaFileSize() > 0)
				{
					temp.clear();
					temp.addAll(queryhouse.SystemMethods.getMediaFiles());
					for (String file : temp){
			            System.out.println(file);			          
			        }
					System.out.println("temp media files : " + temp.size());
				}
				queryhouse.SystemMethods.clearMediaFiles();
				
				deamon.LibraryPopulate.listFolders(queryhouse.SystemMethods.getSystemDrive(0));
				
				System.out.println(queryhouse.SystemMethods.getMediaFolderSize());
				System.out.println(queryhouse.SystemMethods.getMediaFileSize());
				System.out.println(queryhouse.SystemMethods.getSystemDriveSize());
				
				queryhouse.SystemMethods.setCurrentMediaPath(
						queryhouse.SystemMethods.getSystemDrive(0));
				
				directoryPath = queryhouse.SystemMethods.getCurrentMediaPath();
				//get artist names from folders
				//System.out.println(directoryPath);
				
				if (queryhouse.SystemMethods.getMediaFolderSize() > 0)
				{
					temp.clear();
					temp.addAll(queryhouse.SystemMethods.getMediaFolders());
					
					for (String artist : temp){
						//artist database insert
						
/*						queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(
								queryhouse.OmniQueries.queryInsertArtist(artist,
								queryhouse.MediaLibrary.stringReplacement(artist, "'", "\'")
								));*/
						jdbcConnection.runInsertQueryUsingJDBCConnection(
								queryhouse.OmniQueries.queryInsertArtist(deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(artist),
										deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(artist)
										//queryhouse.MediaLibrary.stringReplacement(artist, "'", "\'")
										));
						//go to artist directory
			            queryhouse.SystemMethods.clearMediaFolders();
						//scan for album folders
						deamon.LibraryPopulate.listFolders(directoryPath + "/" + artist);
						subDirectories.clear();
			            subDirectories.addAll(queryhouse.SystemMethods.getMediaFolders());
			            System.out.println(subDirectories.size());
			            if (subDirectories.size() > 0)
			            {
			            	for (String directory : subDirectories)
			            	{
			            		//System.out.println("                " + directory);
			            		//scan for album files
			         
			            		//Insert album into the mp3.album table
			            	   tempLegalAlbumFilePath = queryhouse.MediaLibrary.stringReplacement(directory, "'", "\\'");
			            		tempDirectory = deamon.DataInsertHelpers.generateDatabaseFriendlyTableName(directory);
			            		System.out.println("db friendly table name: " + tempDirectory);
			            		// TODO fix database connection with the library queries.
			            		/*tempArtistKey = queryhouse.OmniCRUDService.runQuerySelectArtistKeyWithNoDatabase(
        								queryhouse.OmniQueries.querySelectArtistKey(
        								//queryhouse.MediaLibrary.stringReplacement(artist, "'", "\'")
        										deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(artist)
        										));*/
			            		tempArtistKey = queryhouse.OmniCRUDService.runQuerySelectArtistKeyWithNoDatabase(queryhouse.OmniQueries.querySelectArtistKey(
			            						deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(artist)));
			            		
			            		System.out.println(tempArtistKey.toString());
			            		
			            		tempQuery = queryhouse.OmniQueries.queryInsertIntoAlbum(
			            				tempLegalAlbumFilePath,
	            						tempDirectory,
	            						tempArtistKey.toString()
	            				);
	            				
			            		System.out.println(tempQuery);
			            		
//			            		queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(tempQuery);
			            		jdbcConnection.runInsertQueryUsingJDBCConnection(tempQuery);
			            		tempLegalTableName = deamon.DataInsertHelpers.generateDatabaseFriendlyTableName(directory);
			            		
			            		tempQuery = queryhouse.OmniQueries.queryCreateAlbumTableInTracksDatabase(
			            				tempLegalTableName,
			            				tempArtistKey.toString()
			            		);
			            		
			            		System.out.println(tempQuery);
			            		jdbcConnection.runInsertQueryUsingJDBCConnection(tempQuery);
			            		//queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(tempQuery);
			            		
		            		   //Insert album into the mp3.paths table
			            		albumFilePath = directoryPath + "/" + artist + "/" + directory;
			            		tempLegalAlbumName = queryhouse.MediaLibrary.stringReplacement(directory, "'", "\\'");
			            		tempLegalAlbumFilePath = queryhouse.MediaLibrary.stringReplacement(albumFilePath,  "'", "\\'");
			            		
			            		tempAlbumKey = 	queryhouse.OmniQueries.querySelectAlbumKey(
			            						tempLegalAlbumName, tempArtistKey.toString());
			            		
			            		System.out.println(tempAlbumKey);
			            		tempAlbumKey = queryhouse.OmniCRUDService.runQuerySelectAlbumKeyWithNoDatabase(tempAlbumKey);
			            		
			            		tempQuery = queryhouse.OmniQueries.queryInsertPath(
			            				tempAlbumKey,
			            				directoryPath, 
			            				tempLegalAlbumFilePath, 
			            				tempLegalAlbumName);
			            		
			            		System.out.println(tempQuery);
			            		jdbcConnection.runInsertQueryUsingJDBCConnection(tempQuery);
			            		//queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(tempQuery);
			            		
			            		subDirectoryFiles.clear();
			            		queryhouse.SystemMethods.clearMediaFiles();
			            		
			            		//Get tracks for album to add to tracks database.
			            		deamon.LibraryPopulate.listFiles(directoryPath + "/" + artist + "/" + directory);
			            		
			            		subDirectoryFiles.addAll(queryhouse.SystemMethods.getMediaFiles());
			            		
			            		for (String file : subDirectoryFiles)
			            		{
			            			if(deamon.DataInsertHelpers.supportedFileExtentions(file)==1)
			            			{
			            				System.out.println(tempArtistKey);
			            				System.out.println(tempAlbumKey);
					            		//tempDirectory = deamon.DataInsertHelpers.generateDatabaseFriendlyTableName(tempDirectory);

			            				tempPathKey = queryhouse.OmniQueries.querySelectPathKey(
														queryhouse.MediaLibrary.stringReplacement(directory, "'", "\\'"));
			            				
			            				System.out.println(tempPathKey);
			            				
										tempPathKey = queryhouse.OmniCRUDService.runQuerySelectPathKeyWithNoDatabase(tempPathKey);
			            				
										System.out.println(tempPathKey);
			            				
			            				tempFile = queryhouse.MediaLibrary.stringReplacement(file,  "'", "\'");
			            				System.out.println(tempFile);
			            				
			            				tempDirectory = deamon.DataInsertHelpers.generateDatabaseFriendlyTableName(directory);
			            				
			            				System.out.println("Directory: " +  directory);
			            				System.out.println("tempDirectory: " + tempDirectory);
			            				file = deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(file);
			            				System.out.println("Filename: " + file);
			            				
				            			tempQuery = queryhouse.OmniQueries.queryInsertAlbum(
				            					
				            					deamon.DataInsertHelpers.generateDatabaseFriendlyTableName(directory),
				            					tempArtistKey.toString(),
				            					tempAlbumKey,
				            					tempPathKey,
				            					file);
				            			
				            			System.out.println(tempQuery);
				            			jdbcConnection.runInsertQueryUsingJDBCConnection(tempQuery);
				            			//queryhouse.OmniCRUDService.runInsertQueryWithNoDatabase(tempQuery);
			            			}
			            	    }
			                }
			            }
			        }
				}			
			}
			
		} catch (SQLException e) {
				e.printStackTrace();
		}
	}
}
