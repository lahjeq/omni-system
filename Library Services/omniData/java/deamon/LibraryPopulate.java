package deamon;
import java.io.File;

public class LibraryPopulate {
	//start scanning paths
	//search for files to add to database
	//add check if file name is short enough to enter the database
	//if not make adjustment; then add files to database
	//if so add file to database
	//if file already exists in the database add it.

	public void listFilesAndFolders(String directoryName){
		 
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            System.out.println(file.getName());
        }
    }
 
    public static void listFiles(String directoryName){
    	System.out.println(directoryName);
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            if (file.isFile()){
            queryhouse.SystemMethods.addFileToMediaFiles(file.getName());
            }
        }
    }
 
    public static void listFolders(String directoryName){
 
         File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            if (file.isDirectory()){
            	queryhouse.SystemMethods.addFolderToMediaFolders(file.getName());
            }
        }
    }
 
    public void listFilesAndFilesSubDirectories(String directoryName){
 
        File directory = new File(directoryName);
 
        //get all the files from a directory
        File[] fList = directory.listFiles();
 
        for (File file : fList){
            if (file.isFile()){
                System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()){
                listFilesAndFilesSubDirectories(file.getAbsolutePath());
            }
        }
    }
 

	
}



