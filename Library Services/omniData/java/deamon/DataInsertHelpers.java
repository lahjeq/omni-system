package deamon;

public class DataInsertHelpers {
	    
	public static Integer supportedFileExtentions(String extension)
	{
		Integer decisionResult;

		if (extension.contains(queryhouse.MediaLibrary.getMp3()))
    	{
			decisionResult = 1;
			return decisionResult;
    	}
		else if (extension.contains(queryhouse.MediaLibrary.getOgg()))
		{
			decisionResult = 1;
			return decisionResult;
		}
		else if (extension.contains(queryhouse.MediaLibrary.getWma()))
		{
			decisionResult = 1;
			return decisionResult;
		}
		else if (extension.contains(queryhouse.MediaLibrary.getWav()))
		{
			decisionResult = 1;
			return decisionResult;
		}
		else if (extension.contains(queryhouse.MediaLibrary.getFlac()))
		{
			decisionResult = 1;
			return decisionResult;
		}
		else if (extension.contains(queryhouse.MediaLibrary.getMp4()))
		{
			decisionResult = 1;
			return decisionResult;
		}
		else {
        //unsupported format
			decisionResult = 0;
			return decisionResult;
		}
	}
    public static String generateDatabaseFriendlyColumnStrings(String columnData)
    {
    	String generatedColumn = null;
    	generatedColumn = columnData;
    	generatedColumn = generatedColumn.replace("�", "\\�");
    	generatedColumn = generatedColumn.replace("'", "\\'");
    	generatedColumn = generatedColumn.replace("\"", "\\\"");
    	
    	return generatedColumn;
  	  
    }
    public static String setColumnLengthLimitationTrim(String columnData, Integer limit)
    {
    	String generatedColumn = null;
    	generatedColumn = columnData.substring(0, limit);
    	return generatedColumn;
    }
	public static String generateDatabaseFriendlyTableName(String name)
	{
	  String generatedTableName = null;
	  queryhouse.SystemMethods.setLength(0);
	  queryhouse.SystemMethods.setLength(name.length());

	  generatedTableName = name;

	  generatedTableName = generatedTableName.replace("'", "");
	  generatedTableName = generatedTableName.replace(".", "");
	  generatedTableName = generatedTableName.replace("-", "_");
	  generatedTableName = generatedTableName.replace(" ", "_");
	  generatedTableName = generatedTableName.replace("_", "_");
	  generatedTableName = generatedTableName.replace(",", "");
	  generatedTableName = generatedTableName.replace("+", "");
	  generatedTableName = generatedTableName.replace("(", "");
	  generatedTableName = generatedTableName.replace(")", "");
	  generatedTableName = generatedTableName.replace("[", "");
	  generatedTableName = generatedTableName.replace("]", "");
	  generatedTableName = generatedTableName.replace("!", "");
	  generatedTableName = generatedTableName.replace("&", "");
	  generatedTableName = generatedTableName.replace("%", "");
	  generatedTableName = generatedTableName.replace("@", "");
	  generatedTableName = generatedTableName.replace("`", "");
	  generatedTableName = generatedTableName.replace("~", "");
	  generatedTableName = generatedTableName.replace("*", "");
	  generatedTableName = generatedTableName.replace("=", "");
	  generatedTableName = generatedTableName.replace("=", "");
	  generatedTableName = generatedTableName.replace("{", "");
	  generatedTableName = generatedTableName.replace("}", "");
	  
	  queryhouse.SystemMethods.setLength(generatedTableName.length());

    if (queryhouse.SystemMethods.getLength() > 64)
    {
       generatedTableName = generatedTableName.substring(0, 63);
    	return generatedTableName;
    }
    else
    {
        return generatedTableName;
    }

	}
}
