package mysqlDataConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import core.Core;
import data.XmlReader;

public class jdbcConnection {
	private static Statement jdbcConnectionStatement;
	private static Connection conn;
	public static void openJdbcConnection() {
	      try {
			 conn = DriverManager.getConnection(Core.getCurrentMysqlDatabaseUrl(),
			            Core.getCurrentMysqlUserId(), 
			            Core.getCurrentMysqlPassword());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static Statement getJdbcConnectionStatement() {
		return jdbcConnectionStatement;
	}
	public static void closeConnection(){
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void closeJdbcConnectionStatement() {
		try {
			jdbcConnectionStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void setJdbcConnectionStatement(Statement jdbcConnectionStatement) {
		jdbcConnection.jdbcConnectionStatement = jdbcConnectionStatement;
	}
	
	public static void executeJdbcUpdateStatement(String sqlStatement){
		try {
			jdbcConnectionStatement.executeUpdate(sqlStatement);
			closeJdbcConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public static ResultSet executeJdbcStatement(String sqlStatement){
		ResultSet rs = null;
		try {
			rs = jdbcConnectionStatement.executeQuery(sqlStatement);
			//closeJdbcConnection();
			} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
		
	}
	public static void closeJdbcConnection() throws SQLException{
		closeConnection();
		closeJdbcConnectionStatement();
	}

	public static void readConnectionConfiguration() throws ClassNotFoundException, SQLException 
	{ 
		try 
		{
			XmlReader.dbInitialization();
			System.out.println(Core.getDatabaseConfigurationName());
			System.out.println(Core.getDatabaseConfigurationType());
			System.out.println(Core.getDatabaseConfigurationDescription());
			System.out.println(Core.getCurrentMysqlUserId());
			System.out.println(Core.getCurrentMysqlPassword());
			System.out.println(Core.getCurrentMysqlClassname());
			System.out.println(Core.getCurrentMysqlJDBCDriver());
			System.out.println(Core.getCurrentMysqlServerId());
			System.out.println(Core.getCurrentMysqlDBPort());
			System.out.println(Core.getCurrentMysqlConnectTimeout());
			System.out.println(Core.getCurrentMysqlSocketTimeout());
			System.out.println(Core.getCurrentMysqlAutoReconnect());
			//printout configurations after read here
			Core.setCurrentMysqlDatabaseUrl();
			System.out.println(Core.getCurrentMysqlDatabaseUrl());
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public static void establishConnectionFromConfiguration() throws ClassNotFoundException, SQLException
	{
		try
		{
			Class.forName(Core.getCurrentMysqlJDBCDriver());
			System.out.println("Connecting to database...");

		   System.out.println("Creating database...");
		   setJdbcConnectionStatement(conn.createStatement());  
		   String sql = "CREATE DATABASE JDBCTEST";
		   executeJdbcUpdateStatement(sql);
		   System.out.println("Database created successfully...");
		   closeJdbcConnection();
		}
		catch (SQLException e) 
		{
			//closeJdbcConnection();
			e.printStackTrace();
		}
		finally
		{
			closeJdbcConnection();
			System.out.println("Closing Connection. In final block of try/catch.");
		}
	}
	
	public static void createDatabaseUsingJDBCConnection(String sqlStatement) throws ClassNotFoundException, SQLException
	{
		try
		{
			Class.forName(Core.getCurrentMysqlJDBCDriver());
			System.out.println("Connecting to database server...");
		   openJdbcConnection();
		   
		   System.out.println("Creating database...");
		   setJdbcConnectionStatement(conn.createStatement());
		   System.out.println("SQL Statement: " + sqlStatement);
		   executeJdbcUpdateStatement(sqlStatement);
		   System.out.println("Database created successfully...");
		   //closeJdbcConnection();
		}
		catch (SQLException e) 
		{
			//closeJdbcConnection();
			e.printStackTrace();
		}
		finally
		{
			closeJdbcConnection();
			System.out.println("Closing Connection. In final block of try/catch.");
		}
	}
	public static void dropDatabaseUsingJDBCConnection(String sqlStatement) throws ClassNotFoundException, SQLException
	{
		try
		{
			Class.forName(Core.getCurrentMysqlJDBCDriver());
			System.out.println("Connecting to database server...");
			openJdbcConnection();
		   System.out.println("Dropping database...");
		   setJdbcConnectionStatement(conn.createStatement());
		   System.out.println("SQL Statement: " + sqlStatement);
		   executeJdbcUpdateStatement(sqlStatement);
		   System.out.println("Database dropped successfully...");
		   //closeJdbcConnection();
		   
		}
		catch (SQLException e) 
		{
			//closeJdbcConnection();
			e.printStackTrace();
		}
		finally
		{
			closeJdbcConnection();
			System.out.println("Closing Connection. In final block of try/catch.");
		}
	}
	public static void dropTableUsingJDBCConnection(String sqlStatement) throws ClassNotFoundException, SQLException
	{
		try
		{
			Class.forName(Core.getCurrentMysqlJDBCDriver());
			System.out.println("Connecting to database server...");
			openJdbcConnection();
		   System.out.println("Creating Table...");
		   setJdbcConnectionStatement(conn.createStatement());
		   System.out.println("SQL Statement: " + sqlStatement);
		   executeJdbcUpdateStatement(sqlStatement);
		   System.out.println("Table dropped successfully...");
		   //closeJdbcConnection();
		   
		}
		catch (SQLException e) 
		{
			//closeJdbcConnection();
			e.printStackTrace();
		}
		finally
		{
			closeJdbcConnection();
			System.out.println("Closing Connection. In final block of try/catch.");
		}
	}
	public static void createTableUsingJDBCConnection(String sqlStatement) throws ClassNotFoundException, SQLException
	{
		try
		{
			Class.forName(Core.getCurrentMysqlJDBCDriver());
			System.out.println("Connecting to database server...");
			openJdbcConnection();
		   System.out.println("Creating table...");
		   setJdbcConnectionStatement(conn.createStatement());
		   System.out.println("SQL Statement: " + sqlStatement);
		   executeJdbcUpdateStatement(sqlStatement);
		   System.out.println("Table created successfully...");
		   //closeJdbcConnection();
		   
		}
		catch (SQLException e) 
		{
			//closeJdbcConnection();
			e.printStackTrace();
		}
		finally
		{
			closeJdbcConnection();
			System.out.println("Closing Connection. In final block of try/catch.");
		}
	}
	public static ResultSet runSelectQueryUsingJDBCConnection(String sqlStatement, Boolean leaveConnectionOpen) throws ClassNotFoundException, SQLException
	{
		ResultSet rs = null;
		try
		{
			Class.forName(Core.getCurrentMysqlJDBCDriver());
			System.out.println("Connecting to database server...");
			openJdbcConnection();
		 
		   System.out.println("Executing Query...");
		   setJdbcConnectionStatement(conn.createStatement());
		   System.out.println("SQL Statement: " + sqlStatement);
		   rs = executeJdbcStatement(sqlStatement);
		   System.out.println("Query Executed successfully...");
		   if (leaveConnectionOpen == false)
		   {
		   closeJdbcConnection();
		   }
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(leaveConnectionOpen == false)
			{
				closeJdbcConnection();
			}
			System.out.println("Closing Connection. In final block of try/catch.");
		}
		return rs;
	}
	public static void runInsertQueryUsingJDBCConnection(String sqlStatement) throws ClassNotFoundException, SQLException
	{
		try
			{
				Class.forName(Core.getCurrentMysqlJDBCDriver());
				System.out.println("Connecting to database server...");
				openJdbcConnection();
			   System.out.println("Inserting Into table...");
			   setJdbcConnectionStatement(conn.createStatement());
			   System.out.println("SQL Statement: " + sqlStatement);
			   executeJdbcUpdateStatement(sqlStatement);
			   System.out.println("Insert completed successfully...");
			   //closeJdbcConnection();
			   
			}
			catch (SQLException e) 
			{
				//closeJdbcConnection();
				e.printStackTrace();
				
			}
			finally
			{
				closeJdbcConnection();
				System.out.println("Closing Connection. In final block of try/catch.");
			}
	}
	
	
}
