package systemCheckHome;

import java.sql.SQLException;

import core.Core;
import data.XmlReader;

public class SystemConfigurationsCheck {
	public static void loadConfigurations() throws ClassNotFoundException, SQLException {
		try {
		// TODO Create database check to check if the system has ran before;
			//if it is the first run, run xmlreader.sysInitalization();
			//else if it has use the system configurations that are in the database.
		System.out.println("Loading System Configurations...");
		XmlReader.sysInitialization();
		System.out.println("Configuration Name: " + Core.getSystemConfigurationName());
		System.out.println("Configuration Type: " + Core.getSystemConfigurationType());
		System.out.println("System Username: " + Core.getSystemCredentialsName());
		System.out.println("System Password: " + Core.getSystemCredentialsPassword());
		System.out.println("Media Path: " + Core.getMediaPath());
		System.out.println("Initilization Path: " + Core.getInitializationPath());
		System.out.println("Configuration Path: " + Core.getConfigurationPath());
		System.out.println("Current Log Directory: " + Core.getCurrentLogDirectory());
		System.out.println("Current Error Directory: " + Core.getCurrentErrorDirectory());
		System.out.println("Current Emergency Directory: " + Core.getCurrentEmergencyDirectory());
		System.out.println("Current Personality Name: " + Core.getPersonalityName());
		System.out.println("Current Personality Type: " + Core.getPersonalityType());
		System.out.println("Configuration Load Complete.");
		// Personality Mods should return an array.
		// Need a for loop to handle the printing of the applied mods.
		//Core.getPersonalityMods();	
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
