package core;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

public class DateTime {

	public static String getFormattedDate()
	{
		DateFormat dateFormat = new SimpleDateFormat(Core.getCurrentDateDefault());
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime().toString());
	}
	
	public static String getFormattedTime()
	{
		DateFormat timeFormat = new SimpleDateFormat(Core.getCurrentDefaultTimeFormat());
		Calendar time = Calendar.getInstance();
		//check for time format, need to know if it's 12 hour or 24 hour format.
		return timeFormat.format(time.getTime().toString());
	}
}
