package core;
import data.Data;
import core.Core;
//import java.sql.Connection;
import java.sql.SQLException;
//import java.sql.Statement;
//import java.sql.DriverManager;

public class InscriberCRUDAction {
  public static void installDatabase() throws ClassNotFoundException
  {
	 Core.setCurrentMysqlDatabase("");
	 Core.setCurrentMysqlUserId("");
	 Core.setCurrentMysqlPassword("");
	 //Core.setCurrentMysqlJdbcClassname("");
	 Core.setCurrentMysqlDBPort("");
	 
	 Core.setCurrentSqlStatement(Data.buildCreateApplicationsTable());
	 Data.executeSqlLive();
	 Core.setCurrentSqlStatement(Data.buildCreateConfigurationsTable());
	 Data.executeSqlLive();
	 Core.setCurrentSqlStatement(Data.buildCreateErrorMessageTable());
	 Data.executeSqlLive();
	 Core.setCurrentSqlStatement(Data.buildCreateLogMessagesTable());
	 Data.executeSqlLive();
  }
  
  public static void checkDatabase() throws ClassNotFoundException, SQLException
  {
	  
  }
  
  public static void writeErrorMessage() throws ClassNotFoundException, SQLException
  {
	 Core.setCurrentMysqlDatabase("");
	 Core.setCurrentMysqlUserId("");
	 Core.setCurrentMysqlPassword("");
	 //Core.setCurrentMysqlJdbcClassname("");
	 Core.setCurrentMysqlDBPort("");
	 
	 Core.setCurrentApplicationID("");
	 Core.setCurrentFilename("");
	 Core.setCurrentCodeLineLocation("");
	 Core.setCurrentFunctionLocation("");
	 Core.setCurrentErrorMessage("");
	 Core.setCurrentTimeCaptured("");
	 
	 Core.setCurrentSqlStatement(Data.buildInsertErrorMessageIntoFluidInscriber());
	 Data.executeSqlLive();
  }
  
  public static void writeLogMessage() throws ClassNotFoundException, SQLException
  {
	 Core.setCurrentMysqlDatabase("");
	 Core.setCurrentMysqlUserId("");
	 Core.setCurrentMysqlPassword("");
  	 //Core.setCurrentMysqlJdbcClassname("");
	 Core.setCurrentMysqlDBPort("");
	 
	 Core.setCurrentApplicationID("");
	 Core.setCurrentFilename("");
	 Core.setCurrentCodeLineLocation("");
	 Core.setCurrentFunctionLocation("");
	 Core.setCurrentLogMessage("");
	 Core.setCurrentTimeCaptured("");
	 
	 Core.setCurrentSqlStatement(Data.buildInsertLogMessageIntoFluidInscriber());
	 Data.executeSqlLive();
  }
  
  public static void createNewInscriberConfiguration() throws ClassNotFoundException, SQLException
  {
	  Core.setCurrentMysqlDatabase("");
	  Core.setCurrentMysqlUserId("");
	  Core.setCurrentMysqlPassword("");
	  //Core.setCurrentMysqlJdbcClassname("");
	  Core.setCurrentMysqlDBPort("");
		 
	  Core.setCurrentConfigurationID("");
	  Core.setCurrentApplicationID("");
	  Core.setCurrentErrorLoggingStatus("");
	  Core.setCurrentErrorLoggingIntensity("");
	  Core.setCurrentInputLoggingStatus("");
	  Core.setCurrentInputLoggingIntensity("");
	  Core.setCurrentOutputLoggingStatus("");
	  Core.setCurrentOutputLoggingIntensity("");
	  Core.setCurrentConfigurationLoggingStatus("");
	  Core.setCurrentConfigurationLoggingIntensity("");
	  
	  Core.setCurrentSqlStatement(Data.buildInsertConfigurationIntoFluidInscriber());
	  Data.executeSqlLive();
  }
  
  public static void createNewApplicationToken() throws ClassNotFoundException, SQLException
  {
	 Core.setCurrentMysqlDatabase("");
	 Core.setCurrentMysqlUserId("");
	 Core.setCurrentMysqlPassword("");
	 //Core.setCurrentMysqlJdbcClassname("");
	 Core.setCurrentMysqlDBPort("");
	  
	 Core.setCurrentApplicationID("");
	 Core.setCurrentApplicationToken("");
	 Core.setCurrentTokenExpiration("");
	  
	 Core.setCurrentSqlStatement(Data.buildInsertTokenIntoFluidScriber());
	 Data.executeSqlLive();
  }
  public void removeDatabase()
  {
	  
  }
  
}
