package core;

import java.sql.Statement;
import java.util.ArrayList;

public class Core {
   private static String  currentSqlStatement;
   private static String  coreXmlFileDir;
   private static String  coreXmlFile;
   private static String  mysqlConnectionUrl;
   private static String  currentMysqlUserId;
   private static String  currentMysqlClassname;
   private static String  currentMysqlJDBCDriver;
   private static String  currentMysqlPassword;
   private static String  currentMysqlDatabase;
   private static String  currentMysqlDBPort;
   private static String  currentMysqlServerId;
   private static String  CurrentMysqlDatabaseUrl;
   private static String  currentMysqlConnectTimeout;
   private static String  currentMysqlSocketTimeout;
   private static String  currentMysqlAutoReconnect;
   private static String  currentApplicationID;
   private static String  currentApplicationName;
   private static String  currentApplicationDescription;
   private static String  currentApplicationOwner;
   private static String  currentApplicationToken;
   private static String  currentTokenExpiration;
   private static String  currentWhereClause;
   private static String  currentConfigurationID;
   private static String  currentErrorLoggingStatus;
   private static String  currentErrorLoggingIntensity;
   private static String  currentInputLoggingStatus;
   private static String  currentInputLoggingIntensity;
   private static String  currentOutputLoggingStatus;
   private static String  currentOutputLoggingIntensity;
   private static String  currentConfigurationLoggingStatus;
   private static String  currentConfigurationLoggingIntensity;
   private static String  currentFilename;
   private static String  currentCodeLineLocation;
   private static String  currentFunctionLocation;
   private static String  currentErrorMessage;
   private static String  currentLogMessage;
   private static String  currentTimeCaptured;
   private static String  currentAccountId;
   private static String  currentUserID;
   private static String  currentFirstName;
   private static String  currentLastName;
   private static String  currentBirthday;
   private static String  currentEmailAddress;
   private static String  currentPhoneNumber;
   private static String  currentPassword;
   private static String  currentZipcode;
   private static String  currentUserLanguage;
   private static String  currentFirstSecurityQuestionID;
   private static String  currentFirstSecurityQuestionAnswer;
   private static String  currentSecondSecurityQuestionID;
   private static String  currentSecondSecurityQuestionAnswer;
   private static String  currentAccountCreateDate;
   private static String  currentAccountTypeCode;
   private static String  currentAccountTypeDescription;
   private static String  currentAccountPriceCode;
   private static String  currentGroupID;
   private static String  currentGroupName;
   private static String  currentGroupDescription;
   private static String  currentUserName;
   private static String  currentLastSuccessfulLogin;
   private static String  currentLastSuccessfulLoginIP;
   private static String  currentSuccessfulLoginCount;
   private static String  currentLastFailedLogin;
   private static String  currentLastFailedLoginIP;
   private static String  currentLastFailedLoginCount;
   private static Boolean databaseLoggingActive;
   private static String  currentLogDirectory;
   private static String  currentEmergencyDirectory;
   private static String  currentMediaDirectory;
   private static String  currentInitializationDirectory;
   private static String  systemConfigurationDirectory;
   private static String  omniInscriberPomFileLocation;
   private static String  systemConfigurationName;
   private static String  systemConfigurationType;
   private static String  personalityName;
   private static String  personalityType;
   private static ArrayList<String> personalityMods = new ArrayList<String> ();
   private static String  personalityPassword;
   private static String  mediaPath;
   private static String  initializationPath;
   private static String  configurationPath;
   private static ArrayList<String> systemMods = new ArrayList<String> ();
   private static ArrayList<String> initialDatabases = new ArrayList<String> ();
   private static String  databaseConfigurationName;
   private static String  databaseConfigurationType;
   private static String  databaseConfigurationDescription;
   private static String  systemCredentialsName;
   private static String  systemCredentialsPassword;
   private static String  currentDateDefault;
   private static String  currentDefaultTimeFormat;
   private static Boolean currentTwelveHourFormat;
   private static ArrayList<String> timeConfigurations = new ArrayList<String> ();
   private static ArrayList<String> dateConfigurations = new ArrayList<String> ();
   
   
   public static String getCurrentDateDefault() {
	return currentDateDefault;
   }
	public static void setCurrentDateDefault(String currentDateDefault) {
		Core.currentDateDefault = currentDateDefault;
	}
	public static String getCurrentDefaultTimeFormat() {
		return currentDefaultTimeFormat;
	}
	public static void setCurrentDefaultTimeFormat(String currentDefaultTimeFormat) {
		Core.currentDefaultTimeFormat = currentDefaultTimeFormat;
	}
	public static Boolean getCurrentTwelveHourFormat() {
		return currentTwelveHourFormat;
	}
	public static void setCurrentTwelveHourFormat(String string) {
		if (string.toUpperCase()=="TRUE")
		{
			Core.currentTwelveHourFormat = true;
		}
		else
		{
			Core.currentTwelveHourFormat = false;
		}
	}
	public static ArrayList<String> getTimeConfigurations() {
		return timeConfigurations;
	}
	public static void setTimeConfigurations(ArrayList<String> timeConfigurations) {
		Core.timeConfigurations = timeConfigurations;
	}
	public static ArrayList<String> getDateConfigurations() {
		return dateConfigurations;
	}
	public static void setDateConfigurations(ArrayList<String> dateConfigurations) {
		Core.dateConfigurations = dateConfigurations;
	}
   
   public static String getDatabaseConfigurationName() {
	   return databaseConfigurationName;
   }
   public static void setDatabaseConfigurationName(String databaseConfigurationName) {
	   Core.databaseConfigurationName = databaseConfigurationName;
   }
   public static String getDatabaseConfigurationType() {
	   return databaseConfigurationType;
   }
   public static void setDatabaseConfigurationType(String databaseConfigurationType) {
	   Core.databaseConfigurationType = databaseConfigurationType;
   }
   public static String getDatabaseConfigurationDescription() {
	   return databaseConfigurationDescription;
   }
   public static void setDatabaseConfigurationDescription(
		   String databaseConfigurationDescription) {
	   Core.databaseConfigurationDescription = databaseConfigurationDescription;
   }
   public static String getSystemCredentialsName() {
	   return systemCredentialsName;
   }
   public static void setSystemCredentialsName(String systemCredentialsName) {
	   Core.systemCredentialsName = systemCredentialsName;
   }
   public static String getSystemCredentialsPassword() {
	   return systemCredentialsPassword;
   }
   public static void setSystemCredentialsPassword(String systemCredentialsPassword) {
	   Core.systemCredentialsPassword = systemCredentialsPassword;
   }
   
   public static String getOmniInscriberPomFileLocation() {
	   return omniInscriberPomFileLocation;
   }
   public static void setOmniInscriberPomFileLocation(
		   String omniInscriberPomFileLocation) {
	   Core.omniInscriberPomFileLocation = omniInscriberPomFileLocation;
   }
   public static String getCoreXmlFileDir() {
	   return coreXmlFileDir;
   }
   public static void setCoreXmlFileDir(String coreXmlFileDir) {
	   Core.coreXmlFileDir = coreXmlFileDir;
   }
   public static String getCoreXmlFile() {
	   return coreXmlFile;
   }
   public static void setCoreXmlFile(String coreXmlFile) {
	   Core.coreXmlFile = coreXmlFile;
   }
   public static Boolean getDatabaseLoggingActive() {
	   return databaseLoggingActive;
   }
   public static void setDatabaseLoggingActive(Boolean databaseLoggingActive) {
	   Core.databaseLoggingActive = databaseLoggingActive;
   }
   public static String getCurrentErrorDirectory() {
	   return currentErrorDirectory;
   }
   public static void setCurrentErrorDirectory(String currentErrorDirectory) {
	   Core.currentErrorDirectory = currentErrorDirectory;
   }
   public static String getCurrentLogDirectory() {
	   return currentLogDirectory;
   }
   public static void setCurrentLogDirectory(String currentLogDirectory) {
	   Core.currentLogDirectory = currentLogDirectory;
   }
   public static String getCurrentEmergencyDirectory() {
	   return currentEmergencyDirectory;
   }
   public static void setCurrentEmergencyDirectory(String currentEmergencyDirectory) {
	   Core.currentEmergencyDirectory = currentEmergencyDirectory;
   }
   public static ArrayList<String> getInitialDatabases() {
	   return initialDatabases;
   }
   public static void setInitialDatabases(ArrayList<String> initialDatabase) {
	   initialDatabases = initialDatabase;
   }
   private static String currentErrorDirectory;

   public static String getSystemConfigurationName() {
	   return systemConfigurationName;
   }
   public static void setSystemConfigurationName(String systemConfigurationName) {
	   Core.systemConfigurationName = systemConfigurationName;
   }
   public static String getSystemConfigurationType() {
	   return systemConfigurationType;
   }
   public static void setSystemConfigurationType(String systemConfigurationType) {
	   Core.systemConfigurationType = systemConfigurationType;
   }
   public static String getPersonalityName() {
	   return personalityName;
   }
   public static void setPersonalityName(String personalityName) {
	   Core.personalityName = personalityName;
   }
   public static String getPersonalityPassword() {
	   return personalityPassword;
   }
   public static void setPersonalityPassword(String personalityPassword) {
	   Core.personalityPassword = personalityPassword;
   }
   public static String getMediaPath() {
	   return mediaPath;
   }
   public static void setMediaPath(String mediaPath) {
	   Core.mediaPath = mediaPath;
   }
   public static String getInitializationPath() {
	   return initializationPath;
   }
   public static void setInitializationPath(String initializationPath) {
	   Core.initializationPath = initializationPath;
   }
   public static String getConfigurationPath() {
	   return configurationPath;
   }
   public static void setConfigurationPath(String configurationPath) {
	   Core.configurationPath = configurationPath;
   }
   public static ArrayList<String> getSystemMods() {
	   return systemMods;
   }
   public static String getCurrentAccountId() 
   {
	   return currentAccountId;
   }
   public static String getCurrentUserID() 
   {
	   return currentUserID;
   }
   public static String getCurrentFirstName() 
   {
	   return currentFirstName;
   }
   public static String getCurrentLastName() 
   {
	   return currentLastName;
   }
   public static String getCurrentBirthday() 
   {
	   return currentBirthday;
   }
   public static String getCurrentEmailAddress() 
   {
	   return currentEmailAddress;
   }
   public static String getCurrentPhoneNumber() 
   {
	   return currentPhoneNumber;
   }
   public static String getCurrentPassword() 
   {
	   return currentPassword;
   }
   public static String getCurrentZipcode() 
   {
	   return currentZipcode;
   }
   public static String getCurrentUserLanguage() 
   {
	   return currentUserLanguage;
   }
   public static String getCurrentFirstSecurityQuestionID() 
   {
	   return currentFirstSecurityQuestionID;
   }
   public static String getCurrentFirstSecurityQuestionAnswer() 
   {
	   return currentFirstSecurityQuestionAnswer;
   }
   public static String getCurrentSecondSecurityQuestionID() 
   {
	   return currentSecondSecurityQuestionID;
   }
   public static String getCurrentSecondSecurityQuestionAnswer() 
   {
	   return currentSecondSecurityQuestionAnswer;
   }
   public static String getCurrentAccountCreateDate() 
   {
	   return currentAccountCreateDate;
   }
   public static String getCurrentAccountTypeCode() 
   {
	   return currentAccountTypeCode;
   }
   public static String getCurrentAccountTypeDescription() 
   {
	   return currentAccountTypeDescription;
   }
   public static String getCurrentAccountPriceCode() 
   {
	   return currentAccountPriceCode;
   }
   public static String getCurrentGroupName() 
   {
	   return currentGroupName;
   }
   public static String getCurrentGroupDescription() 
   {
	   return currentGroupDescription;
   }
   public static String getCurrentUserName() 
   {
	   return currentUserName;
   }
   public static String getCurrentLastSuccessfulLogin() 
   {
	   return currentLastSuccessfulLogin;
   }
   public static String getCurrentLastSuccessfulLoginIP() 
   {
	   return currentLastSuccessfulLoginIP;
   }
   public static String getCurrentGroupID() 
   {
	   return currentGroupID;
   }
   public static String getCurrentSuccessfulLoginCount() 
   {
	   return currentSuccessfulLoginCount;
   }
   public static String getCurrentLastFailedLogin() 
   {
	   return currentLastFailedLogin;
   }
   public static String getCurrentSqlStatement()
   {
	   return currentSqlStatement;
   }
   public static String getMysqlConnectionUrl()
   {
	   return mysqlConnectionUrl;
   }
   public static String getCurrentMysqlUserId()
   {
	   return currentMysqlUserId;
   }
   public static String getCurrentMysqlPassword()
   {
	   return currentMysqlPassword;
   }
   public static String getCurrentMysqlDatabase()
   {
	   return currentMysqlDatabase;
   }

   public static String getCurrentMysqlDBPort()
   {
	   return currentMysqlDBPort;
   }

   public static String getCurrentApplicationID()
   {
	   return currentApplicationID;
   }

   public static String getCurrentApplicationName()
   {
	   return currentApplicationName;
   }

   public static String getCurrentApplicationDescription()
   {
	   return currentApplicationDescription;
   }

   public static String getCurrentApplicationOwner()
   {
	   return currentApplicationOwner;
   }

   public static String getCurrentApplicationToken()
   {
	   return currentApplicationToken;
   }

   public static String getCurrentTokenExpiration()
   {
	   return currentTokenExpiration;
   }

   public static String getCurrentWhereClause()
   {
	   return currentWhereClause;
   }

   public static String getCurrentConfigurationID()
   {
	   return currentConfigurationID;
   }

   public static String getCurrentErrorLoggingStatus()
   {
	   return currentErrorLoggingStatus;
   }

   public static String getCurrentErrorLoggingIntensity()
   {
	   return currentErrorLoggingIntensity;
   }

   public static String getCurrentInputLoggingStatus()
   {
	   return currentInputLoggingStatus;
   }

   public static String getCurrentInputLoggingIntensity()
   {
	   return currentInputLoggingIntensity;
   }

   public static String getCurrentOutputLoggingStatus()
   {
	   return currentOutputLoggingStatus;
   }

   public static String getCurrentOutputLoggingIntensity()
   {
	   return currentOutputLoggingIntensity;
   }

   public static String getCurrentConfigurationLoggingStatus()
   {
	   return currentConfigurationLoggingStatus;
   }

   public static String getCurrentConfigurationLoggingIntensity()
   {
	   return currentConfigurationLoggingIntensity;
   }

   public static String getCurrentFilename()
   {
	   return currentFilename;
   }

   public static String getCurrentCodeLineLocation()
   {
	   return currentCodeLineLocation;
   }

   public static String getCurrentFunctionLocation()
   {
	   return currentFunctionLocation;
   }

   public static String getCurrentErrorMessage()
   {
	   return currentErrorMessage;
   }

   public static String getCurrentLogMessage()
   {
	   return currentLogMessage;
   }

   public static String getCurrentTimeCaptured()
   {
	   return currentTimeCaptured;
   }

   public static String getCurrentLastFailedLoginIP() 
   {
	   return currentLastFailedLoginIP;
   }

   public static String getCurrentLastFailedLoginCount() 
   {
	   return currentLastFailedLoginCount;
   }

   public static String getCurrentMysqlClassname() 
   {
	   return currentMysqlClassname;
   }

   public static void setCurrentSqlStatement(String sqlStatement)
   {
	   currentSqlStatement = sqlStatement;
   }
   public static void setMysqlConnectionUrlNoDB()
   {
	   mysqlConnectionUrl = getCurrentMysqlClassname() + "://" + 
                            getCurrentMysqlServerId() + ":" +
                            getCurrentMysqlDBPort() + "/?" +
                            "connectTimeout=" + getCurrentMysqlConnectTimeout() + "&" +
                            "socketTimeout=" + getCurrentMysqlSocketTimeout() + "&" +
                            "autoReonnect=" + getCurrentMysqlAutoReconnect(); 
  }
   public static void setMysqlConnectionUrl(String database)
   {
	   mysqlConnectionUrl = getCurrentMysqlClassname() + "://" + 
                            getCurrentMysqlServerId() + ":" +
                            getCurrentMysqlDBPort() + "/?" +
                            "connectTimeout=" + getCurrentMysqlConnectTimeout() + "&" +
                            "socketTimeout=" + getCurrentMysqlSocketTimeout() + "&" +
                            "autoReonnect=" + getCurrentMysqlAutoReconnect(); 
  }

   public static void setCurrentMysqlUserId(String userid)
   {
	   currentMysqlUserId = userid;
   }

   public static void setCurrentMysqlPassword(String password)
   {
	   currentMysqlPassword = password;
   }

   public static void setCurrentMysqlDatabase(String database)
   {
	   currentMysqlDatabase = database;
   }

   public static void setCurrentMysqlDBPort(String port)
   {
	   currentMysqlDBPort = port;
   }

   public static void setCurrentApplicationID(String applicationID)
   {
	   currentApplicationID = applicationID;
   }

   public static void setCurrentApplicationName(String applicationName)
   {
	   currentApplicationName = applicationName;
   }

   public static void setCurrentApplicationDescription(String description)
   {
	   currentApplicationDescription = description;
   }

   public static void setCurrentApplicationOwner(String owner)
   {
	   currentApplicationOwner = owner;
   }

   public static void setCurrentApplicationToken(String applicationToken)
   {
	   currentApplicationToken = applicationToken;
   }

   public static void setCurrentTokenExpiration(String timestamp)
   {
	   currentTokenExpiration = timestamp;
   }

   public static void setCurrentWhereClause(String whereClause)
   {
	   currentWhereClause = whereClause;
   }

   public static void setCurrentConfigurationID(String configurationID)
   {
	   currentConfigurationID = configurationID;
   }

   public static void setCurrentErrorLoggingStatus(String errorLoggingStatus)
   {
	   currentErrorLoggingStatus = errorLoggingStatus;
   }

   public static void setCurrentErrorLoggingIntensity(String errorLoggingIntensity)
   {
	   currentErrorLoggingIntensity = errorLoggingIntensity;
   }

   public static void setCurrentInputLoggingStatus(String inputLoggingStatus)
   {
	   currentInputLoggingStatus = inputLoggingStatus;
   }

   public static void setCurrentInputLoggingIntensity(String inputLoggingIntensity)
   {
	   currentInputLoggingIntensity = inputLoggingIntensity;
   }

   public static void setCurrentOutputLoggingStatus(String outputLoggingStatus)
   {
	   currentOutputLoggingStatus = outputLoggingStatus;
   }

   public static void setCurrentOutputLoggingIntensity(String outputLoggingIntensity)
   {
	   currentOutputLoggingIntensity = outputLoggingIntensity;
   }

   public static void setCurrentConfigurationLoggingStatus(String configurationLoggingStatus)
   {
	   currentConfigurationLoggingStatus = configurationLoggingStatus;
   }

   public static void setCurrentConfigurationLoggingIntensity(String configurationLoggingIntensity)
   {
	   currentConfigurationLoggingIntensity = configurationLoggingIntensity;
   }

   public static void setCurrentFilename(String filename)
   {
	   currentFilename = filename;
   }

   public static void setCurrentCodeLineLocation(String codeLineLocation)
   {
	   currentCodeLineLocation = codeLineLocation;
   }

   public static void setCurrentFunctionLocation(String functionLocation)
   {
	   currentFunctionLocation = functionLocation;
   }

   public static void setCurrentErrorMessage(String errorMessage)
   {
	   currentErrorMessage = errorMessage;
   }

   public static void setCurrentLogMessage(String logMessage)
   {
	   currentLogMessage = logMessage;
   }

   public static void setCurrentTimeCaptured(String timeCaptured)
   {
	   currentTimeCaptured = timeCaptured;
   }

   public static void setCurrentAccountPriceCode(String currentAccountPriceCode) 
   {
	   Core.currentAccountPriceCode = currentAccountPriceCode;
   }
   public static void setCurrentGroupID(String currentGroupID) 
   {
	   Core.currentGroupID = currentGroupID;
   }
   public static void setCurrentGroupName(String currentGroupName) 
   {
	   Core.currentGroupName = currentGroupName;
   }
   public static void setCurrentGroupDescription(String currentGroupDescription) 
   {
	   Core.currentGroupDescription = currentGroupDescription;
   }
   public static void setCurrentUserName(String currentUserName) 
   {
	   Core.currentUserName = currentUserName;
   }
   public static void setCurrentLastSuccessfulLogin(
		   String currentLastSuccessfulLogin) 
   {
	   Core.currentLastSuccessfulLogin = currentLastSuccessfulLogin;
   }
   public static void setCurrentLastSuccessfulLoginIP(
		   String currentLastSuccessfulLoginIP) 
   {
	   Core.currentLastSuccessfulLoginIP = currentLastSuccessfulLoginIP;
   }
   public static void setCurrentSuccessfulLoginCount(
		   String currentSuccessfulLoginCount) 
   {
	   Core.currentSuccessfulLoginCount = currentSuccessfulLoginCount;
   }
   public static void setCurrentLastFailedLogin(String currentLastFailedLogin) 
   {
	   Core.currentLastFailedLogin = currentLastFailedLogin;
   }
   public static void setCurrentAccountId(String currentAccountId) 
   {
	   Core.currentAccountId = currentAccountId;
   }
   public static void setCurrentUserID(String currentUserID) {
	   Core.currentUserID = currentUserID;
   }
   public static void setCurrentFirstName(String currentFirstName) {
	   Core.currentFirstName = currentFirstName;
   }
   public static void setCurrentLastName(String currentLastName) {
	   Core.currentLastName = currentLastName;
   }
   public static void setCurrentBirthday(String currentBirthday) {
	   Core.currentBirthday = currentBirthday;
   }
   public static void setCurrentEmailAddress(String currentEmailAddress) {
	   Core.currentEmailAddress = currentEmailAddress;
   }
   public static void setCurrentPhoneNumber(String currentPhoneNumber) {
	   Core.currentPhoneNumber = currentPhoneNumber;
   }
   public static void setCurrentPassword(String currentPassword) {
	   Core.currentPassword = currentPassword;
   }
   public static void setCurrentZipcode(String currentZipcode) {
	   Core.currentZipcode = currentZipcode;
   }
   public static void setCurrentUserLanguage(String currentUserLanguage) {
	   Core.currentUserLanguage = currentUserLanguage;
   }
   public static void setCurrentFirstSecurityQuestionID(
		   String currentFirstSecurityQuestionID) {
	   Core.currentFirstSecurityQuestionID = currentFirstSecurityQuestionID;
   }
   public static void setCurrentFirstSecurityQuestionAnswer(
		   String currentFirstSecurityQuestionAnswer) {
	   Core.currentFirstSecurityQuestionAnswer = currentFirstSecurityQuestionAnswer;
   }
   public static void setCurrentSecondSecurityQuestionID(
		   String currentSecondSecurityQuestionID) {
	   Core.currentSecondSecurityQuestionID = currentSecondSecurityQuestionID;
   }
   public static void setCurrentSecondSecurityQuestionAnswer(
		   String currentSecondSecurityQuestionAnswer) {
	   Core.currentSecondSecurityQuestionAnswer = currentSecondSecurityQuestionAnswer;
   }
   public static void setCurrentAccountCreateDate(String currentAccountCreateDate) {
	   Core.currentAccountCreateDate = currentAccountCreateDate;
   }
   public static void setCurrentAccountTypeCode(String currentAccountTypeCode) {
	   Core.currentAccountTypeCode = currentAccountTypeCode;
   }
   public static void setCurrentAccountTypeDescription(
		   String currentAccountTypeDescription) 
   {
	   Core.currentAccountTypeDescription = currentAccountTypeDescription;
   }
   public static void setCurrentLastFailedLoginIP(String currentLastFailedLoginIP) 
   {
	   Core.currentLastFailedLoginIP = currentLastFailedLoginIP;
   }
   public static void setCurrentLastFailedLoginCount(
		   String currentLastFailedLoginCount) 
   {
	   Core.currentLastFailedLoginCount = currentLastFailedLoginCount;
   }   
   public static void setCurrentMysqlClassname(String currentMysqlClassname) {
	   Core.currentMysqlClassname = currentMysqlClassname;
   }
   public static String getCurrentMediaDirectory() {
	   return currentMediaDirectory;
   }
   public static String getCurrentMysqlJDBCDriver() {
	   return currentMysqlJDBCDriver;
   }
   public static void setCurrentMysqlJDBCDriver(String currentMysqlJDBCDriver) {
	   Core.currentMysqlJDBCDriver = currentMysqlJDBCDriver;
   }
   public static void setCurrentMediaDirectory(String currentMediaDirectory) {
	   Core.currentMediaDirectory = currentMediaDirectory;
   }
   public static String getCurrentInitializationDirectory() {
	   return currentInitializationDirectory;
   }
   public static void setCurrentInitializationDirectory(
		   String currentInitializationDirectory) {
	   Core.currentInitializationDirectory = currentInitializationDirectory;
   }
   public static String getSystemConfigurationDirectory() {
	   return systemConfigurationDirectory;
   }
   public static void setSystemConfigurationDirectory(
		   String systemConfigurationDirectory) {
	   Core.systemConfigurationDirectory = systemConfigurationDirectory;
   }
   public static String getCurrentMysqlServerId() {
	return currentMysqlServerId;
}
public static void setCurrentMysqlServerId(String currentMysqlServerId) {
	Core.currentMysqlServerId = currentMysqlServerId;
}
public static String getCurrentMysqlConnectTimeout() {
	return currentMysqlConnectTimeout;
}
public static void setCurrentMysqlConnectTimeout(
		String currentMysqlConnectTimeout) {
	Core.currentMysqlConnectTimeout = currentMysqlConnectTimeout;
}
public static String getCurrentMysqlSocketTimeout() {
	return currentMysqlSocketTimeout;
}
public static void setCurrentMysqlSocketTimeout(String currentMysqlSocketTimeout) {
	Core.currentMysqlSocketTimeout = currentMysqlSocketTimeout;
}
public static String getCurrentMysqlAutoReconnect() {
	return currentMysqlAutoReconnect;
}
public static void setCurrentMysqlAutoReconnect(String currentMysqlAutoReconnect) {
	Core.currentMysqlAutoReconnect = currentMysqlAutoReconnect;
}

   public static void setSystemMods(ArrayList<String> tempList) {
	   // TODO Auto-generated method stub

   }
/**
 * @return the currentMysqlDatabaseUrl
 */
public static String getCurrentMysqlDatabaseUrl() {
	return CurrentMysqlDatabaseUrl;
}
/**
 * @param currentMysqlDatabaseUrl the currentMysqlDatabaseUrl to set
 */
public static void setCurrentMysqlDatabaseUrl() {
	CurrentMysqlDatabaseUrl = getCurrentMysqlClassname() + "://" + getCurrentMysqlServerId() + "/";
}
public static String getPersonalityType() {
	return personalityType;
}
public static void setPersonalityType(String personalityType) {
	Core.personalityType = personalityType;
}
public static ArrayList<String> getPersonalityMods() {
	return personalityMods;
}
public static void setPersonalityMods(ArrayList<String> personalityMods) {
	Core.personalityMods = personalityMods;
}
}
