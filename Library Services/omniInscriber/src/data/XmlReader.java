package data;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList; 

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import core.Core;


public class XmlReader {


	public static void main(String[] args) throws ClassNotFoundException, SQLException 
	{
		try 
		{
			//dbInitialization();
			//sysInitialization();
			//sysApplicationTypes();
			//sysMessagePriorities();
			//sysMessageTypes();
			sysDateTimeFormats();
			
		}
		catch (Exception ex)
		{
				
		}
	}
	
	public static void sysInitialization() throws ClassNotFoundException, SQLException {
		try
		{
			//System.out.println("Loading System Configurations");
			/* sets pom file location for local application directory */
			//Core.setOmniInscriberPomFileLocation(System.getProperty("user.dir"));
			//Core.setOmniInscriberPomFileLocation(Core.getOmniInscriberPomFileLocation() + "\\poms\\");
			
			//Core.setOmniInscriberPomFileLocation("C:\\omniPoms\\poms");			
			Core.setOmniInscriberPomFileLocation(System.getProperty("user.dir"));
			Core.setOmniInscriberPomFileLocation(Core.getOmniInscriberPomFileLocation() + "\\poms\\");
			System.out.println(Core.getOmniInscriberPomFileLocation());
			Core.setCoreXmlFile("omni-default-system.xml");
			File init = new File(Core.getOmniInscriberPomFileLocation() + Core.getCoreXmlFile());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
			Document doc = dbBuilder.parse(init);
			doc.getDocumentElement().normalize();
			   
			NodeList nodes = doc.getElementsByTagName("configurations");
			
			for (int i = 0; i < nodes.getLength(); i++) {
				   Node node = nodes.item(i);
				   
				   if(node.getNodeType() == Node.ELEMENT_NODE) {
						  Element element = (Element) node;
						  Integer elementCount = 0;
						  
						  elementCount = element.getElementsByTagName("header").getLength();
						  
						  //configuration type
						 
						  Core.setSystemConfigurationName(getValue("name", element, 0));
						  Core.setSystemConfigurationType(getValue("type", element, 0));
						  
						  elementCount = element.getElementsByTagName("credentials").getLength();
					  
						  Core.setSystemCredentialsName(getValue("username", element, 0));
						  Core.setSystemCredentialsPassword(getValue("password", element, 0));
						  
						  elementCount = element.getElementsByTagName("paths").getLength();
						  
						  Core.setMediaPath(getValue("media", element, 0));
						  Core.setInitializationPath(getValue("initialization", element, 0));
						  Core.setConfigurationPath(getValue("system-configuration", element, 0));
						  
						  elementCount = element.getElementsByTagName("logs").getLength();
						  
						  Core.setCurrentLogDirectory(getValue("log", element, 0));
						  Core.setCurrentErrorDirectory(getValue("error", element, 0));
						  Core.setCurrentEmergencyDirectory(getValue("emergency", element, 0));
						  
						  elementCount = element.getElementsByTagName("personalities").getLength();
						  Core.setPersonalityName(getValue("personalityname", element, 0));
						  Core.setPersonalityType(getValue("personalitytype", element, 0)); 
						  
						  elementCount = element.getElementsByTagName("mod").getLength();
						  ArrayList<String> tempList = new ArrayList<String> ();
						  
						  for (int mod = 0; mod < elementCount; mod++) {
							  tempList.add(getValue("mod", element, mod));
						  }
						  
						  Core.setSystemMods(tempList);
				   }
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
    public static void dbInitialization() throws ClassNotFoundException, SQLException {
		try 
		{
		   System.out.println("Loading Configuration");
		   Core.setOmniInscriberPomFileLocation(System.getProperty("user.dir"));
		   Core.setOmniInscriberPomFileLocation(Core.getOmniInscriberPomFileLocation() + "\\poms\\");
		   System.out.println(Core.getOmniInscriberPomFileLocation() + "\\poms\\");
		   Core.setCoreXmlFile("omni-default-db.xml");
		   
		   File init = new File(Core.getOmniInscriberPomFileLocation() + Core.getCoreXmlFile());
		   
		   DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		   DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
		   Document doc = dbBuilder.parse(init);
		   doc.getDocumentElement().normalize();
		   
		   NodeList nodes = doc.getElementsByTagName("configurations");
		   
		   for (int i = 0; i < nodes.getLength(); i++) {
			   Node node = nodes.item(i);
			   			   
			   if(node.getNodeType() == Node.ELEMENT_NODE) {
				  Element element = (Element) node;
				  Integer elementCount = 0;
				  
				  elementCount = element.getElementsByTagName("header").getLength();
				  //configuration type
				  
				  Core.setDatabaseConfigurationName(getValue("name", element, 0));
				  //System.out.println(Core.getDatabaseConfigurationName());
				  Core.setDatabaseConfigurationType(getValue("type", element, 0));
				 // System.out.println(Core.getDatabaseConfigurationType());
				  Core.setDatabaseConfigurationDescription(getValue("description", element, 0));
				  //System.out.println(Core.getDatabaseConfigurationDescription());
				  
				  elementCount = element.getElementsByTagName("connection").getLength();
				  
				  Core.setCurrentMysqlUserId(getValue("username", element, 0));
				  //System.out.println(Core.getCurrentMysqlUserId());
				  Core.setCurrentMysqlPassword(getValue("password", element, 0));
				  //System.out.println(Core.getCurrentMysqlPassword());
				  Core.setCurrentMysqlClassname(getValue("classname", element, 0));
				  //System.out.println(Core.getCurrentMysqlClassname());
				  Core.setCurrentMysqlJDBCDriver(getValue("jdbcdriver", element, 0));
				  //System.out.println(Core.getCurrentMysqlJDBCDriver());
				  Core.setCurrentMysqlServerId(getValue("server", element, 0));
				  //System.out.println(Core.getCurrentMysqlServerId());
				  Core.setCurrentMysqlDBPort(getValue("port", element, 0));
				  //System.out.println(Core.getCurrentMysqlDBPort());
				  Core.setCurrentMysqlConnectTimeout(getValue("connectTimeout", element, 0));
				  //System.out.println(Core.getCurrentMysqlConnectTimeout());
				  Core.setCurrentMysqlSocketTimeout(getValue("socketTimeout", element, 0));
				  //System.out.println(Core.getCurrentMysqlSocketTimeout());
				  Core.setCurrentMysqlAutoReconnect(getValue("autoReconnect", element, 0));
				  //System.out.println(Core.getCurrentMysqlAutoReconnect());
				  	  				  
				  elementCount = 0;
				  elementCount = element.getElementsByTagName("databasenames").getLength();
					  ArrayList<String> tempList = new ArrayList<String> ();
					  for (int database = 0; database < elementCount; database++) {
						  tempList.add(getValue("database", element, database));
				  }
				  Core.setInitialDatabases(tempList);
				  				  			  
			   }
		   }
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
    public static void sysDateTimeFormats() throws ClassNotFoundException, SQLException {
    	try
    	{
    		System.out.println("Loading System Date Configurations");
    		Core.setOmniInscriberPomFileLocation(System.getProperty("user.dir"));
  		    Core.setOmniInscriberPomFileLocation(Core.getOmniInscriberPomFileLocation() + "\\poms\\");
  		    Core.setCoreXmlFile("omni-time-formats.xml");
  		    System.out.println(Core.getOmniInscriberPomFileLocation());
  		    File init = new File(Core.getOmniInscriberPomFileLocation() + Core.getCoreXmlFile());
  		   
  		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
  		    DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
  		    Document doc = dbBuilder.parse(init);
  		    doc.getDocumentElement().normalize();
  		    NodeList nodes = doc.getElementsByTagName("datetimeconfigurations");
  		    for (int i = 0; i < nodes.getLength(); i++) 
  		    {
  		       Node node = nodes.item(i);
	   			   
  			   if(node.getNodeType() == Node.ELEMENT_NODE) 
  			   {
  				  Element element = (Element) node;
  				  Integer elementCount = 0;
  				  elementCount = element.getElementsByTagName("timeconfiguration").getLength();
				  ArrayList<String> tempList = new ArrayList<String> ();
				  for (int timeconfiguration = 0; timeconfiguration < elementCount; timeconfiguration++) {
					  tempList.add(getValue("timeconfiguration", element, timeconfiguration));
				  }
				  
				  Core.setTimeConfigurations(tempList);
				  Core.setCurrentDefaultTimeFormat(getValue("timedefault", element, 0));
				  Core.setCurrentTwelveHourFormat(getValue("twelvehourformat", element, 0));
				  
				  tempList.clear();
				  
				  for (int dateconfiguration = 0; dateconfiguration < elementCount; dateconfiguration++) {
					  tempList.add(getValue("dateconfiguration", element, dateconfiguration));
				  }
				  
				  Core.setCurrentDateDefault(getValue("datedefault", element, 0));
  			   }
  		    }
  		}
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
    	}
    }
    
    private static String getValue(String tag, Element element, Integer index)
	{
		try 
		{
		NodeList nodes = element.getElementsByTagName(tag).item(index).getChildNodes();
		
		Node node = (Node) nodes.item(0);
		return node.getNodeValue();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return ex.toString();
		}
	}
}
