package data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


//import queryhouse.SystemMethods;
import core.Core;
public class Data {
	
	//applications table
	private static String getInsertApplicationIntoFluidInscriber(String applicationID, String applicationName, String applicationDescription, String applicationOwner, String applicationToken)
	{
		String sqlStatement = "INSERT INTO fluidInscriber.applications" + 
						 	  "(applicationID,"+
						      " applicationName," +
							  " applicationDescription,"+
							  " applicationOwner,"+
							  " applicationToken)" +
						  " VALUES" +
						  "(" + applicationID + ","
	   				 	      + applicationName + ","
						 	  + applicationDescription + ","
						 	  + applicationOwner + ","
						 	  + applicationToken + ");";
						 			     
		  return sqlStatement;
	}
	
	public static String buildInsertApplicationIntoFluidInscriber()
	{
		String sqlStatement = getInsertApplicationIntoFluidInscriber(Core.getCurrentApplicationID(),
				Core.getCurrentApplicationName(), Core.getCurrentApplicationDescription(),
				Core.getCurrentApplicationOwner(), Core.getCurrentApplicationToken());
		
		return sqlStatement;
	}
	@SuppressWarnings("unused")
	private String getUpdateApplicationInFluidInscriber(String applicationID, String applicationDescription, String applicationOwner, 
			                                            String applicationToken, String whereClause) 
	{ 
		String sqlStatement = "UPDATE fluidInscriber.applications" +
		                      "SET" +
				              "applicationID = " + applicationID + "," +
		                      "applicationDescription = " + applicationDescription + "," +
				              "applicationOwner = " + applicationOwner + "," +
		                      "applicationToken = " + applicationToken + "," +
	                          "WHERE applicationID = " + whereClause + "," + ";";
		return sqlStatement;
    }
	@SuppressWarnings("unused")
	private String getDeleteApplicationInFluidInscriber(String whereClause) 
	{
		String sqlStatement = "DELETE FROM fluidInscriber.applications";
	
		return sqlStatement;
}
	
    private static String getInsertConfigurationIntoFluidInscriber(String configurationID, String applicationID, String errorLoggingStatus, String errorLoggingIntensity, 
			String inputLoggingStatus, String inputLoggingIntensity, String outputLoggingStatus, String outputLoggingIntensity, 
			String configurationLoggingStatus, String configurationLoggingIntensity) 
	{
		String sqlStatement = "INSERT INTO fluidInscriber.configurations" +
                              "( configurationID," +
				              "  applicationID," +
                              "  errorLoggingStatus," +
                              "  errorLogginIntensity," +
                              "  inputLoggingStatus," +
                              "  inputLoggingIntensity," +
                              "  outputLoggingStatus," +
                              "  outputLoggingIntensity," +
                              "  configuartionLoggingStatus," +
                              "  configurationLoggingIntensity" +
                              "VALUES (" 
                               + configurationID + "," 
                               + applicationID + "," 
                               + errorLoggingStatus + "," 
                               + errorLoggingIntensity + "," 
                               + inputLoggingStatus + "," 
                               + inputLoggingIntensity + "," 
                               + outputLoggingStatus + "," 
                               + outputLoggingIntensity + "," 
                               + configurationLoggingStatus + "," 
                               + configurationLoggingIntensity + ";";
		return sqlStatement;
	}
	
	public static String buildInsertConfigurationIntoFluidInscriber()
	{
		String sqlStatement = getInsertConfigurationIntoFluidInscriber(
				Core.getCurrentConfigurationID(), Core.getCurrentApplicationID(),
				Core.getCurrentErrorLoggingStatus(), Core.getCurrentErrorLoggingIntensity(),
				Core.getCurrentInputLoggingStatus(), Core.getCurrentInputLoggingIntensity(),
				Core.getCurrentOutputLoggingStatus(), Core.getCurrentOutputLoggingIntensity(),
				Core.getCurrentConfigurationLoggingStatus(), Core.getCurrentConfigurationLoggingIntensity());
		return sqlStatement;
	}
	@SuppressWarnings("unused")
	private String getDeleteConfigurationFromFluidInscriber(String whereClause)
	{
	    String sqlStatement = "DELETE FROM fluidInscriber.configurations";
	    return sqlStatement;
    }
	@SuppressWarnings("unused")
	private String getUpdateConfigurationInFluidInscriber(String configurationID, String applicationID, String errorLoggingStatus, String errorLoggingIntensity, 
			                                              String inputLoggingStatus, String inputLoggingIntensity, String outputLoggingStatus, 
			                                              String outputLoggingIntensity, String configurationLoggingStatus, 
			                                              String configurationLoggingIntensity, String whereClause) 
	{ 
		String sqlStatement = "UPDATE fluidInscriber.configurations" +
			                   "SET" +
			                   "configurationID =" + configurationID + "," +
							   "applicationID =" + applicationID + "," +
			                   "errorLoggingStatus =" + errorLoggingStatus + "," + 
							   "errorLogginIntensity =" + errorLoggingIntensity + "," +
			                   "inputLoggingStatus =" + inputLoggingStatus + "," +
							   "inputLoggingIntensity =" + inputLoggingIntensity + "," +
			                   "outputLoggingStatus =" + outputLoggingStatus + "," +
							   "outputLoggingIntensity =" + outputLoggingIntensity + "," +
			                   "configuartionLogging =" + configurationLoggingStatus + "," +
							   "configurationLoggingIntensity =" + configurationLoggingIntensity + 
							   "WHERE configurationID = " + whereClause + ";";
		return sqlStatement;
}	
	private static String getInsertTokenIntoFluidScriber(String applicationID, String applicationToken, String tokenExpiration)
	{
		 String sqlStatement = "INSERT INTO fluidInscriber.token" +
				 			      "(applicationID," +
				 			      "applicationToken," +
				 			      "tokenExpiration)" +
				 			   "VALUES ("
				 			     + applicationID + ","
				 			     + applicationToken + ","
				 			     + tokenExpiration + ");";
		 
		 return sqlStatement;
	}
	
	public static String buildInsertTokenIntoFluidScriber()
	{
		String sqlStatement = getInsertTokenIntoFluidScriber(Core.getCurrentApplicationID(),
				Core.getCurrentApplicationToken(), Core.getCurrentTokenExpiration());
				
		return sqlStatement;
	}
	@SuppressWarnings("unused")
	private String getUpdateTokenInFluidInscriber(String applicationID, String applicationToken, String tokenExpiration, String whereClause) 
	{ 
		String sqlStatement = "UPDATE fluidInscriber.token" +
		"SET" +
		"applicationID = " + applicationID + "," +
		"applicationToken =" + applicationToken + "," +
		"tokenExpiration =" + tokenExpiration + "," +
		"WHERE applicationID =" + whereClause + ";";
		
		return sqlStatement;
	}
	@SuppressWarnings("unused")
	private String deleteTokenFromFluidInscriber(String whereClause)
	{   String sqlStatement = "DELETE FROM fluidInscriber.token WHERE " + whereClause + ";";
	    return sqlStatement;
	}
	
	private static String insertErrorMessageIntoFluidInscriber(String applicationID, String fileName, String codeLineLocation, String functionLocation, 
			String errorMessage, String timeCaptured)
	{ 
		String sqlStatement = "INSERT INTO inscriberData.errorMessages" +
							  "(applicationID," +
							  "fileName," +
							  "codeLineLocation," +
							  "functionLocation," +
						   	  "errorMessage," +
							  "timeCaptured)" +
						   	  "VALUES" +
							  "(" + applicationID + ","
						   	  + fileName + ","
							  + codeLineLocation + ","
						   	  + functionLocation + ","
							  + errorMessage + ","
						   	  + timeCaptured +");";
		return sqlStatement;
	}
	
	public static String buildInsertErrorMessageIntoFluidInscriber()
	{
		String sqlStatement = insertErrorMessageIntoFluidInscriber(Core.getCurrentApplicationID(), 
				Core.getCurrentFilename(), Core.getCurrentCodeLineLocation(), 
				Core.getCurrentFunctionLocation(), Core.getCurrentErrorMessage(),
				Core.getCurrentTimeCaptured());
		
		return sqlStatement;
	}
	@SuppressWarnings("unused")
	private String deleteErrorMessageFromFluidInscriber(String whereClause)
	{
		String sqlStatement = "DELETE FROM inscriberData.errorMessages WHERE " + whereClause + ";";
		return sqlStatement;
	}

	private static String insertLogMessageIntoFluidInscriber(String applicationID, String fileName, String codeLineLocation, String functionLocation, 
			String logMessage, String timeCaptured)
	{
		String sqlStatement = "INSERT INTO inscriberData.logMessage" +
							  "(applicationID," +
							  "fileName," +
							  "codeLineLocation," +
							  "functionLocation," +
							  "logMessage," +
							  "timeCaptured)" +
							  "VALUES" +
							  "(" + applicationID + ","
							  + fileName + ","
							  + codeLineLocation + ","
							  + functionLocation + ","
							  + logMessage + ","
							  + timeCaptured +");";
				return sqlStatement;
	}
	
	public static String buildInsertLogMessageIntoFluidInscriber()
	{
		String sqlStatement = insertLogMessageIntoFluidInscriber(Core.getCurrentApplicationID(), 
				Core.getCurrentFilename(), Core.getCurrentCodeLineLocation(), 
				Core.getCurrentFunctionLocation(), Core.getCurrentLogMessage(),
				Core.getCurrentTimeCaptured());
		return sqlStatement;
	}
	
	@SuppressWarnings("unused")
	private String getDeleteLogMessageFromFluidInscriber(String whereClause) 
	{
		String sqlStatement = "DELETE FROM inscriberData.logMessages WHERE" + whereClause + ";";
		return sqlStatement;
    }
	
	private static String createErrorMessagesTable()
	{
	    String sqlStatement = "CREATE TABLE `errorMessages` (" +
			  "applicationID int(11) NOT NULL," +
			  "fileName varchar(45) NOT NULL," +
			  "codeLineLocation varchar(45) NOT NULL," +
			  "fuctionLocation varchar(45) NOT NULL," +
			  "errorMessage varchar(45) NOT NULL," +
			  "timeCaptured varchar(45) NOT NULL," +
			  "UNIQUE KEY applicationID_UNIQUE (applicationID)" +
			  ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Stores information related to application''s error messages\';";
	    return sqlStatement;
	}
	
	public static String buildCreateErrorMessageTable()
	{
		Core.setCurrentSqlStatement(createErrorMessagesTable());
		return Core.getCurrentSqlStatement();
	}
	
	private static String createLogMessagesTable()
	{
		String sqlStatement = "CREATE TABLE `logMessages` (" +
			  "applicationID` int(11) NOT NULL," +
			  "fileName` varchar(45) NOT NULL," +
			  "lineLocation` varchar(45) NOT NULL," +
			  "functionLocation` varchar(45) NOT NULL," +
			  "logMessage` varchar(45) NOT NULL," +
			  "timeCaptured` varchar(45) NOT NULL," +
			  "PRIMARY KEY (`applicationID`)" +
			  ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Stores information related to application log messages.\';";
		return sqlStatement;
	}
	
	public static String buildCreateLogMessagesTable()
	{
		Core.setCurrentSqlStatement(createLogMessagesTable());
		return Core.getCurrentSqlStatement();
	}
	
	private static String createApplicationsTable()
	{
	    String sqlStatement = "CREATE TABLE `applications` (" +
			  "applicationID int(11) NOT NULL AUTO_INCREMENT," +
			  "applicationName varchar(45) NOT NULL," +
			  "applicationDescription varchar(45) NOT NULL," +
			  "applicationOwner varchar(45) NOT NULL," +
			  "applicationToken varchar(45) DEFAULT NULL," +
			  "PRIMARY KEY (applicationID)" +
			  ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Stores information related to the applications using the inscriber service\';";
	    return sqlStatement;
	}
	
	public static String buildCreateApplicationsTable()
	{
		Core.setCurrentSqlStatement(createApplicationsTable());
		return Core.getCurrentSqlStatement();
	}
	
	private static String createConfigurationsTable()
	{
		String sqlStatement = "CREATE TABLE `configurations` (" +
			  "configurationID int(11) NOT NULL AUTO_INCREMENT," +
			  "applicationID varchar(45) NOT NULL," +
			  "errorLoggingStatus varchar(45) DEFAULT NULL," +
			  "errorLogginIntensity varchar(45) DEFAULT NULL," +
			  "inputLoggingStatus varchar(45) DEFAULT NULL," +
			  "inputLoggingIntensity varchar(45) DEFAULT NULL," +
			  "outputLoggingStatus varchar(45) DEFAULT NULL," +
			  "outputLoggingIntensity varchar(45) DEFAULT NULL," +
			  "configuartionLogging varchar(45) DEFAULT NULL," +
			  "configurationLoggingIntensity varchar(45) DEFAULT NULL," +
			  "PRIMARY KEY (configurationID)," +
			  "UNIQUE KEY applicationID_UNIQUE (applicationID)" +
			  ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'Stores application configurations for each registered application\';";
			
		return sqlStatement;
	}
	
	public static String buildCreateConfigurationsTable()
	{
		Core.setCurrentSqlStatement(createConfigurationsTable());
		return Core.getCurrentSqlStatement();
	}
	
    public String createTokenTable()
    {
		String sqlStatement = "CREATE TABLE token (" +
			  "applicationID int(11) NOT NULL," +
			  "applicationToken timestamp NULL DEFAULT NULL," +
			  "tokenExpiration int(11) DEFAULT NULL," +
			  "PRIMARY KEY (`applicationID`)" +
			  ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores Token Strings \';";
		return sqlStatement;
    }
    
    public String buildCreateTokenTable()
    {
    	Core.setCurrentSqlStatement(createConfigurationsTable());
    	return Core.getCurrentSqlStatement();
    }
    
    public static void executeSqlLive() throws ClassNotFoundException
    {
    	try
    	{
    	    //Class.forName(Core.getMysqlJdbcClassname());
		    Core.setMysqlConnectionUrl(Core.getCurrentMysqlDatabase());
		
    		
    		Connection conn =
    		DriverManager.getConnection(
    				Core.getMysqlConnectionUrl(),
    				Core.getCurrentMysqlUserId(),
    				Core.getCurrentMysqlPassword());
    		
    		Statement stmt = null;
    		
    		stmt = conn.createStatement();
    		Core.getCurrentSqlStatement();
    		stmt.executeUpdate(Core.getCurrentSqlStatement());
    	    
    		stmt.close();
    		conn.close();
    	}
    	catch (SQLException e)
    	{
    		
    	}
    }
    @SuppressWarnings("unused")
    private static String createUserAccountsDatabase()
    {
    	String sqlStatement= "CREATE DATABASE useraccounts;";
    	
    	return sqlStatement;
    }
    @SuppressWarnings("unused")
    private static String createUsersTable()
    { 
    	String sqlStatement = "CREATE TABLE users (" +
    		 " userid int(10) unsigned NOT NULL AUTO_INCREMENT," +
    		 " username varchar(45) NOT NULL," +
    		 " password varchar(45) NOT NULL," +
    		 " groupid int(11) DEFAULT NULL," +
    		 " PRIMARY KEY (`userid`)" +
    		 ") ENGINE=InnoDB DEFAULT CHARSET=latin1;*/";
    	
    	return sqlStatement;
    }
    @SuppressWarnings("unused")
    private static String createUserLoginAttemptsTable(String username, String lastSuccessfulLogin,
    		String lastSuccessfulIP, String successfulLoginCount, String lastFailedLogin,
    		String lastFailedLoginIP, String lastFailedLoginCount)
    {
        String sqlStatement = "CREATE TABLE `userloginattempts` (" +
    		 "username varchar(45) NOT NULL," +
    		 "lastsuccesfullogin datetime NOT NULL," +
    		 "lastsuccessfulip varchar(45) NOT NULL," +
    		 "successfullogincount int(11) NOT NULL," +
    		 "lastfailedlogin datetime NOT NULL," +
    		 "lastfailedloginip varchar(45) NOT NULL," +
    		 "lastfailedlogincount int(11) NOT NULL," +
    		 "PRIMARY KEY (username)" +
    		 ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='stores user account login attempts';";
        
        return sqlStatement;
    }
    @SuppressWarnings("unused")
    private static String createGroupsTable(String groupID, String groupName, 
    		String groupDescription)
    {
       String sqlStatement = "CREATE TABLE groups (" +
    		  "groupid int(10) unsigned NOT NULL AUTO_INCREMENT,"+
    		  "groupname varchar(45) NOT NULL," +
    		  "groupdesc text," +
    		  "PRIMARY KEY (groupid)" +
              ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
       return sqlStatement;
    }
    @SuppressWarnings("unused")
    private static String createAccountTypesTable(String accountTypeCode, 
    		String accountTypeDescription, String accountPriceCode)
    {
    	String sqlStatement = "CREATE TABLE `accounttypes` (" +
    		 " `accounttypecode` int(11) NOT NULL AUTO_INCREMENT," +
    		 " `accounttypedescription` varchar(45) NOT NULL," +
    		 " `accountpricecode` int(11) NOT NULL COMMENT 'foriegn key to accountpricecode.accountpricing'," +
    		 " PRIMARY KEY (`accounttypecode`)" +
    		") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='different account types to determine price';*/";
        
    	return sqlStatement;
    }
    @SuppressWarnings("unused")
    private static String createAccountTypesTable(String accounttypes, String accounttypecode,
    		String accountTypeDescription, String AccountPriceCode)
    {
        String sqlStatement = "CREATE TABLE accounttypes (" +
    		  "accounttypecode int(11) NOT NULL AUTO_INCREMENT," +
    		  "accounttypedescription varchar(45) NOT NULL," +
    		  "accountpricecode int(11) NOT NULL COMMENT 'foriegn key to accountpricecode.accountpricing'," +
    		  "PRIMARY KEY (accounttypecode)" +
    		") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='different account types to determine price';";
        return sqlStatement;
    }
    @SuppressWarnings("unused")
    private static String createUserAccountsAccountsTable(String accountid, String firstName,
      	   String lastName, String birthday, String emailAddress, String phoneNumber,
           String zipcode, String countryCode, String userLanguage, String firstSecurityQuestionID,
           String firstSecurityQuestionAnswer, String seondSecurityquestionID, String secondSecurityQuestionAnswer,
           String accountCreateDate)
     {
      
          String sqlStatement = "CREATE TABLE accounts (" +
             "acountid int(11) NOT NULL AUTO_INCREMENT," +
             "firstname varchar(45) DEFAULT NULL," +
             "lastname varchar(45) DEFAULT NULL," +
             "birthday date NOT NULL," +
             "emailaddress varchar(45) NOT NULL," +
             "phonenumber varchar(45) DEFAULT NULL," +
             "zipcode varchar(10) DEFAULT NULL," +
             "countrycode char(3) DEFAULT NULL," +
             "userlanguage char(30) DEFAULT NULL," +
             "firstsecurityquestionid varchar(45) DEFAULT NULL," +
             "firstsecurityquestionanswer varchar(45) DEFAULT NULL," +
             "secondsecurityquestionid varchar(45) DEFAULT NULL," +
             "secondsecurityquestionanswer varchar(45) DEFAULT NULL," +
             "accountcreatedate datetime NOT NULL," +
             "accountTypeCode varchar(45) DEFAULT NULL," +
             "userid varchar(45) DEFAULT NOT NULL," +
             "PRIMARY KEY (acountid)" +
             ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=\'main account information\';";
     
     return sqlStatement;
     }
    @SuppressWarnings("unused")
    private static String createAccountsTable(String accountID, String firstName, String lastName,
				String birthday, String emailAddress, String phoneNumber, String zipcode,
				String countryCode, String userLanguage, String firstSecurityQuestionID,
				String firstSecurityQuestionAnswer, String secondSecurityQuestionID, 
				String secondSecurityQuestionAnswer, String accountCreateDate, String userID)
    {
		String sqlStatement = "INSERT INTO `useraccounts`.`accounts`" +
								"(`acountid`," +
								"`firstname`," +
								"`lastname`," +
								"`birthday`," +
								"`emailaddress`," +
								"`phonenumber`," +
								"`zipcode`," +
								"`countrycode`," +
								"`userlanguage`," +
								"`firstsecurityquestionid`," +
								"`firstsecurityquestionanswer`," +
								"`secondsecurityquestionid`," +
								"`secondsecurityquestionanswer`," +
								"`accountcreatedate`," +
								"userid)" +
								"VALUES" +
								"(<{acountid: }>," +
								"<{firstname: }>," +
								"<{lastname: }>," +
								"<{birthday: }>," +
								"<{emailaddress: }>," +
								"<{phonenumber: }>," +
								"<{zipcode: }>," +
								"<{countrycode: }>," +
								"<{userlanguage: }>," +
								"<{firstsecurityquestionid: }>," +
								"<{firstsecurityquestionanswer: }>," +
								"<{secondsecurityquestionid: }>," +
								"<{secondsecurityquestionanswer: }>," +
								"<{accountcreatedate: }" +
								"userid>);";

return sqlStatement;

}
    @SuppressWarnings("unused")
	private static String createInsertIntoAccountTypesTable(String accountTypeCode,
			String accountTypeDescription, String accountPriceCode)
	{
		String sqlStatement = "INSERT INTO `useraccounts`.`accounttypes`" +
				"(`accounttypecode`," +
				"`accounttypedescription`," +
				"`accountpricecode`)" +
				"VALUES" +
				"(<{accounttypecode: }>," +
				"<{accounttypedescription: }>," +
				"<{accountpricecode: }>);";

		return sqlStatement;
	}
    @SuppressWarnings("unused")
    private static String createInsertIntoGroupsTable(String groupID, String groupName,
					String groupDescription)
    {
		String sqlStatement = "INSERT INTO `useraccounts`.`groups`" +
					"(`groupid`," +
					"`groupname`," +
					"`groupdesc`)" +
					"VALUES" +
					"(<{groupid: }>," +
					"<{groupname: }>," +
					"<{groupdesc: }>);";
		return sqlStatement;
    }
    @SuppressWarnings("unused")
	private static String createUserAccountsTable(String userName, String userID, String lastsuccesfulLogin,
	String lastSuccessfulIP, String successfulLoginCount, String lastFailedLogin,
	String lastFailedLoginIP, String lastfailedLoginCount)
	{
		String sqlStatement = "INSERT INTO `useraccounts`.`userloginattempts`" +
				"(`username`," +
				" userid," +
				"`lastsuccesfullogin`," +
				"`lastsuccessfulip`," +
				"`successfullogincount`," +
				"`lastfailedlogin`," +
				"`lastfailedloginip`," +
				"`lastfailedlogincount`)" +
				"VALUES" +
				"(<{username: }>," +
				"<{lastsuccesfullogin: }>," +
				"<{lastsuccessfulip: }>," +
				"<{successfullogincount: }>," +
				"<{lastfailedlogin: }>," +
				"<{lastfailedloginip: }>," +
				"<{lastfailedlogincount: }>);";
		
		return sqlStatement;
	}
    @SuppressWarnings("unused")
	private static String createInsertIntoUserAccounts(String userID, String userName, String password,
	 			String groupID)
	{
		String sqlStatement = "INSERT INTO `useraccounts`.`users`" +
				"(`userid`," +
				"`username`," +
				"`password`," +
				"`groupid`)" +
				"VALUES" +
				"(<{userid: }>," +
				"<{username: }>," +
				"<{password: }>," +
				"<{groupid: }>);";
		
		return sqlStatement;
	}
    @SuppressWarnings("unused")
	private static void checkInscribersDatabases()
	{
		/*fluidInscriber
		applications
		configurations
		tokens


	inscriberData
		errorMessages
		logMessages
		variableSnapshots

	envsettings
		driveScheduling
		drivedetails
		drives
		folderDetails
		folderScheduling
		folders
		languagecodes

	driveScheduling
		drivedetails
		drives
		folderDetails
		folderScheduling
		folders
		languagecodes*/

	}
}
