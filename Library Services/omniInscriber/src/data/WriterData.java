package data;

public class WriterData {
	private static String message;
	private static String incidentTime;
	private static String incidentDate; 
	private static String messageType;
	private static String messagePriority;
	private static String fromApplication;
	private static String messageDetails;
	private static String toDatabaseTransactionTime;
	private static String toDatabaseTransactionDate;
	
	public static String getIncidentDate() {
		return incidentDate;
	}
	public static void setIncidentDate(String incidentDate) {
		WriterData.incidentDate = incidentDate;
	}
	public static String getMessage() {
		return message;
	}
	public static void setMessage(String incidentTime, String incidentDate, String messageType, String messagePriority, String fromApplication, String messageDetails, String toDatabaseTransactionTime, String toDatabaseTransactionDate) {
		WriterData.message = incidentTime + messageType + messagePriority + fromApplication + messageDetails + toDatabaseTransactionTime + toDatabaseTransactionDate;
	}
	public static String getToDatabaseTransactionDate() {
		return toDatabaseTransactionDate;
	}
	public static void setToDatabaseTransactionDate(String toDatabaseTransactionDate) {
		WriterData.toDatabaseTransactionDate = toDatabaseTransactionDate;
	}
	public static String getIncidentTime() {
		return incidentTime;
	}
	public static void setIncidentTime(String incidentTime) {
		WriterData.incidentTime = incidentTime;
	}
	public static String getMessageType() {
		return messageType;
	}
	public static void setMessageType(String messageType) {
		WriterData.messageType = messageType;
	}
	public static String getMessagePriority() {
		return messagePriority;
	}
	public static void setMessagePriority(String messagePriority) {
		WriterData.messagePriority = messagePriority;
	}
	public static String getFromApplication() {
		return fromApplication;
	}
	public static void setFromApplication(String fromApplication) {
		WriterData.fromApplication = fromApplication;
	}
	public static String getMessageDetails() {
		return messageDetails;
	}
	public static void setMessageDetails(String messageDetails) {
		WriterData.messageDetails = messageDetails;
	}
	public static String getToDatabaseTransactionTime() {
		return toDatabaseTransactionTime;
	}
	public static void setToDatabaseTransactionTime(String toDatabaseTransactionTime) {
		WriterData.toDatabaseTransactionTime = toDatabaseTransactionTime;
	}
}
