package writer;
import java.sql.SQLException;


public class OmniWriter {

	public static void omniMessageWriter(String incidentTime, String incidentDate, String messageType, String messagePriority, String fromApplication,
			                               String messageDetails, String toDatabaseTransactionTime, String toDatabaseTransactionDate) throws SQLException {
		try {
			
			data.WriterData.setIncidentTime(incidentTime); //here we should allow for flexibility for end users to specific time types to be used.
		    //Formats:                  hh:mm:ss.sss, 12hr/24hr, 
		    //                              hh:mm:ss, 12hr/24hr,
		    //                                 hh:mm, 12hr/24hr, default
			data.WriterData.setIncidentDate(incidentDate);
		    //Formats:         day, month, week, year,
		    //                 day, week, month, year,
		    //                       day, month, year, default,
		    //This data is pulled from xml file initially.
			//--check the database first. 
			//-- if the database connection fails 
			//--then use the xml file.
			//--if neither of those work use the system default
		    data.WriterData.setMessageType(messageType); 
		    // Types:                System Log[SL], use abbreviations for message type during transactions : true/false : default true,
		    //		               System Error[SE], 
		    //                   System Warning[SW], 
		    //              System Notification[SN], 
		    //                  Application Log[AL], 
		    //                Application Error[AE], 
		    //         Application Notification[AN],
		    //              Application Warning[AW],
		    //                  Environment Log[EL],
		    //                Environment Error[EE],
		    //              Environment Warning[EW],
		    //         Environment Notification[EN],
		    data.WriterData.setMessagePriority(messagePriority);
		    // Priorities:              Extreme[EXT], use abbreviations for message priority during transactions : true/false : default true,
		    //                         Ordinary[ORD],
		    //                              Low[LOW];
		    data.WriterData.setFromApplication(fromApplication);
		    // Get Application token for this this should have been done prior to the omniMessageWriter. 
		    //              We'll double check to see if the token is still active at the time of logging being added.
		    data.WriterData.setMessageDetails(messageDetails);
		    //This should contain the details of the message to be submitted.
		    data.WriterData.setToDatabaseTransactionTime(toDatabaseTransactionTime);
		    //Formats:                  hh:mm:ss.sss, 12hr/24hr,
		    //                              hh:mm:ss, 12hr/24hr,
		    //                                 hh:mm, 12hr/24hr, default,
		    data.WriterData.setToDatabaseTransactionDate(toDatabaseTransactionDate);
		    //Formats:         day, month, week, year,
		    //                 day, week, month, year,
		    //                       day, month, year, default
		    //
		    //
		  //This data is pulled from xml file initially.
			//--check the database first. 
			//-- if the database connection fails 
			//--then use the xml file.
			//--if neither of those work use the system default
		    data.WriterData.setMessage(data.WriterData.getIncidentTime(), 
		    						   data.WriterData.getIncidentDate(), 
		    						   data.WriterData.getMessageType(), 
		    						   data.WriterData.getMessagePriority(), 
		    		                   data.WriterData.getFromApplication(), 
		    		                   data.WriterData.getMessageDetails(), 
		    		                   data.WriterData.getToDatabaseTransactionTime(), 
		    		                   data.WriterData.getToDatabaseTransactionDate());
		    System.out.println(data.WriterData.getMessage());
		    System.out.println("Incident Time : Incident Date : Message Type : Message Priority : From Application : Message Details : To Database Transaction Time : To Database Transaction Date");
		
		//get database connection settings
		//--xml reader should have processed and stored information for database connection string already.
		//--so check for existing database string from Data class
		//generate connection string
		//connect to database
		//write log to the transaction database
		//disconnect from database
		//exit routine
		}
		catch (Exception ex) 
		{
		//catching issues.
		//if there are database issues the messages 
		//should be placed in a message queue until 
		//the connection is restored. 
		
		//--upon sql exception
		//test for queue location
		//if queue is good write to it
		//write to new file location.
			ex.printStackTrace();
		}
	}


}
