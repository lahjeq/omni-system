package staticDataBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;

public class QueryFrameworks {

	private static String countryCodeSql;
	private static String languageCodeSql;
	private static String englishShortName;
	private static String frenchShortName;
	private static String alphaTwoCode;
	private static String alphaThreeCode;
	private static String numericCode;
	private static String inputFile;
	private static String outputFile;
	private static String recordDelimiter;
	private static String photographyTypeSql;
	private static String galleryTypeSql;
	private static String mimeTypeSql;
	private static ArrayList<String> outputArray = new ArrayList<String>();
	private static ArrayList<String> inputArray = new ArrayList<String>();
	private static BufferedReader br;
	private static BufferedWriter bw;
	private static FileWriter fw;
	private static FileInputStream fis;
	
	public static ArrayList<String> getOutputArray() {
		return outputArray;
	}
	public static void setOutputArray(ArrayList<String> arg) {
		outputArray = arg;
	}
	public static ArrayList<String> getInputArray() {
		return inputArray;
	}
	public static void setInputArray() throws IOException {
		for (String line = br.readLine(); line != null; line = br.readLine()){
			inputArray.add(line.toString());
			System.out.println(line.toString());
		}
	}
	public static void setInputArray(ArrayList<String> arg) {
		inputArray = arg;
	}
	public static String getInputFile() {
		return inputFile;
	}
	public static void setInputFile(String arg) {
		inputFile = arg;
	}
	public static String getOutputFile() {
		return outputFile;
	}
	public static void setOutputFile(String arg) {
		outputFile = arg;
	}
	public static String getRecordDelimiter() {
		return recordDelimiter;
	}
	public static void setRecordDelimiter(String arg) {
		recordDelimiter = arg;
	}
	public static String getCountryCodeSql() {
		return countryCodeSql;
	}
	public static void setCountryCodeSql(String arg1, String arg2, String arg3, String arg4) {
		countryCodeSql = "INSERT INTO country_codes ("
				+ "                                   english_short_name,      "
				+ "                                       alpha_two_code,      "
				+ "                                     alpha_three_code,      "
				+ "                                         numeric_code)      "
				+ "                                VALUES (                    "
				+ "                       '" + arg1 + "',"
				+ "                       '" + arg2 + "',"
				+ "                       '" + arg3 + "',"
				+ "                       '" + arg4 + "');";
	}
	
	public static void setLanguageCodeSql(String arg1, String arg2) {
		languageCodeSql = "INSERT "
				+ "          INTO "
				+ "                language_codes ("
				+ "                 language_code, "
				+ "                      language )"
				+ "        VALUES "
				+ "                                 ('" + arg1 + "'," 
				+ "                                  '" + arg2 + "');";
	}
	
	public static String getLanguageCodeSql() {
		return languageCodeSql;
	}
	
	public static void setPhotographyTypeSql(String arg1, String arg2) {
		photographyTypeSql = "INSERT "
				+ "             INTO "
				+ "                   photography_services (    "
				+ "                    photography_service,     "
				+ "        photography_service_description )    "
				+ "            VALUES ('" + arg1 + "',          "
			    + "                    '" + arg2 + "');         ";
	}
	
	public static void setGalleryTypeSql(String arg1) {
		galleryTypeSql = "INSERT                         "
				+ "         INTO                         "
				+ "                gallery_group_types ( "
				+ "                         group_name ) "
				+ "       VALUES ('" + arg1 + "');";
	}
	
	public static void setMimeTypeSql(String arg1, String arg2) {
		mimeTypeSql = "INSERT                         "
				+ "      INTO                         "
				+ "           mime_type_category (    "
				+ "                    mime_type,     "
				+ "                 mime_subtype )    "
				+ "    VALUES ( '" + arg1 +        "',"
				+ "             '" + arg2 +        "');";
	}
	
    public static String getMimeTypeSql() {
    	return mimeTypeSql;
    }
	
	public static String getGalleryTypeSql() {
		return galleryTypeSql;
	}
	
	public static String getPhotographyTypeSql() {
		return photographyTypeSql;
	}
	public static String getEnglishShortName() {
		return englishShortName;
	}
	public static void setEnglishShortName(String arg) {
		englishShortName = arg;
	}
	public static String getFrenchShortName() {
		return frenchShortName;
	}
	public static void setFrenchShortName(String arg) {
		frenchShortName = arg;
	}
	public static String getAlphaTwoCode() {
		return alphaTwoCode;
	}
	public static void setAlphaTwoCode(String arg) {
		alphaTwoCode = arg;
	}
	public static String getAlphaThreeCode() {
		return alphaThreeCode;
	}
	public static void setAlphaThreeCode(String arg) {
		alphaThreeCode = arg;
	}
	public static String getNumericCode() {
		return numericCode;
	}
	public static void setNumericCode(String arg) {
		numericCode = arg;
	}
	public static void initializeBufferedWriter(String arg)
	{
		try {
			fw = new FileWriter(arg);
			bw = new BufferedWriter(fw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void initializedBufferedReader(String arg) {
		try {
			fis = new FileInputStream(arg);
			br = new BufferedReader(new InputStreamReader(fis));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void getfileContents() throws IOException
	{
		for (String line = br.readLine(); line != null; line = br.readLine()){
			if ((line.startsWith("-") || (line.startsWith(".")))) {
				inputArray.add(line.toString());
			}
		}
	}
	
	public static void writeNewLine(String line)
	{ 
		try {
			bw.write(line);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void closeBufferedWriter()
	{
		try {
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

