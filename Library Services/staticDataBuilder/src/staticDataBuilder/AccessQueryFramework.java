package staticDataBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Date;
import deamon.DataInsertHelpers;
import staticDataBuilder.QueryFrameworks;


//@SuppressWarnings("unused")
public class AccessQueryFramework 
{
	private static String analyzeData = "";
	private static ArrayList<String> data = new ArrayList<String>();
	private static String[] dataColumns; 

	
    //set input file
	public static void setInputFileForRead()
	{
		staticDataBuilder.QueryFrameworks.setInputFile("country-codes-four-columns.txt");
		staticDataBuilder.QueryFrameworks.initializedBufferedReader(
				staticDataBuilder.QueryFrameworks.getInputFile());
	}
	
	public static void setInputFileForRead(String arg)
	{
		staticDataBuilder.QueryFrameworks.setInputFile(arg);
		staticDataBuilder.QueryFrameworks.initializedBufferedReader(
				staticDataBuilder.QueryFrameworks.getInputFile());
	}
	//set output file
	public static void setOutputFileForRead()
	{
		staticDataBuilder.QueryFrameworks.setOutputFile("country-codes.sql");
		staticDataBuilder.QueryFrameworks.initializeBufferedWriter(
						staticDataBuilder.QueryFrameworks.getOutputFile());
	}
	 
	public static void setOutputFileForRead(String arg)
	{
		staticDataBuilder.QueryFrameworks.setOutputFile(arg);
		staticDataBuilder.QueryFrameworks.initializeBufferedWriter(
						staticDataBuilder.QueryFrameworks.getOutputFile());
	}
	//set delimiter
	public static void setRecordDelimiterForRead()
	{
		staticDataBuilder.QueryFrameworks.setRecordDelimiter(",");
	}
	
	public static void setRecordDelimiterForRead(String arg)
	{
		staticDataBuilder.QueryFrameworks.setRecordDelimiter(arg);
	}
	//read contents into array
	public static void startFileRead()
	{
		try {
			staticDataBuilder.QueryFrameworks.setInputArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//pass contents to QueryFrameworks query generator
	public static void analyzeData()
	{
		data = staticDataBuilder.QueryFrameworks.getInputArray();
		for (int i = 0; i < data.size(); i++) {
			
			analyzeData = data.get(i);
			System.out.println(analyzeData);
			dataColumns = analyzeData.split(
					staticDataBuilder.QueryFrameworks.getRecordDelimiter());
			staticDataBuilder.QueryFrameworks.setCountryCodeSql(
					dataColumns[0].toString().trim(),
					dataColumns[1].toString().trim(),
					dataColumns[2].toString().trim(),
					dataColumns[3].toString().trim()
			);
			System.out.println(staticDataBuilder.QueryFrameworks.getCountryCodeSql());
			sendToOutputFile();
		}
	}
	
	public static void analyzeMimeTypes()
	{
		data = staticDataBuilder.QueryFrameworks.getInputArray();
		for (int i = 0; i < data.size(); i++) {
			 
			analyzeData = data.get(i);
			System.out.println(analyzeData);
			dataColumns = analyzeData.split(
					staticDataBuilder.QueryFrameworks.getRecordDelimiter());
			staticDataBuilder.QueryFrameworks.setMimeTypeSql(
					dataColumns[0].toString().trim(),
					dataColumns[1].toString().trim()
		    );
		System.out.println(staticDataBuilder.QueryFrameworks.getMimeTypeSql());
		sendToOutputFile(staticDataBuilder.QueryFrameworks.getMimeTypeSql());
		}
	}
	
	public static void analyzePhotographyTypes()
	{
		data = staticDataBuilder.QueryFrameworks.getInputArray();
		for (int i = 0; i < data.size(); i++) {
			analyzeData = data.get(i);
			analyzeData.trim();
			analyzeData = deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(analyzeData);
			System.out.println(analyzeData);
			dataColumns = analyzeData.split("\\|");
			staticDataBuilder.QueryFrameworks.setPhotographyTypeSql(
					dataColumns[0].toString().trim(),
					dataColumns[1].toString().trim()
			);
			System.out.println(staticDataBuilder.QueryFrameworks.getPhotographyTypeSql());
			sendToOutputFile(staticDataBuilder.QueryFrameworks.getPhotographyTypeSql());
		}
	}
	
	public static void analyzeLanguageCodes()
	{
		data = staticDataBuilder.QueryFrameworks.getInputArray();
		for (int i = 0; i < data.size(); i++) {
			analyzeData = data.get(i);
			analyzeData.trim();
			analyzeData = deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(analyzeData);
			System.out.println(analyzeData);
			dataColumns = analyzeData.split(staticDataBuilder.QueryFrameworks.getRecordDelimiter());
			
			staticDataBuilder.QueryFrameworks.setLanguageCodeSql(
					deamon.DataInsertHelpers.setColumnLengthLimitationTrim(dataColumns[0].toString().trim(), 3),
					dataColumns[1].toString().trim()
			);
			System.out.println(staticDataBuilder.QueryFrameworks.getLanguageCodeSql());
			sendToOutputFile(staticDataBuilder.QueryFrameworks.getLanguageCodeSql());
		}
	}
	
	public static void analyzeGalleryTypes()
	{
		data = staticDataBuilder.QueryFrameworks.getInputArray();
		for (int i = 0; i < data.size(); i++) {
			analyzeData = data.get(i);
			analyzeData.trim();
			analyzeData = deamon.DataInsertHelpers.generateDatabaseFriendlyColumnStrings(analyzeData);
			System.out.println(analyzeData);
			staticDataBuilder.QueryFrameworks.setGalleryTypeSql(
					analyzeData);
			System.out.println(staticDataBuilder.QueryFrameworks.getGalleryTypeSql());
			sendToOutputFile(staticDataBuilder.QueryFrameworks.getGalleryTypeSql());
		}
	}
	//receive output from query generator and store in an array
	public static void sendToOutputFile()
	{
		staticDataBuilder.QueryFrameworks.writeNewLine(
				staticDataBuilder.QueryFrameworks.getCountryCodeSql());
	}
	
	public static void sendToOutputFile(String arg)
	{
		staticDataBuilder.QueryFrameworks.writeNewLine(arg);
	}
	public static void closeFile(){
		staticDataBuilder.QueryFrameworks.closeBufferedWriter();
	}
	
	public static void photographyTypes() {
		setInputFileForRead("photography-types.txt");
		setOutputFileForRead("photography-types.sql");
		startFileRead();
		setRecordDelimiterForRead("|");
		analyzePhotographyTypes();
		closeFile();
	}
	
	public static void photoGalleryGroupTypes() {
		setInputFileForRead("gallery_types.csv");
		setOutputFileForRead("gallery_types.sql");
		startFileRead();
		analyzeGalleryTypes();
		closeFile();
	}
	
	public static void mimeTypes() {
		setInputFileForRead("mime-types.csv");
		setOutputFileForRead("mime-types.sql");
		setRecordDelimiterForRead(",");
		startFileRead();
		analyzeMimeTypes();
		closeFile();
	}
	public static void languageCodes() {
		setInputFileForRead("language-codes.csv");
		setOutputFileForRead("language-codes.sql");
		setRecordDelimiterForRead(",");
		startFileRead();
		analyzeLanguageCodes();
		closeFile();
	}
	public static void main(String[] args) throws IOException {
	//setInputFileForRead();
	//setOutputFileForRead();
	//startFileRead();
	//setRecordDelimiterForRead();
	//analyzeData();
	//closeFile();
		//photographyTypes();
		//photoGalleryGroupTypes();
		//mimeTypes();
		languageCodes();
	}
//set output file
	//print all output from array to output file
}
