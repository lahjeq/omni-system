package libs.fluidhifi.data;

public class Directory {
	private String directoryPath = null;
	private String directoryGroupID = null;
	private String directoryName = null;
	private String directoryOwner = null;
	/**
	 * @return the directoryPath
	 */
	public String getDirectoryPath() {
		return directoryPath;
	}
	/**
	 * @return the directoryGroupID
	 */
	public String getDirectoryGroupID() {
		return directoryGroupID;
	}
	/**
	 * @return the directoryName
	 */
	public String getDirectoryName() {
		return directoryName;
	}
	/**
	 * @return the directoryOwner
	 */
	public String getDirectoryOwner() {
		return directoryOwner;
	}
	/**
	 * @param directoryPath the directoryPath to set
	 */
	public void setDirectoryPath(String directoryPath) {
		this.directoryPath = directoryPath;
	}
	/**
	 * @param directoryGroupID the directoryGroupID to set
	 */
	public void setDirectoryGroupID(String directoryGroupID) {
		this.directoryGroupID = directoryGroupID;
	}
	/**
	 * @param directoryName the directoryName to set
	 */
	public void setDirectoryName(String directoryName) {
		this.directoryName = directoryName;
	}
	/**
	 * @param directoryOwner the directoryOwner to set
	 */
	public void setDirectoryOwner(String directoryOwner) {
		this.directoryOwner = directoryOwner;
	}
}
