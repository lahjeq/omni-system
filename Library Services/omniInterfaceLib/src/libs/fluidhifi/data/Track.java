package libs.fluidhifi.data;

public class Track {
	private String trackPath = null;
	private String trackTitle = null;
	private String trackSubtitle = null;
	private String trackRating = null;
	private String trackComments = null;
	private String trackDescription = null;
	private String trackMedia = null;
	private String trackContributingArtists = null;
	private String trackAlbumArtist = null;
	private String trackAlbumID = null;
	private String trackAlbumName = null;
	private String trackReleaseYear = null;
	private String trackNumber = null;
	private String trackGenre = null;
	private String trackLength = null;
	private String trackAudio = null;
	private String trackBitRate = null;
	private String trackOrgin = null;
	private String trackPublisher = null;
	private String trackEncoder = null;
	private String trackAuthorURL = null;
	private String trackCopyright = null;
	private String trackContent = null;
	private String trackParentalRating = null;
	private String trackComposers = null;
	private String trackConductors = null;
	private String trackGroupDescription = null;
	private String trackMood = null;
	private String trackPartofSet = null;
	private String trackInitialKey = null;
	private String trackBeatsPerMinute = null;
	private String trackProtection = null;
	private String trackCompilation = null;
	private String trackFileInfo = null;
	private String trackFileName = null;
	private String trackItemType = null;
	private String trackFolderPath = null;
	private String trackDateCreated = null;
	private String trackDateModified = null;
	private String trackSize = null;
	private String trackAttributes = null;
	private String trackOfflineAvailability = null;
	private String trackOfflineStatus = null;
	private String trackSharedWith = null;
	private String trackOwner = null;
	private String trackComputer = null;
	/**
	 * @return the trackPath
	 */
	public String getTrackPath() {
		return trackPath;
	}
	/**
	 * @return the trackTitle
	 */
	public String getTrackTitle() {
		return trackTitle;
	}
	/**
	 * @return the trackSubtitle
	 */
	public String getTrackSubtitle() {
		return trackSubtitle;
	}
	/**
	 * @return the trackRating
	 */
	public String getTrackRating() {
		return trackRating;
	}
	/**
	 * @return the trackComments
	 */
	public String getTrackComments() {
		return trackComments;
	}
	/**
	 * @return the trackDescription
	 */
	public String getTrackDescription() {
		return trackDescription;
	}
	/**
	 * @return the trackMedia
	 */
	public String getTrackMedia() {
		return trackMedia;
	}
	/**
	 * @return the trackContributingArtists
	 */
	public String getTrackContributingArtists() {
		return trackContributingArtists;
	}
	/**
	 * @return the trackAlbumArtist
	 */
	public String getTrackAlbumArtist() {
		return trackAlbumArtist;
	}
	/**
	 * @return the trackAlbumID
	 */
	public String getTrackAlbumID() {
		return trackAlbumID;
	}
	/**
	 * @return the trackAlbumName
	 */
	public String getTrackAlbumName() {
		return trackAlbumName;
	}
	/**
	 * @return the trackReleaseYear
	 */
	public String getTrackReleaseYear() {
		return trackReleaseYear;
	}
	/**
	 * @return the trackNumber
	 */
	public String getTrackNumber() {
		return trackNumber;
	}
	/**
	 * @return the trackGenre
	 */
	public String getTrackGenre() {
		return trackGenre;
	}
	/**
	 * @return the trackLength
	 */
	public String getTrackLength() {
		return trackLength;
	}
	/**
	 * @return the trackAudio
	 */
	public String getTrackAudio() {
		return trackAudio;
	}
	/**
	 * @return the trackBitRate
	 */
	public String getTrackBitRate() {
		return trackBitRate;
	}
	/**
	 * @return the trackOrgin
	 */
	public String getTrackOrgin() {
		return trackOrgin;
	}
	/**
	 * @return the trackPublisher
	 */
	public String getTrackPublisher() {
		return trackPublisher;
	}
	/**
	 * @return the trackEncoder
	 */
	public String getTrackEncoder() {
		return trackEncoder;
	}
	/**
	 * @return the trackAuthorURL
	 */
	public String getTrackAuthorURL() {
		return trackAuthorURL;
	}
	/**
	 * @return the trackCopyright
	 */
	public String getTrackCopyright() {
		return trackCopyright;
	}
	/**
	 * @return the trackContent
	 */
	public String getTrackContent() {
		return trackContent;
	}
	/**
	 * @return the trackParentalRating
	 */
	public String getTrackParentalRating() {
		return trackParentalRating;
	}
	/**
	 * @return the trackComposers
	 */
	public String getTrackComposers() {
		return trackComposers;
	}
	/**
	 * @return the trackConductors
	 */
	public String getTrackConductors() {
		return trackConductors;
	}
	/**
	 * @return the trackGroupDescription
	 */
	public String getTrackGroupDescription() {
		return trackGroupDescription;
	}
	/**
	 * @return the trackMood
	 */
	public String getTrackMood() {
		return trackMood;
	}
	/**
	 * @return the trackPartofSet
	 */
	public String getTrackPartofSet() {
		return trackPartofSet;
	}
	/**
	 * @return the trackInitialKey
	 */
	public String getTrackInitialKey() {
		return trackInitialKey;
	}
	/**
	 * @return the trackBeatsPerMinute
	 */
	public String getTrackBeatsPerMinute() {
		return trackBeatsPerMinute;
	}
	/**
	 * @return the trackProtection
	 */
	public String getTrackProtection() {
		return trackProtection;
	}
	/**
	 * @return the trackCompilation
	 */
	public String getTrackCompilation() {
		return trackCompilation;
	}
	/**
	 * @return the trackFileInfo
	 */
	public String getTrackFileInfo() {
		return trackFileInfo;
	}
	/**
	 * @return the trackFileName
	 */
	public String getTrackFileName() {
		return trackFileName;
	}
	/**
	 * @return the trackItemType
	 */
	public String getTrackItemType() {
		return trackItemType;
	}
	/**
	 * @return the trackFolderPath
	 */
	public String getTrackFolderPath() {
		return trackFolderPath;
	}
	/**
	 * @return the trackDateCreated
	 */
	public String getTrackDateCreated() {
		return trackDateCreated;
	}
	/**
	 * @return the trackDateModified
	 */
	public String getTrackDateModified() {
		return trackDateModified;
	}
	/**
	 * @return the trackSize
	 */
	public String getTrackSize() {
		return trackSize;
	}
	/**
	 * @return the trackAttributes
	 */
	public String getTrackAttributes() {
		return trackAttributes;
	}
	/**
	 * @return the trackOfflineAvailability
	 */
	public String getTrackOfflineAvailability() {
		return trackOfflineAvailability;
	}
	/**
	 * @return the trackOfflineStatus
	 */
	public String getTrackOfflineStatus() {
		return trackOfflineStatus;
	}
	/**
	 * @return the trackSharedWith
	 */
	public String getTrackSharedWith() {
		return trackSharedWith;
	}
	/**
	 * @return the trackOwner
	 */
	public String getTrackOwner() {
		return trackOwner;
	}
	/**
	 * @return the trackComputer
	 */
	public String getTrackComputer() {
		return trackComputer;
	}
	/**
	 * @param trackPath the trackPath to set
	 */
	public void setTrackPath(String trackPath) {
		this.trackPath = trackPath;
	}
	/**
	 * @param trackTitle the trackTitle to set
	 */
	public void setTrackTitle(String trackTitle) {
		this.trackTitle = trackTitle;
	}
	/**
	 * @param trackSubtitle the trackSubtitle to set
	 */
	public void setTrackSubtitle(String trackSubtitle) {
		this.trackSubtitle = trackSubtitle;
	}
	/**
	 * @param trackRating the trackRating to set
	 */
	public void setTrackRating(String trackRating) {
		this.trackRating = trackRating;
	}
	/**
	 * @param trackComments the trackComments to set
	 */
	public void setTrackComments(String trackComments) {
		this.trackComments = trackComments;
	}
	/**
	 * @param trackDescription the trackDescription to set
	 */
	public void setTrackDescription(String trackDescription) {
		this.trackDescription = trackDescription;
	}
	/**
	 * @param trackMedia the trackMedia to set
	 */
	public void setTrackMedia(String trackMedia) {
		this.trackMedia = trackMedia;
	}
	/**
	 * @param trackContributingArtists the trackContributingArtists to set
	 */
	public void setTrackContributingArtists(String trackContributingArtists) {
		this.trackContributingArtists = trackContributingArtists;
	}
	/**
	 * @param trackAlbumArtist the trackAlbumArtist to set
	 */
	public void setTrackAlbumArtist(String trackAlbumArtist) {
		this.trackAlbumArtist = trackAlbumArtist;
	}
	/**
	 * @param trackAlbumID the trackAlbumID to set
	 */
	public void setTrackAlbumID(String trackAlbumID) {
		this.trackAlbumID = trackAlbumID;
	}
	/**
	 * @param trackAlbumName the trackAlbumName to set
	 */
	public void setTrackAlbumName(String trackAlbumName) {
		this.trackAlbumName = trackAlbumName;
	}
	/**
	 * @param trackReleaseYear the trackReleaseYear to set
	 */
	public void setTrackReleaseYear(String trackReleaseYear) {
		this.trackReleaseYear = trackReleaseYear;
	}
	/**
	 * @param trackNumber the trackNumber to set
	 */
	public void setTrackNumber(String trackNumber) {
		this.trackNumber = trackNumber;
	}
	/**
	 * @param trackGenre the trackGenre to set
	 */
	public void setTrackGenre(String trackGenre) {
		this.trackGenre = trackGenre;
	}
	/**
	 * @param trackLength the trackLength to set
	 */
	public void setTrackLength(String trackLength) {
		this.trackLength = trackLength;
	}
	/**
	 * @param trackAudio the trackAudio to set
	 */
	public void setTrackAudio(String trackAudio) {
		this.trackAudio = trackAudio;
	}
	/**
	 * @param trackBitRate the trackBitRate to set
	 */
	public void setTrackBitRate(String trackBitRate) {
		this.trackBitRate = trackBitRate;
	}
	/**
	 * @param trackOrgin the trackOrgin to set
	 */
	public void setTrackOrgin(String trackOrgin) {
		this.trackOrgin = trackOrgin;
	}
	/**
	 * @param trackPublisher the trackPublisher to set
	 */
	public void setTrackPublisher(String trackPublisher) {
		this.trackPublisher = trackPublisher;
	}
	/**
	 * @param trackEncoder the trackEncoder to set
	 */
	public void setTrackEncoder(String trackEncoder) {
		this.trackEncoder = trackEncoder;
	}
	/**
	 * @param trackAuthorURL the trackAuthorURL to set
	 */
	public void setTrackAuthorURL(String trackAuthorURL) {
		this.trackAuthorURL = trackAuthorURL;
	}
	/**
	 * @param trackCopyright the trackCopyright to set
	 */
	public void setTrackCopyright(String trackCopyright) {
		this.trackCopyright = trackCopyright;
	}
	/**
	 * @param trackContent the trackContent to set
	 */
	public void setTrackContent(String trackContent) {
		this.trackContent = trackContent;
	}
	/**
	 * @param trackParentalRating the trackParentalRating to set
	 */
	public void setTrackParentalRating(String trackParentalRating) {
		this.trackParentalRating = trackParentalRating;
	}
	/**
	 * @param trackComposers the trackComposers to set
	 */
	public void setTrackComposers(String trackComposers) {
		this.trackComposers = trackComposers;
	}
	/**
	 * @param trackConductors the trackConductors to set
	 */
	public void setTrackConductors(String trackConductors) {
		this.trackConductors = trackConductors;
	}
	/**
	 * @param trackGroupDescription the trackGroupDescription to set
	 */
	public void setTrackGroupDescription(String trackGroupDescription) {
		this.trackGroupDescription = trackGroupDescription;
	}
	/**
	 * @param trackMood the trackMood to set
	 */
	public void setTrackMood(String trackMood) {
		this.trackMood = trackMood;
	}
	/**
	 * @param trackPartofSet the trackPartofSet to set
	 */
	public void setTrackPartofSet(String trackPartofSet) {
		this.trackPartofSet = trackPartofSet;
	}
	/**
	 * @param trackInitialKey the trackInitialKey to set
	 */
	public void setTrackInitialKey(String trackInitialKey) {
		this.trackInitialKey = trackInitialKey;
	}
	/**
	 * @param trackBeatsPerMinute the trackBeatsPerMinute to set
	 */
	public void setTrackBeatsPerMinute(String trackBeatsPerMinute) {
		this.trackBeatsPerMinute = trackBeatsPerMinute;
	}
	/**
	 * @param trackProtection the trackProtection to set
	 */
	public void setTrackProtection(String trackProtection) {
		this.trackProtection = trackProtection;
	}
	/**
	 * @param trackCompilation the trackCompilation to set
	 */
	public void setTrackCompilation(String trackCompilation) {
		this.trackCompilation = trackCompilation;
	}
	/**
	 * @param trackFileInfo the trackFileInfo to set
	 */
	public void setTrackFileInfo(String trackFileInfo) {
		this.trackFileInfo = trackFileInfo;
	}
	/**
	 * @param trackFileName the trackFileName to set
	 */
	public void setTrackFileName(String trackFileName) {
		this.trackFileName = trackFileName;
	}
	/**
	 * @param trackItemType the trackItemType to set
	 */
	public void setTrackItemType(String trackItemType) {
		this.trackItemType = trackItemType;
	}
	/**
	 * @param trackFolderPath the trackFolderPath to set
	 */
	public void setTrackFolderPath(String trackFolderPath) {
		this.trackFolderPath = trackFolderPath;
	}
	/**
	 * @param trackDateCreated the trackDateCreated to set
	 */
	public void setTrackDateCreated(String trackDateCreated) {
		this.trackDateCreated = trackDateCreated;
	}
	/**
	 * @param trackDateModified the trackDateModified to set
	 */
	public void setTrackDateModified(String trackDateModified) {
		this.trackDateModified = trackDateModified;
	}
	/**
	 * @param trackSize the trackSize to set
	 */
	public void setTrackSize(String trackSize) {
		this.trackSize = trackSize;
	}
	/**
	 * @param trackAttributes the trackAttributes to set
	 */
	public void setTrackAttributes(String trackAttributes) {
		this.trackAttributes = trackAttributes;
	}
	/**
	 * @param trackOfflineAvailability the trackOfflineAvailability to set
	 */
	public void setTrackOfflineAvailability(String trackOfflineAvailability) {
		this.trackOfflineAvailability = trackOfflineAvailability;
	}
	/**
	 * @param trackOfflineStatus the trackOfflineStatus to set
	 */
	public void setTrackOfflineStatus(String trackOfflineStatus) {
		this.trackOfflineStatus = trackOfflineStatus;
	}
	/**
	 * @param trackSharedWith the trackSharedWith to set
	 */
	public void setTrackSharedWith(String trackSharedWith) {
		this.trackSharedWith = trackSharedWith;
	}
	/**
	 * @param trackOwner the trackOwner to set
	 */
	public void setTrackOwner(String trackOwner) {
		this.trackOwner = trackOwner;
	}
	/**
	 * @param trackComputer the trackComputer to set
	 */
	public void setTrackComputer(String trackComputer) {
		this.trackComputer = trackComputer;
	}
}
