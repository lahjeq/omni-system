package libs.fluidhifi.data;

public class SQL {
	private String sqlQuery = null;
	private String sqlServerConnection = null;
	private String sqlUserID = null;
	private String sqlPassword = null;
	private String sqlDatabaseServer = null;
	private String sqlDatabase = null;
	private String sqlDatabaseTable = null; 
	private String sqlDatabasePort = null;
	private String sqlDatabaseEngine = null;
	private String sqlPrimaryKey = null;
	private String sqlUniqueKey = null;
	private int    sqlAutoIncrement = 0;
	private String sqlCharset = null;
	/**
	 * @return the sqlQuery
	 */
	public String getSqlQuery() {
		return sqlQuery;
	}
	/**
	 * @return the sqlServerConnection
	 */
	public String getSqlServerConnection() {
		return sqlServerConnection;
	}
	/**
	 * @return the sqlUserID
	 */
	public String getSqlUserID() {
		return sqlUserID;
	}
	/**
	 * @return the sqlPassword
	 */
	public String getSqlPassword() {
		return sqlPassword;
	}
	/**
	 * @return the sqlDatabaseServer
	 */
	public String getSqlDatabaseServer() {
		return sqlDatabaseServer;
	}
	/**
	 * @return the sqlDatabase
	 */
	public String getSqlDatabase() {
		return sqlDatabase;
	}
	/**
	 * @return the sqlDatabaseTable
	 */
	public String getSqlDatabaseTable() {
		return sqlDatabaseTable;
	}
	/**
	 * @return the sqlDatabasePort
	 */
	public String getSqlDatabasePort() {
		return sqlDatabasePort;
	}
	/**
	 * @return the sqlDatabaseEngine
	 */
	public String getSqlDatabaseEngine() {
		return sqlDatabaseEngine;
	}
	/**
	 * @return the sqlPrimaryKey
	 */
	public String getSqlPrimaryKey() {
		return sqlPrimaryKey;
	}
	/**
	 * @return the sqlUniqueKey
	 */
	public String getSqlUniqueKey() {
		return sqlUniqueKey;
	}
	/**
	 * @return the sqlAutoIncrement
	 */
	public int getSqlAutoIncrement() {
		return sqlAutoIncrement;
	}
	/**
	 * @return the sqlCharset
	 */
	public String getSqlCharset() {
		return sqlCharset;
	}
	/**
	 * @param sqlQuery the sqlQuery to set
	 */
	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}
	/**
	 * @param sqlServerConnection the sqlServerConnection to set
	 */
	public void setSqlServerConnection(String sqlServerConnection) {
		this.sqlServerConnection = sqlServerConnection;
	}
	/**
	 * @param sqlUserID the sqlUserID to set
	 */
	public void setSqlUserID(String sqlUserID) {
		this.sqlUserID = sqlUserID;
	}
	/**
	 * @param sqlPassword the sqlPassword to set
	 */
	public void setSqlPassword(String sqlPassword) {
		this.sqlPassword = sqlPassword;
	}
	/**
	 * @param sqlDatabaseServer the sqlDatabaseServer to set
	 */
	public void setSqlDatabaseServer(String sqlDatabaseServer) {
		this.sqlDatabaseServer = sqlDatabaseServer;
	}
	/**
	 * @param sqlDatabase the sqlDatabase to set
	 */
	public void setSqlDatabase(String sqlDatabase) {
		this.sqlDatabase = sqlDatabase;
	}
	/**
	 * @param sqlDatabaseTable the sqlDatabaseTable to set
	 */
	public void setSqlDatabaseTable(String sqlDatabaseTable) {
		this.sqlDatabaseTable = sqlDatabaseTable;
	}
	/**
	 * @param sqlDatabasePort the sqlDatabasePort to set
	 */
	public void setSqlDatabasePort(String sqlDatabasePort) {
		this.sqlDatabasePort = sqlDatabasePort;
	}
	/**
	 * @param sqlDatabaseEngine the sqlDatabaseEngine to set
	 */
	public void setSqlDatabaseEngine(String sqlDatabaseEngine) {
		this.sqlDatabaseEngine = sqlDatabaseEngine;
	}
	/**
	 * @param sqlPrimaryKey the sqlPrimaryKey to set
	 */
	public void setSqlPrimaryKey(String sqlPrimaryKey) {
		this.sqlPrimaryKey = sqlPrimaryKey;
	}
	/**
	 * @param sqlUniqueKey the sqlUniqueKey to set
	 */
	public void setSqlUniqueKey(String sqlUniqueKey) {
		this.sqlUniqueKey = sqlUniqueKey;
	}
	/**
	 * @param sqlAutoIncrement the sqlAutoIncrement to set
	 */
	public void setSqlAutoIncrement(int sqlAutoIncrement) {
		this.sqlAutoIncrement = sqlAutoIncrement;
	}
	/**
	 * @param sqlCharset the sqlCharset to set
	 */
	public void setSqlCharset(String sqlCharset) {
		this.sqlCharset = sqlCharset;
	}
}
