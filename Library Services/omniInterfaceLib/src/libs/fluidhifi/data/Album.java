package libs.fluidhifi.data;

public class Album {
	private String albumID = null;
	private String albumLink = null;
	private String albumKey = null;
	private String albumName = null;
	private String databaseFormattedAlbumName = null;
	private String albumDirectoryPath = null;
	private String albumArtistID = null;
	private String albumDirectoryPathKey = null;
	private String albumGenre = null;
	private String albumCompusers = null;
	private String albumConductors = null;
	private String albumCopyright = null;
	private String albumParentalRating = null;
	private String albumMood = null;
	/**
	 * @return the albumID
	 */
	public String getAlbumID() {
		return albumID;
	}
	/**
	 * @return the albumName
	 */
	public String getAlbumName() {
		return albumName;
	}
	/**
	 * @return the albumDirectoryPath
	 */
	public String getAlbumDirectoryPath() {
		return albumDirectoryPath;
	}
	/**
	 * @return the albumArtistID
	 */
	public String getAlbumArtistID() {
		return albumArtistID;
	}
	/**
	 * @return the albumDirectoryPathKey
	 */
	public String getAlbumDirectoryPathKey() {
		return albumDirectoryPathKey;
	}
	/**
	 * @return the albumGenre
	 */
	public String getAlbumGenre() {
		return albumGenre;
	}
	/**
	 * @return the albumCompusers
	 */
	public String getAlbumCompusers() {
		return albumCompusers;
	}
	/**
	 * @return the albumConductors
	 */
	public String getAlbumConductors() {
		return albumConductors;
	}
	/**
	 * @return the albumCopyright
	 */
	public String getAlbumCopyright() {
		return albumCopyright;
	}
	/**
	 * @return the albumParentalRating
	 */
	public String getAlbumParentalRating() {
		return albumParentalRating;
	}
	/**
	 * @return the albumMood
	 */
	public String getAlbumMood() {
		return albumMood;
	}
	/**
	 * @param albumID the albumID to set
	 */
	public void setAlbumID(String albumID) {
		this.albumID = albumID;
	}
	/**
	 * @param albumName the albumName to set
	 */
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
	/**
	 * @param albumDirectoryPath the albumDirectoryPath to set
	 */
	public void setAlbumDirectoryPath(String albumDirectoryPath) {
		this.albumDirectoryPath = albumDirectoryPath;
	}
	/**
	 * @param albumArtistID the albumArtistID to set
	 */
	public void setAlbumArtistID(String albumArtistID) {
		this.albumArtistID = albumArtistID;
	}
	/**
	 * @param albumDirectoryPathKey the albumDirectoryPathKey to set
	 */
	public void setAlbumDirectoryPathKey(String albumDirectoryPathKey) {
		this.albumDirectoryPathKey = albumDirectoryPathKey;
	}
	/**
	 * @param albumGenre the albumGenre to set
	 */
	public void setAlbumGenre(String albumGenre) {
		this.albumGenre = albumGenre;
	}
	/**
	 * @param albumCompusers the albumCompusers to set
	 */
	public void setAlbumCompusers(String albumCompusers) {
		this.albumCompusers = albumCompusers;
	}
	/**
	 * @param albumConductors the albumConductors to set
	 */
	public void setAlbumConductors(String albumConductors) {
		this.albumConductors = albumConductors;
	}
	/**
	 * @param albumCopyright the albumCopyright to set
	 */
	public void setAlbumCopyright(String albumCopyright) {
		this.albumCopyright = albumCopyright;
	}
	/**
	 * @param albumParentalRating the albumParentalRating to set
	 */
	public void setAlbumParentalRating(String albumParentalRating) {
		this.albumParentalRating = albumParentalRating;
	}
	/**
	 * @param albumMood the albumMood to set
	 */
	public void setAlbumMood(String albumMood) {
		this.albumMood = albumMood;
	}
	public String getAlbumLink() {
		return albumLink;
	}
	public void setAlbumLink(String albumLink) {
		this.albumLink = albumLink;
	}
	/**
	 * @return the albumKey
	 */
	public String getAlbumKey() {
		return albumKey;
	}
	/**
	 * @param albumKey the albumKey to set
	 */
	public void setAlbumKey(String albumKey) {
		this.albumKey = albumKey;
	}
	/**
	 * @return the databaseFormattedAlbumName
	 */
	public String getDatabaseFormattedAlbumName() {
		return databaseFormattedAlbumName;
	}
	/**
	 * @param databaseFormattedAlbumName the databaseFormattedAlbumName to set
	 */
	public void setDatabaseFormattedAlbumName(String databaseFormattedAlbumName) {
		this.databaseFormattedAlbumName = databaseFormattedAlbumName;
	}
}
