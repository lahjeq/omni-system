package libs.fluidhifi.data;

public class Artist {
private String artistDirectory = null;

/**
 * @return the artistDirectory
 */
public String getArtistDirectory() {
	return artistDirectory;
}

/**
 * @param artistDirectory the artistDirectory to set
 */
public void setArtistDirectory(String artistDirectory) {
	this.artistDirectory = artistDirectory;
}
}
