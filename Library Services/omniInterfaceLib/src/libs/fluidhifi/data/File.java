package libs.fluidhifi.data;

public class File {
private String FilePath;
private String FileName;
private String FileSize;
private String FileOwner;
/**
 * @return the filePath
 */
public String getFilePath() {
	return FilePath;
}
/**
 * @param filePath the filePath to set
 */
public void setFilePath(String filePath) {
	FilePath = filePath;
}
/**
 * @return the fileSize
 */
public String getFileSize() {
	return FileSize;
}
/**
 * @param fileSize the fileSize to set
 */
public void setFileSize(String fileSize) {
	FileSize = fileSize;
}
/**
 * @return the fileName
 */
public String getFileName() {
	return FileName;
}
/**
 * @param fileName the fileName to set
 */
public void setFileName(String fileName) {
	FileName = fileName;
}
/**
 * @return the fileOwner
 */
public String getFileOwner() {
	return FileOwner;
}
/**
 * @param fileOwner the fileOwner to set
 */
public void setFileOwner(String fileOwner) {
	FileOwner = fileOwner;
}
}
