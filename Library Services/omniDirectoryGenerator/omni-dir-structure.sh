#!/bin/bash
#This script was generatedMon Nov 14 15:55:31 CST 2016. It is used to create the directory structure for the omni system.
cd /
mkdir omni
chgrp omni-sys omni
chgrp om-sys omni
chmod 700 omni
cd omni
mkdir source
chgrp omni-sys source
chown om-sys source
chmod 700 source
cd source
mkdir unreal
chgrp omni-sys unreal
chown om-sys unreal
chmod 700 unreal
mkdir anope
chgrp omni-sys anope
chown om-sys anope
chmod 700 anope
mkdir omni
chgrp omni-sys omni
chown om-sys omni
chmod 700 omni
mkdir eggdrop
chgrp omni-sys eggdrop
chown om-sys eggdrop
chmod 700 eggdrop
mkdir mysql
chgrp omni-sys mysql
chown om-sys mysql
chmod 700 mysql
mkdir sqllite
chgrp omni-sys sqllite
chown om-sys sqllite
chmod 700 sqllite
mkdir ioftpd
chgrp omni-sys ioftpd
chmod 700 mkdir ioftpd
cd ..
mkdir console
chgrp omni-sys console
chown om-sys console
chmod 700 console
cd console
mkdir html
chgrp omni-sys html
chown om-sys html
chmod 700 html
mkdir php
chgrp omni-sys php
chown om-sys php
chmod 700 php
mkdir css
chgrp omni-sys css
chown om-sys css
chmod 700 css
mkdir javascript
chgrp omni-sys javascript
chown om-sys javascript
chmod 700 javascript
mkdir images
chgrp omni-sys images
chown om-sys images
chmod 700 images
mkdir docs
chgrp omni-sys docs
chown om-sys docs
chmod 700 docs
mkdir wiki
chgrp omni-sys wiki
chmod 700 mkdir wiki
cd ..
mkdir circulations
chgrp omni-sys circulations
chown om-sys circulations
chmod 700 circulations
cd circulations
mkdir daemon
chgrp omni-sys daemon
chown om-sys daemon
chmod 700 daemon
cd daemon
mkdir ioftpd
chgrp omni-sys ioftpd
chown om-sys ioftpd
chmod 700 ioftpd
cd ioftpd
mkdir version
chgrp omni-sys version
chmod 700 mkdir version
cd ..
mkdir ircd
chgrp omni-sys ircd
chown om-sys ircd
chmod 700 ircd
cd ircd
mkdir daemon
chgrp omni-sys daemon
chown om-sys daemon
chmod 700 daemon
cd daemon
mkdir unreal
chgrp omni-sys unreal
chown om-sys unreal
chmod 700 unreal
cd unreal
mkdir 3.2.8
chgrp omni-sys 3.2.8
chmod 700 mkdir 3.2.8
cd ..
mkdir services
chgrp omni-sys services
chown om-sys services
chmod 700 services
cd services
mkdir anope
chgrp omni-sys anope
chmod 700 mkdir anope
cd ..
mkdir servers
chgrp omni-sys servers
chown om-sys servers
chmod 700 servers
cd servers
mkdir java
chgrp omni-sys java
chown om-sys java
chmod 700 java
mkdir c
chgrp omni-sys c
chmod 700 mkdir c
cd ..
mkdir droplets
chgrp omni-sys droplets
chown om-sys droplets
chmod 700 droplets
cd droplets
mkdir cerebrum
chgrp omni-sys cerebrum
chown om-sys cerebrum
chmod 700 cerebrum
mkdir frontal_lobe
chgrp omni-sys frontal_lobe
chown om-sys frontal_lobe
chmod 700 frontal_lobe
mkdir parietal_lobe
chgrp omni-sys parietal_lobe
chown om-sys parietal_lobe
chmod 700 parietal_lobe
mkdir sensory_lobe
chgrp omni-sys sensory_lobe
chown om-sys sensory_lobe
chmod 700 sensory_lobe
mkdir motor_cortex
chgrp omni-sys motor_cortex
chown om-sys motor_cortex
chmod 700 motor_cortex
mkdir temporal_lobe
chgrp omni-sys temporal_lobe
chown om-sys temporal_lobe
chmod 700 temporal_lobe
mkdir wernicke�s_area
chgrp omni-sys wernicke�s_area
chown om-sys wernicke�s_area
chmod 700 wernicke�s_area
mkdir occipital_lobe
chgrp omni-sys occipital_lobe
chown om-sys occipital_lobe
chmod 700 occipital_lobe
mkdir broca�s_area
chgrp omni-sys broca�s_area
chown om-sys broca�s_area
chmod 700 broca�s_area
mkdir cerebellum
chgrp omni-sys cerebellum
chown om-sys cerebellum
chmod 700 cerebellum
mkdir limbic_system
chgrp omni-sys limbic_system
chown om-sys limbic_system
chmod 700 limbic_system
mkdir amygdala
chgrp omni-sys amygdala
chown om-sys amygdala
chmod 700 amygdala
mkdir hippocampus
chgrp omni-sys hippocampus
chown om-sys hippocampus
chmod 700 hippocampus
mkdir hypothalamus
chgrp omni-sys hypothalamus
chown om-sys hypothalamus
chmod 700 hypothalamus
mkdir thalamus
chgrp omni-sys thalamus
chown om-sys thalamus
chmod 700 thalamus
mkdir brain_stem
chgrp omni-sys brain_stem
chown om-sys brain_stem
chmod 700 brain_stem
mkdir mid_brain
chgrp omni-sys mid_brain
chown om-sys mid_brain
chmod 700 mid_brain
mkdir pons
chgrp omni-sys pons
chown om-sys pons
chmod 700 pons
mkdir medulla
chgrp omni-sys medulla
chmod 700 mkdir medulla
cd ..
mkdir dormant
chgrp omni-sys dormant
chown om-sys dormant
chmod 700 dormant
cd dormant
mkdir cranium
chgrp omni-sys cranium
chmod 700 mkdir cranium
cd ..
mkdir cranium
chgrp omni-sys cranium
chown om-sys cranium
chmod 700 cranium
cd cranium
mkdir cerebrum
chgrp omni-sys cerebrum
chown om-sys cerebrum
chmod 700 cerebrum
cd cerebrum
mkdir left_hemisphere
chgrp omni-sys left_hemisphere
chown om-sys left_hemisphere
chmod 700 left_hemisphere
cd left_hemisphere
mkdir temporal_lobe
chgrp omni-sys temporal_lobe
chown om-sys temporal_lobe
chmod 700 temporal_lobe
cd temporal_lobe
mkdir cerebral_hemisphere
chgrp omni-sys cerebral_hemisphere
chown om-sys cerebral_hemisphere
chmod 700 cerebral_hemisphere
cd cerebral_hemisphere
mkdir visual_memories
chgrp omni-sys visual_memories
chown om-sys visual_memories
chmod 700 visual_memories
mkdir auditory_memories
chgrp omni-sys auditory_memories
chown om-sys auditory_memories
chmod 700 auditory_memories
mkdir speech_capabilities
chgrp omni-sys speech_capabilities
chown om-sys speech_capabilities
chmod 700 speech_capabilities
mkdir hearing_capabilities
chgrp omni-sys hearing_capabilities
chown om-sys hearing_capabilities
chmod 700 hearing_capabilities
mkdir behavioral_elements
chgrp omni-sys behavioral_elements
chown om-sys behavioral_elements
chmod 700 behavioral_elements
mkdir language
chgrp omni-sys language
chmod 700 mkdir language
cd ..
mkdir medial_temporal_lobe
chgrp omni-sys medial_temporal_lobe
chown om-sys medial_temporal_lobe
chmod 700 medial_temporal_lobe
cd medial_temporal_lobe
mkdir hippocampus
chgrp omni-sys hippocampus
chown om-sys hippocampus
chmod 700 hippocampus
cd hippocampus
mkdir long_term_memory_index
chgrp omni-sys long_term_memory_index
chown om-sys long_term_memory_index
chmod 700 long_term_memory_index
mkdir long_term_memory_databases
chgrp omni-sys long_term_memory_databases
chown om-sys long_term_memory_databases
chmod 700 long_term_memory_databases
cd long_term_memory_databases
mkdir music
chgrp omni-sys music
chown om-sys music
chmod 700 music
mkdir movies
chgrp omni-sys movies
chown om-sys movies
chmod 700 movies
mkdir literature
chgrp omni-sys literature
chown om-sys literature
chmod 700 literature
mkdir recipes
chgrp omni-sys recipes
chown om-sys recipes
chmod 700 recipes
mkdir foods
chgrp omni-sys foods
chown om-sys foods
chmod 700 foods
mkdir people
chgrp omni-sys people
chmod 700 mkdir people
cd ..
mkdir occipital_lobe
chgrp omni-sys occipital_lobe
chown om-sys occipital_lobe
chmod 700 occipital_lobe
cd occipital_lobe
mkdir perception_of_light
chgrp omni-sys perception_of_light
chown om-sys perception_of_light
chmod 700 perception_of_light
mkdir perception_of_color
chgrp omni-sys perception_of_color
chown om-sys perception_of_color
chmod 700 perception_of_color
mkdir perception_of_movement
chgrp omni-sys perception_of_movement
chmod 700 mkdir perception_of_movement
cd ..
mkdir parietal_lobe
chgrp omni-sys parietal_lobe
chown om-sys parietal_lobe
chmod 700 parietal_lobe
cd parietal_lobe
mkdir sensory_cortex
chgrp omni-sys sensory_cortex
chown om-sys sensory_cortex
chmod 700 sensory_cortex
mkdir sensory_comprehension
chgrp omni-sys sensory_comprehension
chown om-sys sensory_comprehension
chmod 700 sensory_comprehension
cd sensory_comprehension
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir motor
chgrp omni-sys motor
chown om-sys motor
chmod 700 motor
cd motor
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir sensory
chgrp omni-sys sensory
chown om-sys sensory
chmod 700 sensory
cd sensory
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir memory
chgrp omni-sys memory
chown om-sys memory
chmod 700 memory
cd memory
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir spatial_perception
chgrp omni-sys spatial_perception
chown om-sys spatial_perception
chmod 700 spatial_perception
cd spatial_perception
mkdir interprets_visual_signals
chgrp omni-sys interprets_visual_signals
chown om-sys interprets_visual_signals
chmod 700 interprets_visual_signals
mkdir interprets_audio_signal
chgrp omni-sys interprets_audio_signal
chmod 700 mkdir interprets_audio_signal
cd ..
mkdir visual_perception
chgrp omni-sys visual_perception
chown om-sys visual_perception
chmod 700 visual_perception
cd visual_perception
mkdir interprets_visual_signal
chgrp omni-sys interprets_visual_signal
chown om-sys interprets_visual_signal
chmod 700 interprets_visual_signal
mkdir interprets_audio_signal
chgrp omni-sys interprets_audio_signal
chmod 700 mkdir interprets_audio_signal
cd ..
mkdir tactile_sensation
chgrp omni-sys tactile_sensation
chown om-sys tactile_sensation
chmod 700 tactile_sensation
cd tactile_sensation
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chown om-sys sense_of_temperature
chmod 700 sense_of_temperature
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chmod 700 mkdir sense_of_pain
cd ..
mkdir internal_stimuli
chgrp omni-sys internal_stimuli
chown om-sys internal_stimuli
chmod 700 internal_stimuli
mkdir motor_cortex
chgrp omni-sys motor_cortex
chown om-sys motor_cortex
chmod 700 motor_cortex
mkdir visual_functions
chgrp omni-sys visual_functions
chown om-sys visual_functions
chmod 700 visual_functions
mkdir language
chgrp omni-sys language
chown om-sys language
chmod 700 language
cd language
mkdir interprets_language
chgrp omni-sys interprets_language
chown om-sys interprets_language
chmod 700 interprets_language
mkdir interprets_words
chgrp omni-sys interprets_words
chmod 700 mkdir interprets_words
cd ..
mkdir reading
chgrp omni-sys reading
chown om-sys reading
chmod 700 reading
cd reading
mkdir interprets_language
chgrp omni-sys interprets_language
chown om-sys interprets_language
chmod 700 interprets_language
mkdir interprets_words
chgrp omni-sys interprets_words
chmod 700 mkdir interprets_words
cd ..
mkdir frontal_lobe
chgrp omni-sys frontal_lobe
chown om-sys frontal_lobe
chmod 700 frontal_lobe
cd frontal_lobe
mkdir creative_thought
chgrp omni-sys creative_thought
chown om-sys creative_thought
chmod 700 creative_thought
mkdir problem_solving
chgrp omni-sys problem_solving
chown om-sys problem_solving
chmod 700 problem_solving
mkdir intellect
chgrp omni-sys intellect
chown om-sys intellect
chmod 700 intellect
mkdir judgment
chgrp omni-sys judgment
chown om-sys judgment
chmod 700 judgment
mkdir behavior
chgrp omni-sys behavior
chown om-sys behavior
chmod 700 behavior
mkdir attention
chgrp omni-sys attention
chown om-sys attention
chmod 700 attention
mkdir abstract_thinking
chgrp omni-sys abstract_thinking
chown om-sys abstract_thinking
chmod 700 abstract_thinking
mkdir physical_reactions
chgrp omni-sys physical_reactions
chown om-sys physical_reactions
chmod 700 physical_reactions
mkdir muscle_movements
chgrp omni-sys muscle_movements
chown om-sys muscle_movements
chmod 700 muscle_movements
mkdir coordinated_movements
chgrp omni-sys coordinated_movements
chown om-sys coordinated_movements
chmod 700 coordinated_movements
mkdir smell
chgrp omni-sys smell
chown om-sys smell
chmod 700 smell
mkdir personality
chgrp omni-sys personality
chmod 700 mkdir personality
cd ..
mkdir axons
chgrp omni-sys axons
chown om-sys axons
chmod 700 axons
mkdir right_hemisphere
chgrp omni-sys right_hemisphere
chown om-sys right_hemisphere
chmod 700 right_hemisphere
cd right_hemisphere
mkdir temporal_lobe
chgrp omni-sys temporal_lobe
chown om-sys temporal_lobe
chmod 700 temporal_lobe
cd temporal_lobe
mkdir cerebral_hemisphere
chgrp omni-sys cerebral_hemisphere
chown om-sys cerebral_hemisphere
chmod 700 cerebral_hemisphere
cd cerebral_hemisphere
mkdir visual_memories
chgrp omni-sys visual_memories
chown om-sys visual_memories
chmod 700 visual_memories
mkdir auditory_memories
chgrp omni-sys auditory_memories
chown om-sys auditory_memories
chmod 700 auditory_memories
mkdir speech_capabilities
chgrp omni-sys speech_capabilities
chown om-sys speech_capabilities
chmod 700 speech_capabilities
mkdir hearing_capabilities
chgrp omni-sys hearing_capabilities
chown om-sys hearing_capabilities
chmod 700 hearing_capabilities
mkdir behavioral_elements
chgrp omni-sys behavioral_elements
chown om-sys behavioral_elements
chmod 700 behavioral_elements
mkdir language
chgrp omni-sys language
chmod 700 mkdir language
cd ..
mkdir medial_temporal_lobe
chgrp omni-sys medial_temporal_lobe
chown om-sys medial_temporal_lobe
chmod 700 medial_temporal_lobe
cd medial_temporal_lobe
mkdir hippocampus
chgrp omni-sys hippocampus
chown om-sys hippocampus
chmod 700 hippocampus
cd hippocampus
mkdir long_term_memory_database
chgrp omni-sys long_term_memory_database
chown om-sys long_term_memory_database
chmod 700 long_term_memory_database
cd long_term_memory_database
mkdir music
chgrp omni-sys music
chown om-sys music
chmod 700 music
mkdir movies
chgrp omni-sys movies
chown om-sys movies
chmod 700 movies
mkdir literature
chgrp omni-sys literature
chown om-sys literature
chmod 700 literature
mkdir recipes
chgrp omni-sys recipes
chown om-sys recipes
chmod 700 recipes
mkdir foods
chgrp omni-sys foods
chown om-sys foods
chmod 700 foods
mkdir people
chgrp omni-sys people
chmod 700 mkdir people
cd ..
mkdir occipital_lobe
chgrp omni-sys occipital_lobe
chown om-sys occipital_lobe
chmod 700 occipital_lobe
cd occipital_lobe
mkdir perception_of_light
chgrp omni-sys perception_of_light
chown om-sys perception_of_light
chmod 700 perception_of_light
mkdir perception_of_color
chgrp omni-sys perception_of_color
chown om-sys perception_of_color
chmod 700 perception_of_color
mkdir perception_of_movement
chgrp omni-sys perception_of_movement
chmod 700 mkdir perception_of_movement
cd ..
mkdir parietal_lobe
chgrp omni-sys parietal_lobe
chown om-sys parietal_lobe
chmod 700 parietal_lobe
cd parietal_lobe
mkdir sensory_cortex
chgrp omni-sys sensory_cortex
chown om-sys sensory_cortex
chmod 700 sensory_cortex
mkdir sensory_comprehension
chgrp omni-sys sensory_comprehension
chown om-sys sensory_comprehension
chmod 700 sensory_comprehension
cd sensory_comprehension
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir motor
chgrp omni-sys motor
chown om-sys motor
chmod 700 motor
cd motor
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir sensory
chgrp omni-sys sensory
chown om-sys sensory
chmod 700 sensory
cd sensory
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir memory
chgrp omni-sys memory
chown om-sys memory
chmod 700 memory
cd memory
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chown om-sys sense_of_pain
chmod 700 sense_of_pain
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chmod 700 mkdir sense_of_temperature
cd ..
mkdir spatial_perception
chgrp omni-sys spatial_perception
chown om-sys spatial_perception
chmod 700 spatial_perception
cd spatial_perception
mkdir interprets_visual_signals
chgrp omni-sys interprets_visual_signals
chown om-sys interprets_visual_signals
chmod 700 interprets_visual_signals
mkdir interprets_audio_signal
chgrp omni-sys interprets_audio_signal
chmod 700 mkdir interprets_audio_signal
cd ..
mkdir visual_perception
chgrp omni-sys visual_perception
chown om-sys visual_perception
chmod 700 visual_perception
cd visual_perception
mkdir interprets_visual_signal
chgrp omni-sys interprets_visual_signal
chown om-sys interprets_visual_signal
chmod 700 interprets_visual_signal
mkdir interprets_audio_signal
chgrp omni-sys interprets_audio_signal
chmod 700 mkdir interprets_audio_signal
cd ..
mkdir tactile_sensation
chgrp omni-sys tactile_sensation
chown om-sys tactile_sensation
chmod 700 tactile_sensation
cd tactile_sensation
mkdir sense_of_touch
chgrp omni-sys sense_of_touch
chown om-sys sense_of_touch
chmod 700 sense_of_touch
mkdir sense_of_temperature
chgrp omni-sys sense_of_temperature
chown om-sys sense_of_temperature
chmod 700 sense_of_temperature
mkdir sense_of_pain
chgrp omni-sys sense_of_pain
chmod 700 mkdir sense_of_pain
cd ..
mkdir internal_stimuli
chgrp omni-sys internal_stimuli
chown om-sys internal_stimuli
chmod 700 internal_stimuli
mkdir motor_cortex
chgrp omni-sys motor_cortex
chown om-sys motor_cortex
chmod 700 motor_cortex
mkdir visual_functions
chgrp omni-sys visual_functions
chown om-sys visual_functions
chmod 700 visual_functions
mkdir language
chgrp omni-sys language
chown om-sys language
chmod 700 language
cd language
mkdir interprets_language
chgrp omni-sys interprets_language
chown om-sys interprets_language
chmod 700 interprets_language
mkdir interprets_words
chgrp omni-sys interprets_words
chmod 700 mkdir interprets_words
cd ..
mkdir reading
chgrp omni-sys reading
chown om-sys reading
chmod 700 reading
cd reading
mkdir interprets_language
chgrp omni-sys interprets_language
chown om-sys interprets_language
chmod 700 interprets_language
mkdir interprets_words
chgrp omni-sys interprets_words
chmod 700 mkdir interprets_words
cd ..
mkdir frontal_lobe
chgrp omni-sys frontal_lobe
chown om-sys frontal_lobe
chmod 700 frontal_lobe
cd frontal_lobe
mkdir creative_thought
chgrp omni-sys creative_thought
chown om-sys creative_thought
chmod 700 creative_thought
mkdir problem_solving
chgrp omni-sys problem_solving
chown om-sys problem_solving
chmod 700 problem_solving
mkdir intellect
chgrp omni-sys intellect
chown om-sys intellect
chmod 700 intellect
mkdir judgment
chgrp omni-sys judgment
chown om-sys judgment
chmod 700 judgment
mkdir behavior
chgrp omni-sys behavior
chown om-sys behavior
chmod 700 behavior
mkdir attention
chgrp omni-sys attention
chown om-sys attention
chmod 700 attention
mkdir abstract_thinking
chgrp omni-sys abstract_thinking
chown om-sys abstract_thinking
chmod 700 abstract_thinking
mkdir physical_reactions
chgrp omni-sys physical_reactions
chown om-sys physical_reactions
chmod 700 physical_reactions
mkdir muscle_movements
chgrp omni-sys muscle_movements
chown om-sys muscle_movements
chmod 700 muscle_movements
mkdir coordinated_movements
chgrp omni-sys coordinated_movements
chown om-sys coordinated_movements
chmod 700 coordinated_movements
mkdir smell
chgrp omni-sys smell
chown om-sys smell
chmod 700 smell
mkdir personality
chgrp omni-sys personality
chown om-sys personality
chmod 700 personality
mkdir medial_temporal_lobe
chgrp omni-sys medial_temporal_lobe
chown om-sys medial_temporal_lobe
chmod 700 medial_temporal_lobe
cd medial_temporal_lobe
mkdir hippocampus
chgrp omni-sys hippocampus
chmod 700 mkdir hippocampus
cd ..
mkdir hypothalamus
chgrp omni-sys hypothalamus
chown om-sys hypothalamus
chmod 700 hypothalamus
cd hypothalamus
mkdir hunger
chgrp omni-sys hunger
chown om-sys hunger
chmod 700 hunger
mkdir thirst
chgrp omni-sys thirst
chown om-sys thirst
chmod 700 thirst
mkdir sleep
chgrp omni-sys sleep
chown om-sys sleep
chmod 700 sleep
mkdir sexual_response
chgrp omni-sys sexual_response
chown om-sys sexual_response
chmod 700 sexual_response
mkdir body_temperature
chgrp omni-sys body_temperature
chown om-sys body_temperature
chmod 700 body_temperature
mkdir blood_pressure
chgrp omni-sys blood_pressure
chown om-sys blood_pressure
chmod 700 blood_pressure
mkdir emotions
chgrp omni-sys emotions
chown om-sys emotions
chmod 700 emotions
mkdir hormones
chgrp omni-sys hormones
chmod 700 mkdir hormones
cd ..
mkdir pituitary_stalk
chgrp omni-sys pituitary_stalk
chown om-sys pituitary_stalk
chmod 700 pituitary_stalk
cd pituitary_stalk
mkdir sella_turcica
chgrp omni-sys sella_turcica
chown om-sys sella_turcica
chmod 700 sella_turcica
cd sella_turcica
mkdir pituitary_gland_master_gland
chgrp omni-sys pituitary_gland_master_gland
chown om-sys pituitary_gland_master_gland
chmod 700 pituitary_gland_master_gland
cd pituitary_gland_master_gland
mkdir promotes_sexual_development
chgrp omni-sys promotes_sexual_development
chown om-sys promotes_sexual_development
chmod 700 promotes_sexual_development
mkdir promotes_bone_growth
chgrp omni-sys promotes_bone_growth
chown om-sys promotes_bone_growth
chmod 700 promotes_bone_growth
mkdir promotes_muscle_growth
chgrp omni-sys promotes_muscle_growth
chown om-sys promotes_muscle_growth
chmod 700 promotes_muscle_growth
mkdir responds_to_stress
chgrp omni-sys responds_to_stress
chown om-sys responds_to_stress
chmod 700 responds_to_stress
mkdir fights_diseases
chgrp omni-sys fights_diseases
chmod 700 mkdir fights_diseases
cd ..
mkdir pineal_gland
chgrp omni-sys pineal_gland
chown om-sys pineal_gland
chmod 700 pineal_gland
mkdir internal_clock
chgrp omni-sys internal_clock
chown om-sys internal_clock
chmod 700 internal_clock
cd internal_clock
mkdir secretions
chgrp omni-sys secretions
chown om-sys secretions
chmod 700 secretions
cd secretions
mkdir melatonin
chgrp omni-sys melatonin
chown om-sys melatonin
chmod 700 melatonin
cd melatonin
mkdir regulates_circadian_rhythms
chgrp omni-sys regulates_circadian_rhythms
chmod 700 mkdir regulates_circadian_rhythms
cd ..
mkdir role_in_sexual_development
chgrp omni-sys role_in_sexual_development
chmod 700 mkdir role_in_sexual_development
cd ..
mkdir thalamus
chgrp omni-sys thalamus
chown om-sys thalamus
chmod 700 thalamus
cd thalamus
mkdir relay_station
chgrp omni-sys relay_station
chown om-sys relay_station
chmod 700 relay_station
cd relay_station
mkdir pain_sensation
chgrp omni-sys pain_sensation
chown om-sys pain_sensation
chmod 700 pain_sensation
mkdir attention
chgrp omni-sys attention
chown om-sys attention
chmod 700 attention
mkdir alertness
chgrp omni-sys alertness
chown om-sys alertness
chmod 700 alertness
mkdir memory
chgrp omni-sys memory
chown om-sys memory
chmod 700 memory
