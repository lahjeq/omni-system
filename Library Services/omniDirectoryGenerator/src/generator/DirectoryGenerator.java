package generator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;



public class DirectoryGenerator {

	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		File file = new File("omni-dir-structure.sh");
		FileInputStream fis;
		fis = new FileInputStream("dir-skeleton.txt");
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		ArrayList<String> directories = new ArrayList<String>();
		ArrayList<String> dirs = new ArrayList<String>();
		String content = null;
		String pwd = null;
		String currentString = null;
		String nextString = null;
		String previousString = null;
		String temp_directory_string = null;
		String final_directory_string = null;
		String temp_command_string = null;
		String final_command_string = null;
		String rootdirectory = "";
		Integer listCeiling = 0;
		Integer dirLevelCount = 0;
		Integer dirLevel = 0;
		Integer nextDirLevel = 0;
		Integer previousCount = 0;
		Integer previousDirectoryLevelCount = 0;
		Integer nextCount = 0;
		Integer currentCount = 0;
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		Date date = new Date();

	    String shell_header = "#!/bin/bash";
		bw.write(shell_header);
		bw.newLine();
		String script_notes = "#This script was generated" + date.toString() + ". " + "It is used to create the directory structure for the omni system.";
		bw.write(script_notes);
		bw.newLine();
		String initial_directory = "cd /";
		bw.write(initial_directory);
		bw.newLine();
		String mkdir_omni = "mkdir omni";
		bw.write(mkdir_omni);
		bw.newLine();
	    String chgrp = "chgrp omni-sys omni";
	    bw.write(chgrp);
	    bw.newLine();
	    String chown = "chgrp om-sys omni";
	    bw.write(chown);
	    bw.newLine();
	    String chmod = "chmod 700 omni";
	    bw.write(chmod);
	    bw.newLine();
		String cd_omni = "cd omni";
	    bw.write(cd_omni);
	    bw.newLine();
	    
				
		for (String line = br.readLine(); line != null; line = br.readLine()){
			if ((line.startsWith("-") || (line.startsWith(".")))) {
			directories.add(line.toString());
			}
		}
			
		for (String dir : directories){
				String current_directory = dir.toString();
				//System.out.println("current_directory = " + current_directory);
				if (current_directory.startsWith("."))
				{
					rootdirectory = current_directory.replaceAll(".", "");
					dirs.add(rootdirectory);
					dirs.add("cd " + rootdirectory);
					break;
				}
		}

		for (int i = 0; i < directories.size(); i++)
			{
				String current_directory = directories.get(i);
				if (current_directory.startsWith("-"))
				{
					currentString = directories.get(i);
					String[] currentDir = currentString.split("-");
					currentCount = currentDir.length - 1;
					
					//get list size
					listCeiling = directories.size();
						previousString = directories.get(i-1);
						String[] previousDir = previousString.split("-");
						previousCount = previousDir.length - 1;
					
					if (i < listCeiling - 1)
					{
						nextString = directories.get(i+1);
						String[] nextDir = nextString.split("-");
						nextCount = nextDir.length - 1;
					}
					if (currentCount < nextCount )
					{
						temp_directory_string = directories.get(i).replaceAll("-", "");
						temp_directory_string = temp_directory_string.trim();
						temp_directory_string = temp_directory_string.replaceAll("\\(", "_");
						temp_directory_string = temp_directory_string.replaceAll("\\)", "");
						temp_directory_string = temp_directory_string.replaceAll("\\'", "");
						temp_directory_string = temp_directory_string.replaceAll(" ", "_");
						temp_directory_string = temp_directory_string.toLowerCase();
						final_directory_string = "mkdir " + temp_directory_string;
						bw.write(final_directory_string);
						bw.newLine();
						System.out.println(final_directory_string);
						final_command_string = "chgrp omni-sys " + temp_directory_string;
						bw.write(final_command_string);
						bw.newLine();
						final_command_string = "chown om-sys " + temp_directory_string;
						bw.write(final_command_string);
						bw.newLine();
						System.out.println(final_command_string);
						final_command_string = "chmod 700 " + temp_directory_string;
						bw.write(final_command_string);
						bw.newLine();
						System.out.println(final_command_string);
						temp_command_string = directories.get(i).replaceAll("-", "");
						temp_command_string = temp_command_string.trim();
						temp_command_string = temp_command_string.replaceAll("\\(", "_");
						temp_command_string = temp_command_string.replaceAll("\\)", "");
						temp_command_string = temp_command_string.replaceAll("\\'", "");
						temp_command_string = temp_command_string.replaceAll(" ", "_");
						temp_command_string = temp_command_string.toLowerCase();
						final_command_string = "cd " + temp_command_string;
						bw.write(final_command_string);
						bw.newLine();
						System.out.println(final_command_string);
					}
					else if (currentCount == nextCount)
					{
						temp_directory_string = directories.get(i).replaceAll("-", "");
						temp_directory_string = temp_directory_string.trim();
						temp_directory_string = temp_directory_string.replaceAll("\\(", "_");
						temp_directory_string = temp_directory_string.replaceAll("\\)", "");
						temp_directory_string = temp_directory_string.replaceAll("\\'", "");
						temp_directory_string = temp_directory_string.replaceAll(" ", "_");
						temp_directory_string = temp_directory_string.toLowerCase();
						final_directory_string = "mkdir " + temp_directory_string;
						System.out.println(final_directory_string);
						bw.write(final_directory_string);
						bw.newLine();
						final_directory_string = "chgrp omni-sys " + temp_directory_string;
						System.out.println(final_directory_string);
						bw.write(final_directory_string);
						bw.newLine();
						final_directory_string = "chown om-sys " + temp_directory_string;
						System.out.println(final_directory_string);
						bw.write(final_directory_string);
						bw.newLine();
						final_command_string = "chmod 700 " + temp_directory_string;
						System.out.println(final_command_string);
						bw.write(final_command_string);
						bw.newLine();
					}
					else if (currentCount > nextCount)
					{
						temp_directory_string = directories.get(i).replaceAll("-", "");
						temp_directory_string = temp_directory_string.trim();
						temp_directory_string = temp_directory_string.replaceAll("\\(", "_");
						temp_directory_string = temp_directory_string.replaceAll("\\)", "");
						temp_directory_string = temp_directory_string.replaceAll("\\'", "");
						temp_directory_string = temp_directory_string.replaceAll(" ", "_");
						temp_directory_string = temp_directory_string.toLowerCase();
						final_directory_string = "mkdir " + temp_directory_string;
						bw.write(final_directory_string);
						bw.newLine();
						System.out.println(final_directory_string);
						final_command_string = "chgrp omni-sys " + temp_directory_string;
						bw.write(final_command_string);
						bw.newLine();
						final_command_string = "chown om-sys " + temp_directory_string;
						System.out.println(final_command_string);
						final_command_string = "chmod 700 " + final_directory_string;
						bw.write(final_command_string);
						bw.newLine();
						System.out.println(final_command_string);
						final_command_string = "cd ..";
						bw.write(final_command_string);
						bw.newLine();
						System.out.println(final_command_string);	
					}
				}
			}
			bw.close();
		}
	}

		


		
